﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;

namespace STP_Lite.Measurers
{
    public class ApiMeasurer
    {
        private string service = "";

        public void SetService(string service)
        {
            this.service = service;
        }

        public T Execute<T>(RestRequest request, string id, string operation_type) where T : new()
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(service + "/" + id + "/" + operation_type);

            var response = client.Execute<T>(request);
            if (!response.Content.Contains("[[]]"))
            {
                if (response.ErrorException != null)
                {
                    Logger logger = new Logger();
                    logger.LogException(response.ErrorException);
                    //throw response.ErrorException;
                }
            }
            return response.Data;
        }

        public string SendOperation(string id, string operation_type)
        {
            var client = new RestClient(service);
            var request = new RestRequest("operation/{ID}/{OperationType}", Method.POST);
            request.AddUrlSegment("ID", id);
            request.AddUrlSegment("OperationType", operation_type);
            var response = client.Execute(request);
            var content = response.Content;
            return content;
        }

        public string SendOperation(string id, string operation_type, Dictionary<string, string> parameter)
        {
            var client = new RestClient(service);
            var request = new RestRequest("operation/{ID}/{OperationType}", Method.POST);
            request.AddUrlSegment("ID", id);
            request.AddUrlSegment("OperationType", operation_type);
            request.AddJsonBody(parameter);
            var response = client.Execute(request);
            var content = response.Content;
            return content;
        }
    }
}
