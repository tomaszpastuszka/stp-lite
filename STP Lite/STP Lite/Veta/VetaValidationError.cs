﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STP_Lite.Veta
{
    public class VetaValidationError
    {
        public string eng;
        public string pl;
        //public DocStatus status;

        public VetaValidationError(string eng, string pl)
        {
            this.eng = eng;
            this.pl = pl;
            //this.status = status;
        }
    }
}
