﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ten kod został wygenerowany przez narzędzie.
//     Wersja wykonawcza:2.0.50727.8009
//
//     Zmiany w tym pliku mogą spowodować nieprawidłowe zachowanie i zostaną utracone, jeśli
//     kod zostanie ponownie wygenerowany.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Xml.Serialization;

// 
// This source code was auto-generated by xsd, Version=2.0.50727.3038.
// 


/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("orderResponseTypeElement", Namespace="", IsNullable=false)]
public partial class orderResponseType : orderBaseType {
    
    private responseStatusEnum statusField;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public responseStatusEnum status {
        get {
            return this.statusField;
        }
        set {
            this.statusField = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
public enum responseStatusEnum {
    
    /// <uwagi/>
    Success,
    
    /// <uwagi/>
    Fail,
}

/// <uwagi/>
[System.Xml.Serialization.XmlIncludeAttribute(typeof(updateInformationResponseType))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(addInformationResponseType))]
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("simpleResponseInformationTypeElement", Namespace="", IsNullable=false)]
public partial class simpleResponseInformationType {
    
    private errorType errorField;
    
    private System.DateTime deliveryDateField;
    
    private bool deliveryDateFieldSpecified;
    
    private System.DateTime modifiedDateField;
    
    private bool modifiedDateFieldSpecified;
    
    /// <uwagi/>
    public errorType Error {
        get {
            return this.errorField;
        }
        set {
            this.errorField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public System.DateTime deliveryDate {
        get {
            return this.deliveryDateField;
        }
        set {
            this.deliveryDateField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool deliveryDateSpecified {
        get {
            return this.deliveryDateFieldSpecified;
        }
        set {
            this.deliveryDateFieldSpecified = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public System.DateTime modifiedDate {
        get {
            return this.modifiedDateField;
        }
        set {
            this.modifiedDateField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool modifiedDateSpecified {
        get {
            return this.modifiedDateFieldSpecified;
        }
        set {
            this.modifiedDateFieldSpecified = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("errorTypeElement", Namespace="", IsNullable=false)]
public partial class errorType {
    
    private int codeField;
    
    private bool codeFieldSpecified;
    
    private string valueField;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public int code {
        get {
            return this.codeField;
        }
        set {
            this.codeField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool codeSpecified {
        get {
            return this.codeFieldSpecified;
        }
        set {
            this.codeFieldSpecified = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlTextAttribute()]
    public string Value {
        get {
            return this.valueField;
        }
        set {
            this.valueField = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("responseObligationTypeElement", Namespace="", IsNullable=false)]
public partial class responseObligationType {
    
    private string documentNumberField;
    
    private documentTypeEnum documentTypeField;
    
    private string customDocumentTypeField;
    
    private System.DateTime documentCreationDateField;
    
    private System.DateTime dueDateField;
    
    private string bankAccountField;
    
    private moneyType totalAmountField;
    
    private moneyType totalAmountNetField;
    
    private object itemField;
    
    private string userIDField;
    
    /// <uwagi/>
    public string DocumentNumber {
        get {
            return this.documentNumberField;
        }
        set {
            this.documentNumberField = value;
        }
    }
    
    /// <uwagi/>
    public documentTypeEnum DocumentType {
        get {
            return this.documentTypeField;
        }
        set {
            this.documentTypeField = value;
        }
    }
    
    /// <uwagi/>
    public string CustomDocumentType {
        get {
            return this.customDocumentTypeField;
        }
        set {
            this.customDocumentTypeField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime DocumentCreationDate {
        get {
            return this.documentCreationDateField;
        }
        set {
            this.documentCreationDateField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime DueDate {
        get {
            return this.dueDateField;
        }
        set {
            this.dueDateField = value;
        }
    }
    
    /// <uwagi/>
    public string BankAccount {
        get {
            return this.bankAccountField;
        }
        set {
            this.bankAccountField = value;
        }
    }
    
    /// <uwagi/>
    public moneyType TotalAmount {
        get {
            return this.totalAmountField;
        }
        set {
            this.totalAmountField = value;
        }
    }
    
    /// <uwagi/>
    public moneyType TotalAmountNet {
        get {
            return this.totalAmountNetField;
        }
        set {
            this.totalAmountNetField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute("Entrepreneur", typeof(entrepreneurType))]
    [System.Xml.Serialization.XmlElementAttribute("LegalPerson", typeof(legalPersonType))]
    public object Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string userID {
        get {
            return this.userIDField;
        }
        set {
            this.userIDField = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("responseRegulationsAcceptanceTypeElement", Namespace="", IsNullable=false)]
public partial class responseRegulationsAcceptanceType {
    
    private string regulationsIdField;
    
    private System.DateTime acceptanceDateField;
    
    private string clientIPField;
    
    /// <uwagi/>
    public string RegulationsId {
        get {
            return this.regulationsIdField;
        }
        set {
            this.regulationsIdField = value;
        }
    }
    
    /// <uwagi/>
    public System.DateTime AcceptanceDate {
        get {
            return this.acceptanceDateField;
        }
        set {
            this.acceptanceDateField = value;
        }
    }
    
    /// <uwagi/>
    public string ClientIP {
        get {
            return this.clientIPField;
        }
        set {
            this.clientIPField = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("responsePartialPaymentTypeElement", Namespace="", IsNullable=false)]
public partial class responsePartialPaymentType {
    
    private System.DateTime paidDateField;
    
    private moneyType amountField;
    
    private string userIDField;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime PaidDate {
        get {
            return this.paidDateField;
        }
        set {
            this.paidDateField = value;
        }
    }
    
    /// <uwagi/>
    public moneyType Amount {
        get {
            return this.amountField;
        }
        set {
            this.amountField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string userID {
        get {
            return this.userIDField;
        }
        set {
            this.userIDField = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("responseFullPaymentTypeElement", Namespace="", IsNullable=false)]
public partial class responseFullPaymentType {
    
    private System.DateTime paidDateField;
    
    private NonConsumerProviderDataFilterEnum nonConsumerProviderDataFilterField;
    
    private object[] itemsField;
    
    private string userIDField;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime PaidDate {
        get {
            return this.paidDateField;
        }
        set {
            this.paidDateField = value;
        }
    }
    
    /// <uwagi/>
    public NonConsumerProviderDataFilterEnum NonConsumerProviderDataFilter {
        get {
            return this.nonConsumerProviderDataFilterField;
        }
        set {
            this.nonConsumerProviderDataFilterField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute("ContractorConsentRequestData", typeof(contractorConsentRequestDataType))]
    [System.Xml.Serialization.XmlElementAttribute("HasIndividualConsent", typeof(bool))]
    public object[] Items {
        get {
            return this.itemsField;
        }
        set {
            this.itemsField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string userID {
        get {
            return this.userIDField;
        }
        set {
            this.userIDField = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("responseUnholdObligationTypeElement", Namespace="", IsNullable=false)]
public partial class responseUnholdObligationType {
    
    private string userIDField;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string userID {
        get {
            return this.userIDField;
        }
        set {
            this.userIDField = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("addInformationResponseTypeElement", Namespace="", IsNullable=false)]
public partial class addInformationResponseType : simpleResponseInformationType {
    
    private object itemField;
    
    private bool verifyResultField;
    
    private bool verifyResultFieldSpecified;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute("Obligation", typeof(responseObligationType))]
    [System.Xml.Serialization.XmlElementAttribute("RegulationsAcceptance", typeof(responseRegulationsAcceptanceType))]
    public object Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public bool verifyResult {
        get {
            return this.verifyResultField;
        }
        set {
            this.verifyResultField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool verifyResultSpecified {
        get {
            return this.verifyResultFieldSpecified;
        }
        set {
            this.verifyResultFieldSpecified = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("updateInformationResponseTypeElement", Namespace="", IsNullable=false)]
public partial class updateInformationResponseType : simpleResponseInformationType {
    
    private object itemField;
    
    private bool verifyResultField;
    
    private bool verifyResultFieldSpecified;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute("FullPayment", typeof(responseFullPaymentType))]
    [System.Xml.Serialization.XmlElementAttribute("PartialPayment", typeof(responsePartialPaymentType))]
    [System.Xml.Serialization.XmlElementAttribute("UnholdObligation", typeof(responseUnholdObligationType))]
    public object Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public bool verifyResult {
        get {
            return this.verifyResultField;
        }
        set {
            this.verifyResultField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool verifyResultSpecified {
        get {
            return this.verifyResultFieldSpecified;
        }
        set {
            this.verifyResultFieldSpecified = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("getInformationResponseTypeElement", Namespace="", IsNullable=false)]
public partial class getInformationResponseType {
    
    private object itemField;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute("Error", typeof(errorType))]
    [System.Xml.Serialization.XmlElementAttribute("ObligationState", typeof(responseObligationStateType))]
    public object Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("responseObligationStateTypeElement", Namespace="", IsNullable=false)]
public partial class responseObligationStateType {
    
    private obligationStatusEnum obligationStatusField;
    
    private informationStatusEnum informationStatusField;
    
    private positiveInformationDataType positiveInformationDataField;
    
    private negativeInformationDataType negativeInformationDataField;
    
    private bool onHoldField;
    
    private string userIDField;
    
    /// <uwagi/>
    public obligationStatusEnum ObligationStatus {
        get {
            return this.obligationStatusField;
        }
        set {
            this.obligationStatusField = value;
        }
    }
    
    /// <uwagi/>
    public informationStatusEnum InformationStatus {
        get {
            return this.informationStatusField;
        }
        set {
            this.informationStatusField = value;
        }
    }
    
    /// <uwagi/>
    public positiveInformationDataType PositiveInformationData {
        get {
            return this.positiveInformationDataField;
        }
        set {
            this.positiveInformationDataField = value;
        }
    }
    
    /// <uwagi/>
    public negativeInformationDataType NegativeInformationData {
        get {
            return this.negativeInformationDataField;
        }
        set {
            this.negativeInformationDataField = value;
        }
    }
    
    /// <uwagi/>
    public bool OnHold {
        get {
            return this.onHoldField;
        }
        set {
            this.onHoldField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string userID {
        get {
            return this.userIDField;
        }
        set {
            this.userIDField = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
public enum obligationStatusEnum {
    
    /// <uwagi/>
    New,
    
    /// <uwagi/>
    Overdue,
    
    /// <uwagi/>
    InReminderStage,
    
    /// <uwagi/>
    ReadyForVindication,
    
    /// <uwagi/>
    InArbitrationStage,
    
    /// <uwagi/>
    InVindicationStage,
    
    /// <uwagi/>
    Paid,
    
    /// <uwagi/>
    Canceled,
    
    /// <uwagi/>
    Ended,
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
public enum informationStatusEnum {
    
    /// <uwagi/>
    NotSet,
    
    /// <uwagi/>
    NegativeInformationAdded,
    
    /// <uwagi/>
    NegativeInformationPaid,
    
    /// <uwagi/>
    NegativeInformationRemoved,
    
    /// <uwagi/>
    PositiveInformationAdded,
    
    /// <uwagi/>
    ConditionsForPositiveInformationNotMet,
    
    /// <uwagi/>
    WaitingForRegisteringPositiveInformation,
    
    /// <uwagi/>
    PositiveInformationRemoved,
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("positiveInformationDataTypeElement", Namespace="", IsNullable=false)]
public partial class positiveInformationDataType {
    
    private bool hasContractOriginalField;
    
    private bool hasConsentField;
    
    private bool addPInfInProgressField;
    
    /// <uwagi/>
    public bool HasContractOriginal {
        get {
            return this.hasContractOriginalField;
        }
        set {
            this.hasContractOriginalField = value;
        }
    }
    
    /// <uwagi/>
    public bool HasConsent {
        get {
            return this.hasConsentField;
        }
        set {
            this.hasConsentField = value;
        }
    }
    
    /// <uwagi/>
    public bool AddPInfInProgress {
        get {
            return this.addPInfInProgressField;
        }
        set {
            this.addPInfInProgressField = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute("negativeInformationDataTypeElement", Namespace="", IsNullable=false)]
public partial class negativeInformationDataType {
    
    private bool blockedField;
    
    private bool suspendedField;
    
    private System.DateTime suspendStartDateField;
    
    private bool suspendStartDateFieldSpecified;
    
    private System.DateTime suspendEndDateField;
    
    private bool suspendEndDateFieldSpecified;
    
    private string suspendCommentField;
    
    /// <uwagi/>
    public bool Blocked {
        get {
            return this.blockedField;
        }
        set {
            this.blockedField = value;
        }
    }
    
    /// <uwagi/>
    public bool Suspended {
        get {
            return this.suspendedField;
        }
        set {
            this.suspendedField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime SuspendStartDate {
        get {
            return this.suspendStartDateField;
        }
        set {
            this.suspendStartDateField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool SuspendStartDateSpecified {
        get {
            return this.suspendStartDateFieldSpecified;
        }
        set {
            this.suspendStartDateFieldSpecified = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime SuspendEndDate {
        get {
            return this.suspendEndDateField;
        }
        set {
            this.suspendEndDateField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool SuspendEndDateSpecified {
        get {
            return this.suspendEndDateFieldSpecified;
        }
        set {
            this.suspendEndDateFieldSpecified = value;
        }
    }
    
    /// <uwagi/>
    public string SuspendComment {
        get {
            return this.suspendCommentField;
        }
        set {
            this.suspendCommentField = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=false)]
public partial class Output {
    
    private object itemField;
    
    private string versionField;
    
    private string generatorField;
    
    private string fileNameField;
    
    private System.DateTime timeStampField;
    
    private bool timeStampFieldSpecified;
    
    private string applicationSourceNameField;
    
    private System.DateTime startedField;
    
    private System.DateTime processedField;
    
    private int orderCountField;
    
    private int successCountField;
    
    private int failCountField;
    
    public Output() {
        this.versionField = "1.0";
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute("FatalError", typeof(errorType))]
    [System.Xml.Serialization.XmlElementAttribute("InformationManagement", typeof(OutputInformationManagement))]
    public object Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string version {
        get {
            return this.versionField;
        }
        set {
            this.versionField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string generator {
        get {
            return this.generatorField;
        }
        set {
            this.generatorField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string fileName {
        get {
            return this.fileNameField;
        }
        set {
            this.fileNameField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public System.DateTime timeStamp {
        get {
            return this.timeStampField;
        }
        set {
            this.timeStampField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool timeStampSpecified {
        get {
            return this.timeStampFieldSpecified;
        }
        set {
            this.timeStampFieldSpecified = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string applicationSourceName {
        get {
            return this.applicationSourceNameField;
        }
        set {
            this.applicationSourceNameField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public System.DateTime started {
        get {
            return this.startedField;
        }
        set {
            this.startedField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public System.DateTime processed {
        get {
            return this.processedField;
        }
        set {
            this.processedField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public int orderCount {
        get {
            return this.orderCountField;
        }
        set {
            this.orderCountField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public int successCount {
        get {
            return this.successCountField;
        }
        set {
            this.successCountField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public int failCount {
        get {
            return this.failCountField;
        }
        set {
            this.failCountField = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public partial class OutputInformationManagement {
    
    private OutputInformationManagementOrder[] orderField;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute("Order")]
    public OutputInformationManagementOrder[] Order {
        get {
            return this.orderField;
        }
        set {
            this.orderField = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public partial class OutputInformationManagementOrder : orderResponseType {
    
    private object itemField;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute("AddInformation", typeof(addInformationResponseType))]
    [System.Xml.Serialization.XmlElementAttribute("GetInformation", typeof(getInformationResponseType))]
    [System.Xml.Serialization.XmlElementAttribute("UpdateInformation", typeof(updateInformationResponseType))]
    public object Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
}
