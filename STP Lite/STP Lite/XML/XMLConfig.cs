﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;
using ADOX;
using System.Data.OleDb;
using System.Xml;

namespace STP_Lite.XML
{
    public static class XMLConfig
    {
        private static Logger logger = new Logger();
        private static string fileConf = @"C:\Users\" + Environment.UserName + @"\Documents\FPEASY\";
        public static string fileName = fileConf + "FPEASY.conf";
        public static string databasefileName = fileConf + "database.mdb";

        public static void CreateDoc(int license_ver, Version version)
        {
            string str_ver = string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);

            if (!File.Exists(fileName))
            {
                logger.LogInfo("CreateDoc tworzenie pliku konfig");
                XDocument myDoc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));
                XElement root = new XElement("Root");
                XElement xeLogin = new XElement("login", "");
                XElement xePassword = new XElement("password", "");
                XElement xeLicenseAcc = new XElement("license_acceptation", "");
                XElement xeLicenseOWU = new XElement("OWU_acceptation", "");
                XElement xeFirstRun = new XElement("first_run", "1");
                XElement xeLicenseVer = new XElement("license_ver", license_ver);
#if dev
                XElement xeEnvironment = new XElement("environment", "1");
#else
                XElement xeEnvironment = new XElement("environment", "0");
#endif
                XElement xeAppVersion = new XElement("app_version", str_ver);
                XElement xeAppId = new XElement("app_id", "");
                XElement xeLoginTime = new XElement("login_time", "0,0");
                XElement xeRegulationsId = new XElement("regulations_id", "");
                XElement regDateAccept = new XElement("Acceptation_WznsUpoo", "");
                root.Add(xeLogin);
                root.Add(xePassword);
                root.Add(xeLicenseAcc);
                root.Add(xeLicenseOWU);
                root.Add(xeFirstRun);
                root.Add(xeLicenseVer);
                root.Add(xeEnvironment);
                root.Add(xeAppVersion);
                root.Add(xeAppId);
                root.Add(xeLoginTime);
                root.Add(xeRegulationsId);
                root.Add(regDateAccept);
                try
                {
                    myDoc.Add(root);
                    myDoc.Save(fileName);
                }
                catch (Exception ex)
                {
                    logger.LogMessage("XMLConfig: CreateDoc -> Błąd zapisu do pliku FPEASY.conf \n" + ex.Message + " \n" + ex.StackTrace);
                }
            }
            else
            {
                SetAppVersion(str_ver);
                InsertMissingElement(fileName, "regulations_id", "");
                InsertMissingElement(fileName, "Acceptation_WznsUpoo", "");
                //    #if dev
                //    InsertMissingElement(fileName, "environment", "1");
                //    #else
                //    InsertMissingElement(fileName, "environment", "0");
                //    #endif
                //    InsertMissingElement(fileName, "login_time", "0,0");
            }
        }

        public static void InsertMissingElement(string path, string name, string value)
        {
            XDocument doc = XDocument.Load(path);

            XElement newelement = new XElement(name);

            IEnumerable<XElement> search_element = from m in doc.Root.Elements(name) select m;
            if (search_element.Count() == 0)
            {
                IEnumerable<XElement> elements = doc.Root.Elements();
                XElement modules = elements.Last();
                modules.AddBeforeSelf(new XElement(name, value));
            }

            doc.Save(path);
        }

        public static void CreateDataBase()
        {
            if (!Directory.Exists(fileConf))
            {
                Directory.CreateDirectory(fileConf);
            }
            if (!File.Exists(databasefileName))
            {
                ADOX._Catalog cat = new ADOX.Catalog();
                cat.Create("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + databasefileName + ";Jet OLEDB:Engine Type=5");
                OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasefileName + "; Jet OLEDB:Engine Type=5");
                connection.Open();
                string strTemp = "id AUTOINCREMENT PRIMARY KEY, full_number Text(200), gross_amount Float, net_amount Float, bank_account Text(50), status Integer, lacks Memo,  " +
                                 "paid_amount Float, doc_currency Text(10), creation_date DateTime, sent Bit, send_date DateTime, user_id Text(200), case_closed Bit, " +
                                 "contractor_id Integer, due_date DateTime, contractor_tax_number Text(255), contractor_name Text(250), veta_errors Memo";
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;
                command.CommandText = "CREATE TABLE SentDocuments(" + strTemp + ")";
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    logger.LogMessage("CreateDataBase: Błąd tworzenia tabeli SentDocuments \n" + ex.Message);
                }




                strTemp = "id AUTOINCREMENT PRIMARY KEY, name Text(255), acronym Text(255), tax_number Text(255), " +
                          "address Text(255), nr Text(10), flat Text(10), city Text(255), post_code Text(255), post Text(50), country Text(50), email Text(100), phone Text(20), bank_account Text(50), creation_date DateTime, " +
                          "firstName Text(100), lastName Text(100), contact_firstname Text(100), contact_lastname Text(100), contact_role Text(100), updated Bit, last_modify DateTime";
                command.CommandText = "CREATE TABLE Contractors(" + strTemp + ")";
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    logger.LogMessage("CreateDataBase: Błąd tworzenia tabeli Contractors \n" + ex.Message);
                }

                strTemp = "id AUTOINCREMENT PRIMARY KEY, week_id Integer, dictionary_key Text(255), dictionary_value Text(255), sent Bit, creation_date DateTime, sent_date DateTime";
                command.CommandText = "CREATE TABLE WeeklyStatistics(" + strTemp + ")";
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    logger.LogMessage("CreateDataBase: Błąd tworzenia tabeli WeeklyStatistics \n" + ex.Message);
                }

                GenerateWeeklyStatistics(connection);

                /*strTemp = "id Integer, dict_type Integer, name Text(255)";
                command.CommandText = "CREATE TABLE ColumnDictionary(" + strTemp + ")";*/
                command.Connection.Close();
            }
            //else
            //{
            //OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasefileName + "; Jet OLEDB:Engine Type=5");
            //connection.Open();

            //InsertMissingField(connection, "Documents", "full_number", "Text(200)");
            //InsertMissingField(connection, "Documents", "contractor_name", "Text(200)");
            //InsertMissingField(connection, "Documents", "issue_date", "DateTime");
            //InsertMissingField(connection, "Documents", "contractor_id", "Integer");
            //InsertMissingField(connection, "Documents", "bank_account", "Text(50)");
            //InsertMissingField(connection, "Documents", "contractor_tax_number", "Text(255)");
            //InsertMissingField(connection, "Documents", "due_date", "DateTime");
            //InsertMissingField(connection, "Documents", "updated", "Bit");
            //InsertMissingField(connection, "Documents", "net_amount", "Float");
            //InsertMissingField(connection, "Documents", "doc_currency", "Text(10)");
            //InsertMissingField(connection, "Documents", "veta_error", "Bit");

            //Dictionary<string, string> columns = new Dictionary<string, string>();
            //columns.Add("week_id", "Integer");
            //columns.Add("dictionary_key", "Text(255)");
            //columns.Add("dictionary_value", "Text(255)");
            //columns.Add("sent", "Bit");
            //columns.Add("creation_date", "DateTime");
            //columns.Add("sent_date", "DateTime");
            //if(CreateMissingTable(connection, "WeeklyStatistics", columns))
            //{
            //    GenerateWeeklyStatistics(connection);
            //}

            //columns.Clear();

            //columns.Add("external_id", "Integer");
            //columns.Add("integration_type", "Integer");
            //columns.Add("company", "Integer");
            //columns.Add("company_name", "Text(255)");
            //columns.Add("name", "Text(255)");
            //columns.Add("acronym", "Text(255)");
            //columns.Add("tax_number", "Text(255)");
            //columns.Add("address", "Text(255)");
            //columns.Add("nr", "Text(10)");
            //columns.Add("flat", "Text(10)");
            //columns.Add("city", "Text(255)");
            //columns.Add("post_code", "Text(255)");
            //columns.Add("post", "Text(50)");
            //columns.Add("email", "Text(100)");
            //columns.Add("phone1", "Text(20)");
            //columns.Add("phone2", "Text(20)");
            //columns.Add("phone3", "Text(20)");
            //columns.Add("creation_date", "DateTime");
            //columns.Add("last_modify", "DateTime");
            //columns.Add("deleted", "Bit");
            //columns.Add("excluded", "Bit");
            //columns.Add("firstName", "Text(100)");
            //columns.Add("lastName", "Text(100)");
            //columns.Add("contact_firstname", "Text(100)");
            //columns.Add("contact_lastname", "Text(100)");
            //columns.Add("contact_role", "Text(100)");
            //columns.Add("updated", "Bit");

            //try 
            //{
            //    if (!CreateMissingTable(connection, "Contractors", columns))
            //    {
            //        InsertMissingField(connection, "Contractors", "firstName", "Text(100)");
            //        InsertMissingField(connection, "Contractors", "lastName", "Text(100)");
            //        InsertMissingField(connection, "Contractors", "updated", "Bit");
            //        InsertMissingField(connection, "Contractors", "nr", "Text(10)");
            //        InsertMissingField(connection, "Contractors", "flat", "Text(10)");
            //        InsertMissingField(connection, "Contractors", "post", "Text(50)");
            //        InsertMissingField(connection, "Contractors", "country", "Text(50)");
            //        InsertMissingField(connection, "Contractors", "contact_firstname", "Text(100)");
            //        InsertMissingField(connection, "Contractors", "contact_lastname", "Text(100)");
            //        InsertMissingField(connection, "Contractors", "contact_role", "Text(100)");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    logger.LogMessage("Błąd podczas tworzenia tabeli w istniejącej bazie. " + ex.Message);
            //}


            //columns.Clear();

            /*columns.Add("id", "Integer");
            columns.Add("dict_type", "Integer");
            columns.Add("name", "Text(255)");
            if(CreateMissingTable(connection, "WeeklyStatistics", columns))
            {
                CreateMissingTable(connection, "ColumnDictionary", columns);
            }
            columns.Clear();*/
            //}
        }

        //public static void InsertMissingField(OleDbConnection connection, string table_name, string col_name, string type)
        //{
        //    var schema = connection.GetSchema("COLUMNS");

        //    var col = schema.Select("TABLE_NAME='" + table_name + "' AND COLUMN_NAME='" + col_name + "'");

        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;

        //    if (col.Length == 0)
        //    {
        //        cmd.Transaction = connection.BeginTransaction();
        //        cmd.CommandText = "ALTER TABLE " + table_name + " ADD " + col_name + " " + type + "  DEFAULT NULL";
        //        cmd.ExecuteNonQuery();
        //        cmd.Transaction.Commit();
        //    }
        //}

        //public static bool CreateMissingTable(OleDbConnection connection, string table_name, Dictionary<string, string> columns)
        //{
        //    var exist = connection.GetSchema("TABLES", new string[4] { null, null, table_name, "TABLE" }).Rows.Count > 0;
        //    if(!exist)
        //    {
        //        OleDbCommand cmd = new OleDbCommand();
        //        cmd.Connection = connection;
        //        string fields = "";
        //        foreach(var elem in columns)
        //        {
        //            fields += elem.Key + " " + elem.Value;
        //            if(elem.Key != columns.Last().Key)
        //            {
        //                fields += ", ";
        //            }
        //        }
        //        cmd.CommandText = "CREATE TABLE " + table_name + " (" + fields + ")";
        //        try
        //        {
        //            cmd.ExecuteNonQuery();
        //            return true;
        //        }
        //        catch(Exception ex)
        //        {
        //            logger.LogMessage("Błąd dodawania tabeli " + table_name + ". " + ex.Message);
        //            return false;
        //        }
        //    }
        //    else
        //    {
        //        return false;
        //    }

        //}

        public static void GenerateWeeklyStatistics(OleDbConnection connection)
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = connection;
            cmd.Transaction = connection.BeginTransaction();
            int week_id = 0;

            cmd.CommandText = "SELECT MAX(week_id) FROM WeeklyStatistics";
            var result = cmd.ExecuteScalar();

            if (object.ReferenceEquals(result, null))
            {
                week_id = 1;
            }
            else
            {
                week_id += 1;
            }


            cmd.CommandText = "INSERT INTO WeeklyStatistics (week_id, dictionary_key, dictionary_value, sent, creation_date) VALUES(" + week_id + ", 'qualified', '0', 0, '" + DateTime.Now + "')";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "INSERT INTO WeeklyStatistics (week_id, dictionary_key, dictionary_value, sent, creation_date) VALUES(" + week_id + ", 'bufor', '0', 0, '" + DateTime.Now + "')";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "INSERT INTO WeeklyStatistics (week_id, dictionary_key, dictionary_value, sent, creation_date) VALUES (" + week_id + ", 'delivered', '0', 0, '" + DateTime.Now + "')";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "INSERT INTO WeeklyStatistics (week_id, dictionary_key, dictionary_value, sent, creation_date) VALUES(" + week_id + ", 'lacks', '0', 0, '" + DateTime.Now + "')";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "INSERT INTO WeeklyStatistics (week_id, dictionary_key, dictionary_value, sent, creation_date) VALUES(" + week_id + ", 'logged_in_hrs', '0', 0, '" + DateTime.Now + "')";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "INSERT INTO WeeklyStatistics (week_id, dictionary_key, dictionary_value, sent, creation_date) VALUES(" + week_id + ", 'nb_of_loggins', '0', 0, '" + DateTime.Now + "')";
            cmd.ExecuteNonQuery();

            cmd.Transaction.Commit();

        }

        //public static void GenerateColumnsDict(OleDbConnection connection)
        //{
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    int id = 1;

        //    /*cmd.CommandText = "SELECT MAX(id) FROM ColumnDictionary where dict_type = 1";
        //    var result = cmd.ExecuteScalar();

        //    if (object.ReferenceEquals(result, null))
        //    {
        //        id = 1;
        //    }
        //    else
        //    {
        //        id += 1;
        //    }*/


        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 1, 'nrfaktury')";
        //    cmd.ExecuteNonQuery();
        //    id++;
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 1, 'nrdokumentu')";
        //    cmd.ExecuteNonQuery();
        //    id++;
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 1, 'numerfaktury')";
        //    cmd.ExecuteNonQuery();
        //    id++;
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 1, 'numerdokumentu')";
        //    id++;
        //    cmd.ExecuteNonQuery();
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 1, 'numer faktury')";
        //    id++;
        //    cmd.ExecuteNonQuery();
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 1, 'numer dokumentu')";
        //    id++;
        //    cmd.ExecuteNonQuery();
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 1, 'nrdok')";
        //    id++;
        //    cmd.ExecuteNonQuery();
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 1, 'numerrdok')";
        //    cmd.ExecuteNonQuery();

        //    id = 1;
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 2, 'nrfaktury')";
        //    cmd.ExecuteNonQuery();
        //    id++;
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 2, 'nrdokumentu')";
        //    cmd.ExecuteNonQuery();
        //    id++;
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 2, 'numerfaktury')";
        //    cmd.ExecuteNonQuery();
        //    id++;
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 2, 'numerdokumentu')";
        //    id++;
        //    cmd.ExecuteNonQuery();
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 2, 'numer faktury')";
        //    id++;
        //    cmd.ExecuteNonQuery();
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 2, 'numer dokumentu')";
        //    id++;
        //    cmd.ExecuteNonQuery();
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 2, 'nrdok')";
        //    id++;
        //    cmd.ExecuteNonQuery();
        //    cmd.CommandText = "INSERT INTO ColumnDictionary (id, dict_type, name) VALUES(" + id + ", 2, 'numerrdok')";
        //    cmd.ExecuteNonQuery();


        //    cmd.Transaction.Commit();

        //}

        public static string GetLogin()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                return doc.Root.Element("login").Value;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("GetLogin error");
                return null;
            }
        }

        public static string GetPassword()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                return doc.Root.Element("password").Value;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("GetPassword error");
                return null;
            }
        }

        public static string GetRegDateAccept()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                return doc.Root.Element("Acceptation_WznsUpoo").Value;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("GetRegDateAccept error");
                return null;
            }
        }
        public static string GetLicenseAcceptation()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                return doc.Root.Element("license_acceptation").Value;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("GetLicenseAcceptation error");
                return null;
            }
        }

        public static bool GetOWUAcceptation()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                string owu = doc.Root.Element("OWU_acceptation").Value;
                if (string.IsNullOrEmpty(owu))
                {
                    return false;
                }
                else if (owu == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("GetOWUAcceptation error");
                return false;
            }
        }

        public static string GetFirstRun()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                logger.LogInfo("GetFirstRun pobrano");
                return doc.Root.Element("first_run").Value;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("GetFirstRun error");
                return null;
            }
        }

        public static string GetLastSynchro()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                return doc.Root.Element("last_synchro").Value;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("GetLastSynchro error");
                return null;
            }
        }

        public static string GetLicenseVersion()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                return doc.Root.Element("license_ver").Value;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("GetLicenseVersion error");
                return null;
            }
        }

        public static string GetEnvironment()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                return doc.Root.Element("environment").Value;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("GetEnvironment error");
                return null;
            }
        }

        public static string GetAppVersion()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                return doc.Root.Element("app_version").Value;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("GetAppVersion error");
                return null;
            }
        }

        public static string GetAppID()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                return doc.Root.Element("app_id").Value;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("GetAppID error");
                return null;
            }
        }

        public static string GetLoginTime()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                return doc.Root.Element("login_time").Value;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("GetLoginTime error");
                return null;
            }
        }

        public static string GetRegulationsId()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                return doc.Root.Element("regulations_id").Value;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("GetRegulationsId error");
                return null;
            }
        }

        public static void SetLogin(string login)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("login").Value = login;
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetLogin error");
            }
        }

        public static void SetRegDateAccept(DateTime date)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("Acceptation_WznsUpoo").Value = date.ToString();
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetLogin error");
            }
        }
        public static void SetPassword(string password)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("password").Value = password;
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetPassword error");
            }
        }

        public static void SetLicenseAcceptation(string license_acceptation)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("license_acceptation").Value = license_acceptation;
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetLicenseAcceptation error");
            }
        }

        public static void SetOWUAcceptation(string license_acceptation)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("OWU_acceptation").Value = license_acceptation;
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetOWUAcceptation error");
            }
        }

        public static void SetFirstRun(string first_run)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("first_run").Value = first_run;
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetFirstRun error");
            }
        }

        public static void SetLastSynchro(string last_synchro)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("last_synchro").Value = last_synchro;
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetLastSynchro error");
            }
        }

        public static void SetLicenseVersion(string license_version)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("license_ver").Value = license_version;
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetLicenseVersion error");
            }
        }

        public static void SetEnvironment(string environment)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("environment").Value = environment;
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetEnvironment error");
            }
        }

        public static void SetAppVersion(string app_version)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("app_version").Value = app_version;
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetAppVersion error");
            }
        }

        public static void SetAppID(string app_id)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("app_id").Value = app_id;
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetAppID error");
            }
        }

        public static void SetLoginTime(string login_time)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("login_time").Value = login_time;
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetLoginTime error");
            }
        }

        public static void SetRegulationsId(string regulations_id)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("regulations_id").Value = regulations_id;
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetRegulationsId error");
            }
        }

        public static string GetNewID()
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                var id = doc.Root.Element("last_mod_id").Value;
                doc.Root.Element("last_mod_id").Value = (Convert.ToInt32(id) + 1).ToString();
                doc.Save(fileName);
                return id;

            }
            catch (Exception e)
            {
                logger.LogException(e);
                return null;
            }
        }

        public static List<Module> GetList()
        {
            XDocument doc = null;
            try
            {
                doc = XDocument.Load(fileName);

            }
            catch (Exception ex)
            {
                logger.LogMessage("XMLConfig: GetList -> Błąd ładowania pliku FPEASY.conf \n" + ex.Message + " \n" + ex.StackTrace);

            }
            var a = doc.Root.Elements("modules");
            bool added = false;
            foreach (var elem in a.Elements())
            {
                if (object.ReferenceEquals(elem.Element("bank_account"), null))
                {
                    elem.Add(new XElement("bank_account"));
                    added = true;
                }

                if (object.ReferenceEquals(elem.Element("branch"), null))
                {
                    elem.Add(new XElement("branch"));
                    added = true;
                }
            }

            List<Module> list = (
            from m in doc.Root.Elements("modules").Elements("module")
            select new Module(
                m.Attribute("id").Value,
                m.Element("title").Value,
                m.Element("identstr").Value,
                m.Element("user").Value,
                m.Element("password").Value,
                m.Element("database").Value,
                m.Element("sql_server").Value,
                m.Element("export_dir").Value,
                m.Element("admin_login").Value,
                m.Element("admin_pass").Value,
                m.Element("company_id").Value,
                m.Element("company_name").Value,
                m.Element("big_settings").Value,
                m.Element("synchro_time").Value,
                m.Element("bank_account").Value,
                m.Element("branch").Value
                )).ToList<Module>();

            if (added)
            {
                try
                {
                    SetList(list);
                }
                catch (Exception ex)
                {
                    logger.LogMessage("XMLConfig: GetList -> SetList -> Błąd zapisu do pliku FPEASY.conf \n" + ex.Message + " \n" + ex.StackTrace);
                }
            }
            return list;
        }

        public static void SetList(List<Module> list)
        {
            XElement dos = new XElement("modules",
            from m in list
            select
                new XElement("module",
                new XAttribute("id", m.id),
                new XElement("title", m.title),
                new XElement("identstr", m.identstr),
                new XElement("user", m.login),
                new XElement("password", m.pass),
                new XElement("database", m.database),
                new XElement("sql_server", m.server),
                new XElement("export_dir", m.export_dir),
                new XElement("admin_login", m.admin_login),
                new XElement("admin_pass", m.admin_pass),
                new XElement("company_id", m.company_id),
                new XElement("company_name", m.company_name),
                new XElement("big_settings", m.big_settings),
                new XElement("synchro_time", m.synchro_time),
                new XElement("bank_account", m.bank_account),
                new XElement("branch", m.branch)
                ));
            try
            {
                XDocument doc = XDocument.Load(fileName);
                doc.Root.Element("modules").Remove();
                doc.Root.Add(dos);
                doc.Save(fileName);
            }
            catch (Exception ex)
            {
                logger.LogMessage("XMLConfig: SetList -> Błąd zapisu do pliku FPEASY.conf \n" + ex.Message + " \n" + ex.StackTrace);
            }



        }

        public static void SetModuleExportDir(Module module)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                var mlist = doc.Root.Element("modules");
                foreach (var m in mlist.Elements("module"))
                {
                    if (m.Attribute("id").Value.ToString() == module.id)
                    {
                        m.Element("export_dir").Value = module.export_dir;
                        break;
                    }
                }
                doc.Save(fileName);
            }
            catch (Exception e)
            {
                logger.LogException(e);
                logger.LogInfo("SetLoginTime error");
            }
        }


    }
}
