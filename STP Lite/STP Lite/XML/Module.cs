﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace STP_Lite.XML
{
    public class Module
    {
        [XmlAttribute("Id")]
        public string id { get; set; }
        [XmlAttribute("Title")]
        public string title { get; set; }
        [XmlAttribute("Identstr")]
        public string identstr { get; set; }
        [XmlAttribute("User")]
        public string login { get; set; }
        [XmlAttribute("Password")]
        public string pass { get; set; }
        [XmlAttribute("database")]
        public string database { get; set; }
        [XmlAttribute("sql_server")]
        public string server { get; set; }
        [XmlAttribute("export_dir")]
        public string export_dir { get; set; }
        [XmlAttribute("admin_login")]
        public string admin_login { get; set; }
        [XmlAttribute("admin_pass")]
        public string admin_pass { get; set; }
        [XmlAttribute("company_id")]
        public string company_id { get; set; }
        [XmlAttribute("company_name")]
        public string company_name { get; set; }
        [XmlAttribute("big_settings")]
        public string big_settings { get; set; }
        [XmlAttribute("synchro_time")]
        public string synchro_time { get; set; }
        [XmlAttribute("bank_account")]
        public string bank_account { get; set; }
        [XmlAttribute("branch")]
        public string branch { get; set; }

        public Module()
        {
            this.id = XMLConfig.GetNewID();
            this.title = "";
            this.identstr = "";
            this.login = "";
            this.pass = "";
            this.database = "";
            this.server = "";
            this.export_dir = "";
            this.admin_login = "";
            this.admin_pass = "";
            this.company_id = "";
            this.company_name = "";
            //this.big_settings = (BIGSettings.POSITIVE_7 | BIGSettings.POSITIVE_759 | BIGSettings.NEGATIVE_59).ToString();
            int hour = DateTime.Now.Hour;
            string shour = "";
            int minute = DateTime.Now.Minute;
            string sminute = "";
            this.bank_account = "";
            this.branch = "0";

            if (hour < 10)
            {
                shour = "0";
            }
            shour += hour.ToString();

            if (minute < 10)
            {
                sminute = "0";
            }
            sminute += minute.ToString();
            string time = shour + ":" + sminute;

            this.synchro_time = time;
        }

        public Module(string id, string title, string identstr, string login, string pass, string database, string server, string export_dir, string admin_login, string admin_pass, string company_id, string company_name, string bank_account, string branch)
        {
            this.id = id;
            this.title = title;
            this.identstr = identstr;
            this.login = login;
            this.pass = pass;
            this.database = database;
            this.server = server;
            this.export_dir = export_dir;
            this.admin_login = admin_login;
            this.admin_pass = admin_pass;
            this.company_id = company_id;
            this.company_name = company_name;
            //this.big_settings = (BIGSettings.POSITIVE_7 | BIGSettings.POSITIVE_759 | BIGSettings.NEGATIVE_59).ToString();

            int hour = DateTime.Now.Hour;
            string shour = "";
            int minute = DateTime.Now.Minute;
            string sminute = "";

            if (hour < 10)
            {
                shour = "0";
            }
            shour += hour.ToString();

            if (minute < 10)
            {
                sminute = "0";
            }
            sminute += minute.ToString();
            string time = shour + ":" + sminute;

            this.synchro_time = time;
            this.bank_account = bank_account;
            this.branch = branch;
        }

        public Module(string id, string title, string identstr, string login, string pass, string database, string server, string export_dir, string admin_login, string admin_pass, string company_id, string company_name, string big_settings, string synchro_time, string bank_account, string branch)
        {
            this.id = id;
            this.title = title;
            this.identstr = identstr;
            this.login = login;
            this.pass = pass;
            this.database = database;
            this.server = server;
            this.export_dir = export_dir;
            this.admin_login = admin_login;
            this.admin_pass = admin_pass;
            this.company_id = company_id;
            this.company_name = company_name;
            this.big_settings = big_settings;
            this.synchro_time = synchro_time;
            this.bank_account = bank_account;
            this.branch = branch;
        }
    }
}
