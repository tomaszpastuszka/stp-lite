﻿using STP_Lite.BIR;
using STP_Lite.Structures;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;

namespace STP_Lite.GUS
{
    class GUSApi
    {
        public static string clientKEY = "d80e1a8053c64f688b0a";    //Klucz do usługi BIR1 w GUS
        public string sid = "";        
        public BIR.UslugaBIRzewnPublClient client = null;
        public System.ServiceModel.Channels.HttpRequestMessageProperty requestMessage = new System.ServiceModel.Channels.HttpRequestMessageProperty();
        MainWindow mwindow = null;

        public GUSApi(MainWindow mwindow)
        {
            this.mwindow = mwindow;
            client = GetBIRclient("https://Wyszukiwarkaregon.stat.gov.pl/wsBIR/UslugaBIRzewnPubl.svc");
                        
            if (string.IsNullOrEmpty(sid))
            {
                try
                {
                    sid = client.Zaloguj(clientKEY);
                }
                catch (Exception ex)
                {
                    mwindow.logger.LogMessage("GUSApi: Pierwsze logowanie. Błąd: \n" + ex.Message);
                }
                
            }
            
        }

        

        private BIR.UslugaBIRzewnPublClient GetBIRclient(string url)
        {
            WSHttpBinding binding = new WSHttpBinding();
            binding.MessageEncoding = WSMessageEncoding.Mtom;
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            EndpointAddress ea = new EndpointAddress(url);

            client = new BIR.UslugaBIRzewnPublClient(binding, ea);
            return client;
        }

        public bool Login()
        {
            try
            {
                this.sid = client.Zaloguj(clientKEY);
            }
            catch (Exception ex)
            {
                mwindow.logger.LogMessage("GUSApi: Logowanie. Błąd: \n" + ex.Message);
            }

            if (string.IsNullOrEmpty(sid))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public BitmapImage GetCaptcha()
        {
            bool tryAgain = false;
            try
            {
                using (new OperationContextScope(client.InnerChannel))
                {
                    requestMessage = new System.ServiceModel.Channels.HttpRequestMessageProperty();
                    requestMessage.Headers["sid"] = sid;
                    OperationContext.Current.OutgoingMessageProperties[System.ServiceModel.Channels.HttpRequestMessageProperty.Name] = requestMessage;

                    string strCaptchaBASE644 = client.PobierzCaptcha();
                    if (!string.IsNullOrEmpty(strCaptchaBASE644) && strCaptchaBASE644 != "-1")
                    {
                        byte[] imageBytes = Convert.FromBase64String(strCaptchaBASE644);
                        MemoryStream ms = new MemoryStream(imageBytes);
                        BitmapImage bi = new BitmapImage();
                        bi.BeginInit();
                        bi.StreamSource = ms;
                        bi.EndInit();
                        //Image captcha = Image.FromStream(ms, true, true);
                        return bi;
                    }
                    else if (strCaptchaBASE644 != "-1")
                    {
                        if (Login())
                        {
                            tryAgain = true;
                            using (new OperationContextScope(client.InnerChannel))
                            {
                                requestMessage = new System.ServiceModel.Channels.HttpRequestMessageProperty();
                                requestMessage.Headers["sid"] = sid;
                                OperationContext.Current.OutgoingMessageProperties[System.ServiceModel.Channels.HttpRequestMessageProperty.Name] = requestMessage;

                                strCaptchaBASE644 = client.PobierzCaptcha();
                                if (!string.IsNullOrEmpty(strCaptchaBASE644))
                                {
                                    byte[] imageBytes = Convert.FromBase64String(strCaptchaBASE644);
                                    MemoryStream ms = new MemoryStream(imageBytes);
                                    BitmapImage bi = new BitmapImage();
                                    bi.BeginInit();
                                    bi.StreamSource = ms;
                                    bi.EndInit();
                                    //Image captcha = Image.FromStream(ms, true, true);
                                    return bi;
                                }

                            }
                        }
                        return null;
                    }
                    else
                    {
                        return null;
                    }
                }

            }
            catch (Exception ex)
            {
                mwindow.logger.LogMessage("Błąd pobierania captcha. sid = " + sid + "\"  \n" + ex.Message);
                return null;
            }

        }

        public bool CheckCaptcha(string captcha_to_check)
        {
            bool res = false;
            try
            {
                using (new OperationContextScope(client.InnerChannel))
                {
                    requestMessage = new System.ServiceModel.Channels.HttpRequestMessageProperty();
                    requestMessage.Headers["sid"] = sid;
                    OperationContext.Current.OutgoingMessageProperties[System.ServiceModel.Channels.HttpRequestMessageProperty.Name] = requestMessage;

                    if (client.SprawdzCaptcha(captcha_to_check))
                    {
                        res = true;
                    }
                    else
                    {
                        res = false;
                    }
                    
                }

            }
            catch (Exception ex)
            {
                mwindow.logger.LogMessage("Błąd sprawdzania captcha. sid = " + sid + "\"  \n" + ex.Message);
                res = false;
            }

            return res;
        }

        public XDocument GetDane(string nip)
        {
            XDocument xml;
            string dane  =  "";
            try
            {
                using (new OperationContextScope(client.InnerChannel))
                {
                    requestMessage = new System.ServiceModel.Channels.HttpRequestMessageProperty();
                    requestMessage.Headers["sid"] = sid;
                    OperationContext.Current.OutgoingMessageProperties[System.ServiceModel.Channels.HttpRequestMessageProperty.Name] = requestMessage;

                    ParametryWyszukiwania parameters = new ParametryWyszukiwania();
                    parameters.Nip = nip;
                    dane = client.DaneSzukaj(parameters);
                    if (!string.IsNullOrEmpty(dane))
                    {
                        xml = XDocument.Parse(dane);
                        return xml;
                    }
                    else
                    {
                        return null;
                    }
                }

            }
            catch (Exception ex)
            {
                mwindow.logger.LogMessage("Błąd sprawdzania captcha. sid = " + sid + "\"  \n" + ex.Message);
                return null; 
            }



            
            
        }

        public XDocument GetFullRaport(string regon, string raportName)
        {
            XDocument xml;
            string dane = "";
            try
            {
                using (new OperationContextScope(client.InnerChannel))
                {
                    requestMessage = new System.ServiceModel.Channels.HttpRequestMessageProperty();
                    requestMessage.Headers["sid"] = sid;
                    OperationContext.Current.OutgoingMessageProperties[System.ServiceModel.Channels.HttpRequestMessageProperty.Name] = requestMessage;

                    dane = client.DanePobierzPelnyRaport(regon, raportName);
                    if (!string.IsNullOrEmpty(dane))
                    {
                        xml = XDocument.Parse(dane);
                        return xml;
                    }
                    else
                    {
                        return null;
                    }
                }

            }
            catch (Exception ex)
            {
                mwindow.logger.LogMessage("Błąd pobierania raportu. sid = " + sid + "\"  \n" + ex.Message);
                return null;
            }

            
        }

        public bool SessionExist()
        {
            using (new OperationContextScope(client.InnerChannel))
            {
                requestMessage = new System.ServiceModel.Channels.HttpRequestMessageProperty();
                requestMessage.Headers["sid"] = sid;
                OperationContext.Current.OutgoingMessageProperties[System.ServiceModel.Channels.HttpRequestMessageProperty.Name] = requestMessage;
                string r = client.GetValue("StatusSesji");
                if(r == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
        }
        public bool RequiredCaptcha()
        {
            using (new OperationContextScope(client.InnerChannel))
            {
                requestMessage = new System.ServiceModel.Channels.HttpRequestMessageProperty();
                requestMessage.Headers["sid"] = sid;
                OperationContext.Current.OutgoingMessageProperties[System.ServiceModel.Channels.HttpRequestMessageProperty.Name] = requestMessage;
                string r = client.GetValue("KomunikatKod");
                if (r == "1")
                {
                    return true;
                }
                else
                {
                    return true;
                }

            }
        }
        public bool ServiceAvailaible()
        {
            string res = client.GetValue("StatusUslugi");
            if (res == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
