﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STP_Lite.Structures;
using STP_Lite.pl.krd.demo.lucy;
using STP_Lite.pl.krd.demo.siddin;
using System.Xml.Serialization;
using System.IO;
using STP_Lite.KrdStructures;
using System.Xml;
using STP_Lite.Measurers;
using System.Globalization;
using STP_Lite.Veta;



namespace STP_Lite.KRD
{
    public class KRDApi
    {
        FairPayService client = new FairPayService();
        ApiMeasurer measurer = new ApiMeasurer();
        Import importClient = new Import();
        Regulation regulation = new Regulation();
        Logger logger = new Logger();
        public string Ticket = "";
        public DateTime LastLogin = DateTime.Now;
        string publicip = "";
        private System.Windows.Forms.Timer tLogin = new System.Windows.Forms.Timer();
        private List<VetaValidationError> vetaerrors = new List<VetaValidationError>();

        private void FillVetaErrors()
        {
            vetaerrors.Add(new VetaValidationError("Add obligation command is required", "Wymagana komenda dodania zobowiązania"));
            vetaerrors.Add(new VetaValidationError("Address building is too long", "Zbyt długi numer budynku"));
            vetaerrors.Add(new VetaValidationError("Address city is too long", "Zbyt długa nazwa miasta"));
            vetaerrors.Add(new VetaValidationError("Address country code is invalid", "Niepoprawny kod państwa"));
            vetaerrors.Add(new VetaValidationError("Address flat is too long", "Zbyt długi numer mieszkania"));
            vetaerrors.Add(new VetaValidationError("Address post is required", "Brak poczty"));
            vetaerrors.Add(new VetaValidationError("Address post is too long", "Zbyt długa nazwa poczty"));
            vetaerrors.Add(new VetaValidationError("Address street is too long", "Zbyt długa nazwa ulicy"));
            vetaerrors.Add(new VetaValidationError("Address street or city is required", "Brak ulicy lub miasta"));
            vetaerrors.Add(new VetaValidationError("Address zip code is invalid", "Niepoprawny kod pocztowy"));
            vetaerrors.Add(new VetaValidationError("Address zip code is required", "Brak kodu pocztowego"));
            vetaerrors.Add(new VetaValidationError("Address zip code is too long", "Zbyt długi kod pocztowy"));
            vetaerrors.Add(new VetaValidationError("Customer identity number is too long", "Numer identyfikacyjny klienta jest zbyt długi"));
            vetaerrors.Add(new VetaValidationError("Customer identity number is invalid", "Niepoprawny numer identyfikacyjny klienta"));
            vetaerrors.Add(new VetaValidationError("Customer identity number is required", "Brak numeru identyfikacyjnego klienta"));
            vetaerrors.Add(new VetaValidationError("Customer identity number type is not supported", "Nieobsługiwany numer identyfikacyjny klienta"));
            vetaerrors.Add(new VetaValidationError("Contact email is invalid", "Niepoprawny email osoby kontaktowej"));
            vetaerrors.Add(new VetaValidationError("Contact email is required", "Brak adresu email osoby kontaktowej"));
            vetaerrors.Add(new VetaValidationError("Contact email is too long", "Zbyt długi adres email"));
            vetaerrors.Add(new VetaValidationError("Contact first name is required", "Brak imienia osoby kontaktowej"));
            vetaerrors.Add(new VetaValidationError("Contact first name is too long", "Zbyt długie imię osoby kontaktowej"));
            vetaerrors.Add(new VetaValidationError("Contact phone is invalid", "Niepoprawny nr telefonu osoby kontaktowej"));
            vetaerrors.Add(new VetaValidationError("Contact phone is required", "Brak nr telefonu osoby kontaktowej"));
            vetaerrors.Add(new VetaValidationError("Contact phone is too long", "Zbyt długi nr telefonu osoby kontaktowej"));
            vetaerrors.Add(new VetaValidationError("Contact role is too long", "Zbyt długa nazwa stanowiska osoby kontaktowej"));
            vetaerrors.Add(new VetaValidationError("Contact surname is required", "Brak nazwiska osoby kontaktowej"));
            vetaerrors.Add(new VetaValidationError("Contact surname is too long", "Zbyt długie nazwisko osoby kontaktowej"));
            vetaerrors.Add(new VetaValidationError("Contractor branch is required", "Brak branży osoby kontaktowej"));
            vetaerrors.Add(new VetaValidationError("Contractor company name is required", "Brak nazwy firmy osoby kontaktowej"));
            vetaerrors.Add(new VetaValidationError("Contractor company name is too long", "Zbyt długa nazwa firmy osoby kontaktowej"));
            vetaerrors.Add(new VetaValidationError("At least one contractor contact is required", "Przynajmniej jedna osoba kontaktowa jest wymagana"));
            vetaerrors.Add(new VetaValidationError("Contractor non customer identity number is required", "Wymagany numer identyfikacji podatkowej kontrahenta"));
            vetaerrors.Add(new VetaValidationError("Contractor seat address is required", "Brak adresu siedziby kontrahenta"));
            vetaerrors.Add(new VetaValidationError("At least one contractor polish address is required", "Przynajmniej jeden polski adres kontrahenta jest wymagany"));
            vetaerrors.Add(new VetaValidationError("Contractor consent request data email is invalid", "Niepoprawny email w żądaniu dotyczącym zgody kontrahenta"));
            vetaerrors.Add(new VetaValidationError("Contractor consent request data email is required", "Brak adresu email w żądaniu dotyczącym zgody kontrahenta"));
            vetaerrors.Add(new VetaValidationError("Contractor consent request data email is too long", "Zbyt długi adres email w żądaniu dotyczącym zgody kontra"));
            vetaerrors.Add(new VetaValidationError("Contractor consent request data name is invalid", "Niepoprawna nazwa w żądaniu dotyczącym zgody kontrahenta"));
            vetaerrors.Add(new VetaValidationError("Contractor consent request data name is required", "Brak nazwy w żądaniu dotyczącym zgody kontrahenta"));
            vetaerrors.Add(new VetaValidationError("Contractor consent request data name is too long", "Zbyt długa nazwa w żądaniu dotyczącym zgody kontrahenta"));
            vetaerrors.Add(new VetaValidationError("Contractor consent request data phone is invalid", "Niepoprawny telefon w żądaniu dotyczącym zgody kontrahenta"));
            vetaerrors.Add(new VetaValidationError("Contractor consent request data phone is required", "Brak telefonu w żądaniu dotyczącym zgody kontrahenta"));
            vetaerrors.Add(new VetaValidationError("Contractor consent request data phone is too long", "Zbyt długi telefon w żądaniu dotyczącym zgody kontrahenta"));
            vetaerrors.Add(new VetaValidationError("Document scan file content is required", "Wymagany skan dokumentu w załączniku"));
            vetaerrors.Add(new VetaValidationError("Document scan file name is required", "Wymagana nazwa pliku skanu dokumentu"));
            vetaerrors.Add(new VetaValidationError("Document scan file name is too long", "Zbyt długa nazwa pliku skanu dokumentu"));
            vetaerrors.Add(new VetaValidationError("Emails in contacts must be unique", "Email osoby kontaktowej musi być unikalny"));
            vetaerrors.Add(new VetaValidationError("Entrepreneur contractor company name must contain first name and surname", "Nazwa firmy przedsiębiorcy musi zawierać imię i nazwsko"));
            vetaerrors.Add(new VetaValidationError("Entrepreneur contractor first name is required", "Brak imienia przedsiębiorcy"));
            vetaerrors.Add(new VetaValidationError("Entrepreneur contractor first name is too long", "Zbyt długi imię przedsiębiorcy"));
            vetaerrors.Add(new VetaValidationError("Entrepreneur contractor surname is required", "Brak nazwiska przedsiębiorcy"));
            vetaerrors.Add(new VetaValidationError("Entrepreneur contractor surname is too long", "Zbyt długi nazwisko przedsiębiorcy"));
            vetaerrors.Add(new VetaValidationError("System ID is too long", "Zbyt długi System ID"));
            vetaerrors.Add(new VetaValidationError("User ID is too long", "Zbyt długi User ID"));
            vetaerrors.Add(new VetaValidationError("Non consumer identity number custom type is too long", "Numer identyfikacyjny firmy jest za długi"));
            vetaerrors.Add(new VetaValidationError("Non consumer identity number is invalid", "Numer identyfikacyjny firmy jest niepoprawny"));
            vetaerrors.Add(new VetaValidationError("Non consumer identity number is required", "Numer identyfikacyjny firmy jest wymagany"));
            vetaerrors.Add(new VetaValidationError("Non consumer identity number is too long", "Numer identyfikacyjny firmy jest za długi"));
            vetaerrors.Add(new VetaValidationError("Non consumer identity number type only support tax ID", "Jako numer identyfikacyjny można podać tylko numer identyfikacji podatkowej"));
            vetaerrors.Add(new VetaValidationError("Obligation arrears must be greater than zero", "Zaległosci zobowiązania muszą byś większe od 0"));
            vetaerrors.Add(new VetaValidationError("Obligation bank account is invalid", "Niepoprawny nr konta bankowego zobowiązania"));
            vetaerrors.Add(new VetaValidationError("Obligation bank account is required", "Brak nr konta bankowego zobowiązania"));
            vetaerrors.Add(new VetaValidationError("Obligation contractor is required", "Brak kontrahenta zobowiązania"));
            vetaerrors.Add(new VetaValidationError("Invalid length of obligation custom document type", "Niepoprawna długość typu dokumentu"));
            vetaerrors.Add(new VetaValidationError("Obligation delivery date can not ber from future", "Data dostarczenia zobowiązania nie może być z przyszłości"));
            vetaerrors.Add(new VetaValidationError("Obligation delivery date is required", "Brak daty dostarczenia zobowiązania"));
            vetaerrors.Add(new VetaValidationError("Obligation document creation date can not be from future", "Data utworzenia zobowiązania nie może być z przyszłości"));
            vetaerrors.Add(new VetaValidationError("Obligation document creation date is required", "Brak daty utworzenia zobowiązania"));
            vetaerrors.Add(new VetaValidationError("Invalid length of obligation document number", "Niepoprawna długość numeru dokumentu zobowiązania"));
            vetaerrors.Add(new VetaValidationError("Obligation document number is required", "Brak numeru dokumentu zobowiązania"));
            vetaerrors.Add(new VetaValidationError("Obligation due date is invalid", "Niepoprawny termin spłaty zobowiązania"));
            vetaerrors.Add(new VetaValidationError("Obligation due date is required", "Brak terminu spłaty zobowiązania"));
            vetaerrors.Add(new VetaValidationError("Obligation total amount must be greater than zero", "Całkowita kwota zobowiązania musi być większa od 0"));
            vetaerrors.Add(new VetaValidationError("Obligation total net amount is greater than total amount", "Całkowita kwota netto zobowiązania jest większa niż całkowita kwota zobowiązania"));
            vetaerrors.Add(new VetaValidationError("Obligation total net amount is less than total amount divided by tax factor", "Całkowita kwota netto jest mniejsza od cłakowitej kwoty zobowiązania podzielonej przez współczynnik podatkowy"));
            vetaerrors.Add(new VetaValidationError("Obligation full payment contractor consent data must be provided", "Obligation full payment contractor consent data must be provided"));
            vetaerrors.Add(new VetaValidationError("Obligation full payment paid date is incorrect", "Niepoprawna data pełnej spłaty zobowiązania"));
            vetaerrors.Add(new VetaValidationError("Obligation partial payment amount must be greater than zero", "Kwota częściowej spłaty zobowiązania musi być większa od 0"));
            vetaerrors.Add(new VetaValidationError("Obligation partial payment paid date is incorrect", "Niepoprawna data częściowej spłaty zobowiązania"));
            vetaerrors.Add(new VetaValidationError("Regulations acceptance date is invalid", "Niepoprawna data akceptacji regulaminu"));
            vetaerrors.Add(new VetaValidationError("Regulations acceptance date must be less than or equal now", "Data akceptacji regulaminu musi być mniejsza lub równa dzisiejsze dacie"));
            vetaerrors.Add(new VetaValidationError("Regulations Client IP is too long", "Zbyt długi adres IP klienta"));
            vetaerrors.Add(new VetaValidationError("Regulations ID is required", "Brak ID regulaminu"));
            vetaerrors.Add(new VetaValidationError("Regulations acceptance source system IP is required", "Brak systemu źródłowego IP Akceptacji regulaminu"));
            vetaerrors.Add(new VetaValidationError("Regulations acceptance source system IP is too long", "Zbyt długi system źródłowy IP akceptacji regulaminu"));
            vetaerrors.Add(new VetaValidationError("Representative first name is required", "Brak imienia osoby reprezentującej"));
            vetaerrors.Add(new VetaValidationError("Representative first name is too long", "Zbyt długie imię osoby reprezentującej"));
            vetaerrors.Add(new VetaValidationError("Representative position is required", "Brak pozycji osoby reprezentującej"));
            vetaerrors.Add(new VetaValidationError("Representative position is too long", "Zbyt długa nazwa pozycji osoby reprezentującej"));
            vetaerrors.Add(new VetaValidationError("Representative second name is too long", "Zbyt długie drugie imię osoby reprezentującej"));
            vetaerrors.Add(new VetaValidationError("Representative surname is required", "Brak nazwiska osoby reprezentującej"));
            vetaerrors.Add(new VetaValidationError("Representative surname is too long", "Zbyt długie nazwisko osoby reprezentującej"));

        }
        
        public KRDApi(int environment)
        {
            FillVetaErrors();
            tLogin.Enabled = false;
            tLogin.Interval = 600000;
            tLogin.Tick += new System.EventHandler(tLogin_Tick);
            var publicip = Utility.GetPublicIP();
            logger.LoggerStart();
            if(publicip.status == 1)
            {
                logger.LogInfo("Pobrano adres IP: " + publicip.msg);
            }
            else
            {
                logger.LogMessage("Błąd pobierania publicznego adresu IP. " + publicip.msg);
            }
            ChangeEnvironment(environment);
        }
       
        public string SerializeData(Input input)
        {       
            XmlSerializer xmlSerializer = new XmlSerializer(input.GetType());
            StringWriter writer = new StringWriter();   
            xmlSerializer.Serialize(writer, input);
            return writer.ToString();
        }

        public Output DeserializeData(string xml)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Output));
            StringReader reader = new StringReader(xml);
            Output output = (Output)xmlSerializer.Deserialize(reader);
            return output;
        }

        public ChunkBag UploadChunkRequest(string xml)
        {
            ChunkBag chunkBag = null;
            int actual_char = 0;
            int max_chunk_size = 300;

            for (actual_char = 0; actual_char < xml.Length; )
            {
                UpladChunkRequest uploadChunkRequest;

                uploadChunkRequest = new UpladChunkRequest()
                {
                    ChunkBag = chunkBag,
                    Ticket = this.Ticket,
                    Data = xml.Substring(actual_char, actual_char + max_chunk_size > xml.Length ? xml.Length - actual_char : max_chunk_size)

                };
                actual_char = actual_char + max_chunk_size;
                try
                {
                    chunkBag = importClient.UploadChunk(uploadChunkRequest);
                }
                catch (Exception ex)
                {
                    logger.LogMessage("Błąd w fukcji UploadChunkRequest. " + ex.Message);
                }
            }
            return chunkBag;       
        }

        public Guid CloseChunkBagEx(ChunkBag chunkBag)
        {
            CloseChunkBagRequestEx closechunkBagEx = new CloseChunkBagRequestEx();

            closechunkBagEx.ChunkBagEx = new ChunkBagEx();
            closechunkBagEx.Ticket = this.Ticket;
            closechunkBagEx.ChunkBagEx.ID = new Guid(chunkBag.ID.ToByteArray());
            closechunkBagEx.ChunkBagEx.Size = chunkBag.Size;
            closechunkBagEx.ChunkBagEx.ProtocolType = ProtocolEnum.Veta;
            closechunkBagEx.ChunkBagEx.ProtocolVersion = ProtocolVersionEnum.Version_1_0;
            try
            {
                var result = importClient.CloseChunkBagEx(closechunkBagEx);
                return result;
            }
            catch (Exception ex)
            {
                logger.LogMessage(ex.Message);
                return Guid.Empty;
            }
        }

        public Regulation LoginToKrdService(string login, string password)
        {
            client.AuthorizationValue = new Authorization();
            client.AuthorizationValue.AuthorizationType = AuthorizationType.LoginAndPasswordHash;
            client.AuthorizationValue.Login = login;
            client.AuthorizationValue.PasswordHash = password;
            regulation = new Regulation();
            try
            {
                client.GetRights(out regulation.RegulationAccepted, out regulation.RegulationAcceptedSpecified, out regulation.CanManageObligations, out regulation.CanManageObligationsSpecified, out regulation.HasContractOriginal, out regulation.HasContractOriginalSpecified);
                this.Ticket = client.TicketId.Text[0];
            }
            catch(Exception ex)
            {
                logger.LogMessage("Bład logowania. Użytkownik: " + login + ", Hasło: " + password + ". " + ex.Message);
                regulation.RegulationAccepted = false;
                regulation.RegulationAcceptedSpecified = false;
                regulation.CanManageObligations = false;
                regulation.CanManageObligationsSpecified = false;
                regulation.HasContractOriginal = false;
                regulation.HasContractOriginalSpecified = false;
            }

            string msg = "Użytkownik : \"" + login + "\" Hasło: \"" + password + "\"\n" +
                         "Rights:\n" +
                         "\tRegulationAccepted = " + regulation.RegulationAccepted.ToString() + "\n" +
                         "\tRegulationAcceptedSpecified = " + regulation.RegulationAcceptedSpecified.ToString() + "\n" +
                         "\tCanManageObligations = " + regulation.CanManageObligations.ToString() + "\n" +
                         "\tCanManageObligationsSpecified = " + regulation.CanManageObligationsSpecified.ToString() + "\n" +
                         "\tHasContractOriginal = " + regulation.HasContractOriginal.ToString() + "\n" +
                         "\tHasContractOriginalSpecified = " + regulation.HasContractOriginalSpecified.ToString() + "\n";


            logger.LogInfo(msg);

            if (regulation.RegulationAccepted && regulation.RegulationAcceptedSpecified && 
                regulation.CanManageObligations && regulation.CanManageObligationsSpecified && 
                regulation.HasContractOriginal && regulation.HasContractOriginalSpecified)
            {
                Utility.UpdateLoginNb();
                tLogin.Enabled = true;
            }

            return regulation;
        }

        public GetRegulationsResponse GetRegulation()
        {
            var regulation = client.GetRegulations();
            return regulation;
        }

        public Input AcceptRegulation(Input input, GetRegulationsResponse regulation)
        {
            input.Item.Order[0] = new informationManagementOrderType();
            input.Item.Order[0].ID = "1";

            addInformationType AddInformation = new addInformationType();
            AddInformation.verifyResult = Convert.ToBoolean(1);

            regulationsAcceptanceType RegulationsAcceptance = new regulationsAcceptanceType();
            RegulationsAcceptance.AcceptanceDate = DateTime.Now.Date;
            RegulationsAcceptance.RegulationsId = regulation.RegulationsId;

            AddInformation.Item = RegulationsAcceptance;
            input.Item.Order[0].Item = AddInformation;

            return input;
        }

        public void GetJobs()
        {
            GetJobsRequest getjobrequest = new GetJobsRequest();
            getjobrequest.GetAll = true;
            getjobrequest.Ticket = this.Ticket;

            var jobs = importClient.GetJobs(getjobrequest);
       
            if (jobs[0].Progress == 100)
            {
                try
                {
                    GetChunkBagRequestEx chunbagrequestex = new GetChunkBagRequestEx();
                    chunbagrequestex.JobId = jobs[0].JobID;
                    chunbagrequestex.Ticket = this.Ticket;
                    chunbagrequestex.ChunkSize = 1000000;
                    
                    var chunkbag = importClient.GetChunkBagEx(chunbagrequestex);
                    StringBuilder stringBuilder_xml = new StringBuilder();
                    XmlDocument xml_document = new XmlDocument();
                    for (int i = 1; i <= chunkbag.Count; i++)
                    {
                        DownloadChunkRequest downloadChunkRequest = new DownloadChunkRequest();
                        downloadChunkRequest.Number = i;
                        downloadChunkRequest.Ticket = this.Ticket;
                        downloadChunkRequest.ChunkBag = new ChunkBag();
                        downloadChunkRequest.ChunkBag.ID = chunkbag.ID;
                      
                        stringBuilder_xml.Append(importClient.DownloadChunk(downloadChunkRequest));
                        
                    }
                    string string_xml = stringBuilder_xml.ToString();
                    SaveResponseFile(string_xml, jobs[0].JobID);
                    xml_document.LoadXml(string_xml);
                    var output = DeserializeData(string_xml);
                    if (output.failCount > 0)
                    {
                        logger.LogXMLError(string_xml);
                    }

                    string type = output.Item.GetType().ToString();
                    if (type == "OutputInformationManagement")
                    {
                        OutputInformationManagement information = (OutputInformationManagement)output.Item;
                        foreach(var order in information.Order)
                        {
                            //order.
                        }
                    }
 
                    /*foreach(var order in output.Item)
                    {

                    }
                     */
                 }
                 catch (Exception ex)
                 {
                     logger.LogMessage(ex.Message);                          
                 }
            }         
        }

        public int GetJobForRegualtionAcceptance(Guid guid)
        {
            int ret = 1;
            GetJobsRequest getjobrequest = new GetJobsRequest();
            getjobrequest.GetAll = true;
            getjobrequest.Ticket = this.Ticket;

            var jobs = importClient.GetJobs(getjobrequest);
            foreach (var job in jobs)
            {
                if (job.JobID == guid)
                {
                    if (job.Progress == 100)
                    {
                        try
                        {
                            GetChunkBagRequestEx chunbagrequestex = new GetChunkBagRequestEx();
                            chunbagrequestex.JobId = jobs[0].JobID;
                            chunbagrequestex.Ticket = this.Ticket;
                            chunbagrequestex.ChunkSize = 1000000;

                            var chunkbag = importClient.GetChunkBagEx(chunbagrequestex);
                            StringBuilder stringBuilder_xml = new StringBuilder();
                            for (int i = 1; i <= chunkbag.Count; i++)
                            {
                                DownloadChunkRequest downloadChunkRequest = new DownloadChunkRequest();
                                downloadChunkRequest.Number = i;
                                downloadChunkRequest.Ticket = this.Ticket;
                                downloadChunkRequest.ChunkBag = new ChunkBag();
                                downloadChunkRequest.ChunkBag.ID = chunkbag.ID;

                                stringBuilder_xml.Append(importClient.DownloadChunk(downloadChunkRequest));

                            }
                            string string_xml = stringBuilder_xml.ToString();
                            SaveResponseFile(string_xml, guid);
                            var output = DeserializeData(string_xml);

                            if (output.failCount > 0)
                            {
                                logger.LogXMLError(string_xml);
                                ret = 2;
                            }

                            
                        }
                        catch (Exception ex)
                        {
                            logger.LogMessage(ex.Message);
                        }
                    }
                    else
                    {
                        ret = 0;
                    }
                    break;
                }
            }
            return ret;
        }

        private fullPaymentInformationType AddFullPayment(Repayment repayment, string branch)
        {
            fullPaymentInformationType fullPayment = new fullPaymentInformationType();
            if (repayment.Positive)
            {
                fullPayment.RegisterPositiveInformation = true;
                fullPayment.RegisterPositiveInformationSpecified = true;
            }
            else if(repayment.Negative)
            {
                fullPayment.RegisterPositiveInformation = false;
                fullPayment.RegisterPositiveInformationSpecified = true;
            }
            fullPayment.userID = repayment.Userid;
            fullPayment.PaidDate = repayment.paid_date;

            if (branch == "1")
            {
                fullPayment.NonConsumerProviderDataFilter = NonConsumerProviderDataFilterEnum.Branch;
            }
            else
            {
                fullPayment.NonConsumerProviderDataFilter = NonConsumerProviderDataFilterEnum.All;
            }
            fullPayment.Items = new object[1];
            fullPayment.Items[0] = true;

            return fullPayment;
        }

        private partialPaymentInformationType AddPartialPayment(Repayment repayment)
        {
            partialPaymentInformationType partialPayment = new partialPaymentInformationType();
            partialPayment.userID = repayment.Userid;
            partialPayment.PaidDate = repayment.paid_date;
            partialPayment.Amount = new moneyType();
            partialPayment.Amount.currency = repayment.amount.currency;
            partialPayment.Amount.Value = Convert.ToDecimal(repayment.amount.amount);

            return partialPayment;
        }

        public void SaveRequestFile(string content, Guid guid)
        {
            string baseDir = @"C:\Users\" + Environment.UserName + @"\Documents\FPEASY\Communication";
            if(!Directory.Exists(baseDir))
            {
                Directory.CreateDirectory(baseDir);
            }
            string file = baseDir + "\\Request_" + guid.ToString() + ".txt";
            StreamWriter w = null;
            if(!File.Exists(file))
            {
                w = new StreamWriter(File.Create(file));   
            }
            else
            {
                w = new StreamWriter(file);
            }

            if (!object.ReferenceEquals(w, null))
            {
                w.Write(content);
                w.Flush();
                w.Close();
            }
        }

        public void SaveResponseFile(string content, Guid guid)
        {
            string baseDir = @"C:\Users\" + Environment.UserName + @"\Documents\FPEASY\Communication";
            if (!Directory.Exists(baseDir))
            {
                Directory.CreateDirectory(baseDir);
            }
            string file = baseDir + "\\Response_" + guid.ToString() + ".txt";
            StreamWriter w = null;
            if (!File.Exists(file))
            {
                w = new StreamWriter(File.Create(file));
            }
            else
            {
                w = new StreamWriter(file);
            }

            if (!object.ReferenceEquals(w, null))
            {
                w.Write(content);
                w.Flush();
                w.Close();
            }
        }

        private Input AddObligations(List<Document> documents, Input input, int lastidx)
        {
            for (int i = 0; i < documents.Count; i++)
            {
                input.Item.Order[i + lastidx] = new informationManagementOrderType();
                input.Item.Order[i + lastidx].ID = (i + lastidx + 1).ToString();

                addInformationType AddInformation = new addInformationType();
                AddInformation.verifyResult = true;
                AddInformation.verifyResultSpecified = true;

                obligationInformationType obligationinfo = new obligationInformationType();
                obligationinfo.PublishCreditorData = true;
                obligationinfo.PublishCreditorDataSpecified = true;
                obligationinfo.userID = documents[i].identifiers.userId;
                //obligationinfo.DocumentType = (documentTypeEnum)documents[i].type;
                obligationinfo.DocumentNumber = documents[i].full_number;
                obligationinfo.DueDate = documents[i].due_date;
                obligationinfo.DocumentCreationDate = Convert.ToDateTime(documents[i].creation_date);
                obligationinfo.TotalAmount = new moneyType();
                obligationinfo.TotalAmount.currency = documents[i].gross_amount.currency;
                obligationinfo.TotalAmount.Value = Convert.ToDecimal(documents[i].gross_amount.amount);

                obligationinfo.TotalAmountNet = new moneyType();
                obligationinfo.TotalAmountNet.currency = documents[i].net_amount.currency;
                obligationinfo.TotalAmountNet.Value = Convert.ToDecimal(documents[i].net_amount.amount);
                obligationinfo.BankAccount = documents[i].bank_account;


                if (!documents[i].contractor.entreprenuer)
                {
                    legalPersonType legal_person = new legalPersonType();
                    legal_person.CompanyName = documents[i].contractor.name;
                   
                    legal_person.NonConsumerIdentityNumber = new nonConsumerIdentityNumberType();
                    legal_person.NonConsumerIdentityNumber.Item = documents[i].contractor.tax_number;
                    legal_person.SeatAddress = new addressType();
                    legal_person.SeatAddress.CountryCode = documents[i].contractor.address.country;

                    if (!string.IsNullOrEmpty(documents[i].contractor.address.city))
                    {
                        legal_person.SeatAddress.City = documents[i].contractor.address.city;
                    }

                    if (!string.IsNullOrEmpty(documents[i].contractor.address.street))
                    {
                        legal_person.SeatAddress.Street = documents[i].contractor.address.street;
                    }

                    if (!string.IsNullOrEmpty(documents[i].contractor.address.building))
                    {
                        legal_person.SeatAddress.Building = documents[i].contractor.address.building;
                    }

                    if (!string.IsNullOrEmpty(documents[i].contractor.address.flat))
                    {
                        legal_person.SeatAddress.Flat = documents[i].contractor.address.flat;
                    }

                    if (!string.IsNullOrEmpty(documents[i].contractor.address.post))
                    {
                        legal_person.SeatAddress.Post = documents[i].contractor.address.post;
                    }
                    legal_person.SeatAddress.ZipCode = documents[i].contractor.address.post_code;

                    legal_person.Contacts = new contactType[1];
                    legal_person.Contacts[0] = new contactType();
                    if (!string.IsNullOrEmpty(documents[i].contractor.contact.name))
                    {
                        legal_person.Contacts[0].FirstName = documents[i].contractor.contact.name;
                    }

                    if (!string.IsNullOrEmpty(documents[i].contractor.contact.surname))
                    {
                        legal_person.Contacts[0].Surname = documents[i].contractor.contact.surname;
                    }

                    if (!string.IsNullOrEmpty(documents[i].contractor.contact.role))
                    {
                        legal_person.Contacts[0].Role = documents[i].contractor.contact.role;
                    }
                    if (!string.IsNullOrEmpty(documents[i].contractor.contact.phone))
                    {
                        legal_person.Contacts[0].Phone = documents[i].contractor.contact.phone;
                    }
                    legal_person.Contacts[0].Email = documents[i].contractor.contact.email;

                    obligationinfo.Item = legal_person;
                }

                else
                {
                    entrepreneurType entrepreneur = new entrepreneurType();
                    entrepreneur.CompanyName = documents[i].contractor.name;
                    entrepreneur.FirstName = documents[i].contractor.firstname;
                    entrepreneur.Surname = documents[i].contractor.lastname;

                    entrepreneur.NonConsumerIdentityNumber = new nonConsumerIdentityNumberType();
                    entrepreneur.NonConsumerIdentityNumber.Item = documents[i].contractor.tax_number;
                    entrepreneur.SeatAddress = new addressType();
                    entrepreneur.SeatAddress.CountryCode = documents[i].contractor.address.country;
                    if (!string.IsNullOrEmpty(documents[i].contractor.address.city))
                    {
                        entrepreneur.SeatAddress.City = documents[i].contractor.address.city;
                    }

                    if (!string.IsNullOrEmpty(documents[i].contractor.address.street))
                    {
                        entrepreneur.SeatAddress.Street = documents[i].contractor.address.street;
                    }

                    if (!string.IsNullOrEmpty(documents[i].contractor.address.building))
                    {
                        entrepreneur.SeatAddress.Building = documents[i].contractor.address.building;
                    }

                    if (!string.IsNullOrEmpty(documents[i].contractor.address.flat))
                    {
                        entrepreneur.SeatAddress.Flat = documents[i].contractor.address.flat;
                    }

                    if (!string.IsNullOrEmpty(documents[i].contractor.address.post))
                    {
                        entrepreneur.SeatAddress.Post = documents[i].contractor.address.post;
                    }
                    entrepreneur.SeatAddress.ZipCode = documents[i].contractor.address.post_code;

                    entrepreneur.Contacts = new contactType[1];
                    entrepreneur.Contacts[0] = new contactType();
                    if (!string.IsNullOrEmpty(documents[i].contractor.contact.name))
                    {
                        entrepreneur.Contacts[0].FirstName = documents[i].contractor.contact.name;
                    }

                    if (!string.IsNullOrEmpty(documents[i].contractor.contact.name))
                    {
                        entrepreneur.Contacts[0].Surname = documents[i].contractor.contact.surname;
                    }

                    if (!string.IsNullOrEmpty(documents[i].contractor.contact.role))
                    {
                        entrepreneur.Contacts[0].Role = documents[i].contractor.contact.role;
                    }
                    if (!string.IsNullOrEmpty(documents[i].contractor.contact.phone))
                    {
                        entrepreneur.Contacts[0].Phone = documents[i].contractor.contact.phone;
                    }
                    entrepreneur.Contacts[0].Email = documents[i].contractor.contact.email;

                    obligationinfo.Item = entrepreneur;
                }

                AddInformation.Item = obligationinfo;
                input.Item.Order[i + lastidx].Item = AddInformation;
            }
            return input;
        }

        private Input AddObligationLite(Document document, Input input, int id)
        {
            int order_id = 0;
            if (id > 1)
            {
                order_id = 1;
            }
           
                input.Item.Order[order_id] = new informationManagementOrderType();
                input.Item.Order[order_id].ID = id.ToString();

                addInformationType AddInformation = new addInformationType();
                AddInformation.verifyResult = true;
                AddInformation.verifyResultSpecified = true;

                obligationInformationType obligationinfo = new obligationInformationType();
                obligationinfo.PublishCreditorData = true;
                obligationinfo.PublishCreditorDataSpecified = true;
                obligationinfo.userID = document.identifiers.userId;
                obligationinfo.DocumentType = documentTypeEnum.Invoice;
                obligationinfo.DocumentNumber = document.full_number;
                obligationinfo.DueDate = document.due_date;
                obligationinfo.DocumentCreationDate = Convert.ToDateTime(document.creation_date);
                obligationinfo.TotalAmount = new moneyType();
                obligationinfo.TotalAmount.currency = document.gross_amount.currency;
                obligationinfo.TotalAmount.Value = Convert.ToDecimal(document.gross_amount.amount);

                obligationinfo.TotalAmountNet = new moneyType();
                obligationinfo.TotalAmountNet.currency = document.net_amount.currency;
                obligationinfo.TotalAmountNet.Value = Convert.ToDecimal(document.net_amount.amount);
                obligationinfo.BankAccount = document.bank_account;


                if (!document.contractor.entreprenuer)
                {
                    legalPersonType legal_person = new legalPersonType();
                    legal_person.CompanyName = document.contractor.name;

                    legal_person.NonConsumerIdentityNumber = new nonConsumerIdentityNumberType();
                    legal_person.NonConsumerIdentityNumber.Item = document.contractor.tax_number;
                    legal_person.SeatAddress = new addressType();
                    legal_person.SeatAddress.CountryCode = document.contractor.address.country;

                    if (!string.IsNullOrEmpty(document.contractor.address.city))
                    {
                        legal_person.SeatAddress.City = document.contractor.address.city;
                    }

                    if (!string.IsNullOrEmpty(document.contractor.address.street))
                    {
                        legal_person.SeatAddress.Street = document.contractor.address.street;
                    }

                    if (!string.IsNullOrEmpty(document.contractor.address.building))
                    {
                        legal_person.SeatAddress.Building = document.contractor.address.building;
                    }

                    if (!string.IsNullOrEmpty(document.contractor.address.flat))
                    {
                        legal_person.SeatAddress.Flat = document.contractor.address.flat;
                    }

                    if (!string.IsNullOrEmpty(document.contractor.address.post))
                    {
                        legal_person.SeatAddress.Post = document.contractor.address.post;
                    }
                    legal_person.SeatAddress.ZipCode = document.contractor.address.post_code;

                    legal_person.Contacts = new contactType[1];
                    legal_person.Contacts[0] = new contactType();
                    if (!string.IsNullOrEmpty(document.contractor.contact.name))
                    {
                        legal_person.Contacts[0].FirstName = document.contractor.contact.name;
                    }

                    if (!string.IsNullOrEmpty(document.contractor.contact.surname))
                    {
                        legal_person.Contacts[0].Surname = document.contractor.contact.surname;
                    }

                    if (!string.IsNullOrEmpty(document.contractor.contact.role))
                    {
                        legal_person.Contacts[0].Role = document.contractor.contact.role;
                    }
                    if (!string.IsNullOrEmpty(document.contractor.contact.phone))
                    {
                        legal_person.Contacts[0].Phone = document.contractor.contact.phone;
                    }
                    legal_person.Contacts[0].Email = document.contractor.contact.email;

                    obligationinfo.Item = legal_person;
                }

                else
                {
                    entrepreneurType entrepreneur = new entrepreneurType();
                    entrepreneur.CompanyName = document.contractor.name;
                    entrepreneur.FirstName = document.contractor.firstname;
                    entrepreneur.Surname = document.contractor.lastname;

                    entrepreneur.NonConsumerIdentityNumber = new nonConsumerIdentityNumberType();
                    entrepreneur.NonConsumerIdentityNumber.Item = document.contractor.tax_number;
                    entrepreneur.SeatAddress = new addressType();
                    entrepreneur.SeatAddress.CountryCode = document.contractor.address.country;
                    if (!string.IsNullOrEmpty(document.contractor.address.city))
                    {
                        entrepreneur.SeatAddress.City = document.contractor.address.city;
                    }

                    if (!string.IsNullOrEmpty(document.contractor.address.street))
                    {
                        entrepreneur.SeatAddress.Street = document.contractor.address.street;
                    }

                    if (!string.IsNullOrEmpty(document.contractor.address.building))
                    {
                        entrepreneur.SeatAddress.Building = document.contractor.address.building;
                    }

                    if (!string.IsNullOrEmpty(document.contractor.address.flat))
                    {
                        entrepreneur.SeatAddress.Flat = document.contractor.address.flat;
                    }

                    if (!string.IsNullOrEmpty(document.contractor.address.post))
                    {
                        entrepreneur.SeatAddress.Post = document.contractor.address.post;
                    }
                    entrepreneur.SeatAddress.ZipCode = document.contractor.address.post_code;

                    entrepreneur.Contacts = new contactType[1];
                    entrepreneur.Contacts[0] = new contactType();
                    if (!string.IsNullOrEmpty(document.contractor.contact.name))
                    {
                        entrepreneur.Contacts[0].FirstName = document.contractor.contact.name;
                    }

                    if (!string.IsNullOrEmpty(document.contractor.contact.name))
                    {
                        entrepreneur.Contacts[0].Surname = document.contractor.contact.surname;
                    }

                    if (!string.IsNullOrEmpty(document.contractor.contact.role))
                    {
                        entrepreneur.Contacts[0].Role = document.contractor.contact.role;
                    }
                    if (!string.IsNullOrEmpty(document.contractor.contact.phone))
                    {
                        entrepreneur.Contacts[0].Phone = document.contractor.contact.phone;
                    }
                    entrepreneur.Contacts[0].Email = document.contractor.contact.email;

                    obligationinfo.Item = entrepreneur;
                
                
                }

                AddInformation.Item = obligationinfo;
                input.Item.Order[order_id].Item = AddInformation;
            return input;
        }
        public Input AddPayments(Input input, List<Document> obligations, List<Repayment> repayments, int lastidx, string branch)
        {
            for (int i = 0; i < repayments.Count; i++)
            {
                input.Item.Order[i + lastidx] = new informationManagementOrderType();
                input.Item.Order[i + lastidx].ID = (i + lastidx + 1).ToString();

                updateInformationType updateInformation = new updateInformationType();
                updateInformation.verifyResult = true;
                updateInformation.verifyResultSpecified = true;
                //DocStatus doc_status = obligations.Find(o => o.identifiers.userId == repayments[i].Userid).status;
                //if (doc_status.HasFlag(DocStatus.PAID))
                //{
                //    var fullPayment = AddFullPayment(repayments[i], branch);
                //    updateInformation.Item = fullPayment;
                //}
                //else if (doc_status.HasFlag(DocStatus.PARTIALY_PAID))
                //{
                //    var partialPayment = AddPartialPayment(repayments[i]);
                //    updateInformation.Item = partialPayment;
                //}
                //input.Item.Order[i + lastidx].Item = updateInformation;
            }
            return input;
        }

        public void SendData(ref bool send_regulation_acceptance, GetRegulationsResponse regulation, List<Document> all_documents, List<Document> obligations_to_send, List<Repayment> repayments, ref int successcount, ref int failcount, ref int errorcount, string identstr, string branch)
        {
            int count = obligations_to_send.Count + repayments.Count;
            int last_idx = 0;
            if(send_regulation_acceptance)
            {
                count += 1;
            }

            Input input = new Input();
            input.fileName = "DataSend";
            input.generator = "DataSend";
            input.timeStamp = DateTime.Now.Date;
            string integration = "";

            switch(identstr)
            {
                case "optima":
                    {
                        integration = "Comarch Optima";
                    }break;
                case "subiektgt":
                    {
                        integration = "InsERT Subiekt GT";
                    } break;
                case "wfmag":
                    {
                        integration = "Asseco WAPRO WFMAG";
                    } break;
                case "symfonia":
                    {
                        integration = "Sage Symfonia FiK";
                    } break;
                case "comarchxl":
                    {
                        integration = "Comarch XL";
                    } break;
                case "enova":
                    {
                        integration = "Soneta Enova";
                    } break;
                case "excel":
                    {
                        integration = "Plik dedykowany";
                    } break;
            }

            input.applicationSourceName = "FPConnectLite";
            input.Item = new informationManagementOrdersType();
            input.Item.Order = new informationManagementOrderType[count];
            
            if (send_regulation_acceptance)
            {
                input = AcceptRegulation(input, regulation);
                last_idx = 1;
            }

            input = AddObligations(obligations_to_send, input, last_idx);
            last_idx += obligations_to_send.Count;

            input = AddPayments(input, all_documents, repayments, last_idx, branch);

            string data = SerializeData(input);

            var chunkBag = UploadChunkRequest(data);
            var guid = CloseChunkBagEx(chunkBag);
            DateTime start = DateTime.Now;
            SaveRequestFile(data, guid);
            int getjobsresult = 0;
            while (getjobsresult == 0)
            {
                System.Threading.Thread.Sleep(5000);
                getjobsresult = GetJobsForSendData(guid, obligations_to_send, repayments, ref successcount, ref failcount, ref errorcount, ref send_regulation_acceptance);
            }
            DateTime end = DateTime.Now;
            int second = Convert.ToInt32(end.Subtract(start).TotalSeconds);
            logger.LogInfo("Całkowity czas operacji przekazania transzy w sekundach: " + second.ToString());
        }

        public void SendDataLite(ref bool send_regulation_acceptance, GetRegulationsResponse regulation, Document obligation_to_send, ref int successcount, ref int failcount, ref int errorcount, string branch)
        {            
            
            Input input = new Input();
            input.fileName = "DataSend";
            input.generator = "DataSend";
            input.timeStamp = DateTime.Now.Date;
            string integration = "FPConnectLite";
            
            input.applicationSourceName = "FPConnectLite"; //"FP Connect - " + integration;
            
            input.Item = new informationManagementOrdersType();
            
            int id = 1;
            if (send_regulation_acceptance)
            {
                input.Item.Order = new informationManagementOrderType[2];
                regulation = GetRegulation();
                input = AcceptRegulation(input, regulation);
                id++;
            }
            else
            {
                input.Item.Order = new informationManagementOrderType[1];
            }

            input = AddObligationLite(obligation_to_send, input, id);
            

            //input = AddPayments(input, all_documents, repayments, last_idx, branch);

            string data = SerializeData(input);

            var chunkBag = UploadChunkRequest(data);
            var guid = CloseChunkBagEx(chunkBag);
            DateTime start = DateTime.Now;
            SaveRequestFile(data, guid);
            int getjobsresult = 0;
            while (getjobsresult == 0)
            {
                System.Threading.Thread.Sleep(10000);
                getjobsresult = GetJobsForSendDataLite(guid, obligation_to_send, ref successcount, ref failcount, ref errorcount, ref send_regulation_acceptance);
            }
            logger.LogInfo("GetJobsForSendDataLite result: " + getjobsresult);
            DateTime end = DateTime.Now;
            int second = Convert.ToInt32(end.Subtract(start).TotalSeconds);
            logger.LogInfo("Całkowity czas operacji przekazania transzy w sekundach: " + second.ToString());
        }

        public int GetJobsForSendData(Guid guid, List<Document> documents_sent, List<Repayment> repayments_sent, ref int successcount, ref int failcount, ref int errorcount, ref bool send_regulation_acceptance)
        {
            int ret = 1;
            GetJobsRequest getjobrequest = new GetJobsRequest();
            getjobrequest.GetAll = true;
            getjobrequest.Ticket = this.Ticket;

            var jobs = importClient.GetJobs(getjobrequest);
            foreach (var job in jobs)
            {
                if (job.JobID == guid)
                {
                    if (job.Progress == 100)
                    {
                        try
                        {
                            GetChunkBagRequestEx chunbagrequestex = new GetChunkBagRequestEx();
                            chunbagrequestex.JobId = jobs[0].JobID;
                            chunbagrequestex.Ticket = this.Ticket;
                            chunbagrequestex.ChunkSize = 1000000;

                            var chunkbag = importClient.GetChunkBagEx(chunbagrequestex);
                            StringBuilder stringBuilder_xml = new StringBuilder();
                            for (int i = 1; i <= chunkbag.Count; i++)
                            {
                                DownloadChunkRequest downloadChunkRequest = new DownloadChunkRequest();
                                downloadChunkRequest.Number = i;
                                downloadChunkRequest.Ticket = this.Ticket;
                                downloadChunkRequest.ChunkBag = new ChunkBag();
                                downloadChunkRequest.ChunkBag.ID = chunkbag.ID;

                                stringBuilder_xml.Append(importClient.DownloadChunk(downloadChunkRequest));

                            }
                            string string_xml = stringBuilder_xml.ToString();
                            SaveResponseFile(string_xml, guid);
                            var output = DeserializeData(string_xml);

                            if (output.failCount > 0)
                            {
                                logger.LogXMLError(string_xml);
                            }

                            if (output.Item.GetType() == typeof(OutputInformationManagement))
                            {
                                OutputInformationManagement information = (OutputInformationManagement)output.Item;
                                foreach (var order in information.Order)
                                {
                                    if (order.Item.GetType() == typeof(addInformationResponseType))
                                    {
                                        addInformationResponseType order_item = (addInformationResponseType)order.Item;
                                        if (!ReferenceEquals(order_item.Item, null))
                                        {
                                            if (order_item.Item.GetType() == typeof(responseObligationType))
                                            {
                                                responseObligationType obligation = (responseObligationType)order_item.Item;
                                                var document = documents_sent.Find(o => o.identifiers.userId == obligation.userID);
                                                if (order.status == responseStatusEnum.Success)
                                                {
                                                    //document.status |= DocStatus.SENT;
                                                    //if (document.status.HasFlag(DocStatus.NO_CONTR_DATA) || document.status.HasFlag(DocStatus.NO_DOCUMENT_DATA)/* || document.status.HasFlag(DocStatus.VETA_VALIDATION_ERROR)*/)
                                                    //{
                                                    //    document.status &= ~DocStatus.NO_CONTR_DATA;
                                                    //    document.status &= ~DocStatus.NO_DOCUMENT_DATA;
                                                    //    //document.status &= ~DocStatus.VETA_VALIDATION_ERROR;
                                                    //    document.lacks = "";
                                                    //    document.identifiers.response = guid;
                                                    //}
                                                    successcount++;
                                                }
                                                else
                                                {
                                                    
                                                    simpleResponseInformationType simple_response = (simpleResponseInformationType)order.Item;
                                                    ////DocStatus doc_status = DocStatus.NONE;
                                                    //string errors = ValidateVetaResponse(simple_response, ref doc_status, ref document.veta_error);
                                                    //if (errors.Length > 0)
                                                    //{
                                                    //    document.status &= ~DocStatus.SENT;
                                                    //    if(document.veta_error)
                                                    //    {
                                                    //        errorcount++;
                                                    //        doc_status &= ~DocStatus.NO_CONTR_DATA;
                                                    //        doc_status &= ~DocStatus.NO_DOCUMENT_DATA;
                                                    //    }
                                                    //    else
                                                    //    {
                                                    //        failcount++;
                                                    //    }

                                                    //    document.status |= doc_status;
                                                    //    document.lacks = document.full_number + ";" + errors;
                                                    //    //document.status |= DocStatus.VETA_VALIDATION_ERROR;
                                                    //}
                                                    //else
                                                    //{
                                                    //    document.status |= DocStatus.ERRORS;
                                                    //}
                                                }

                                            }
                                        }
                                        else
                                        {
                                            if (order.status == responseStatusEnum.Success && send_regulation_acceptance)
                                            {
                                                if(order.ID == "1")
                                                {
                                                    send_regulation_acceptance = false;
                                                }                                               
                                            }
                                            else if (order.status == responseStatusEnum.Fail && send_regulation_acceptance)
                                            {
                                                if (order.ID == "1")
                                                {
                                                    send_regulation_acceptance = true;
                                                }
                                            }
                                        }
                                    }
                                    else if ((order.Item.GetType() == typeof(updateInformationResponseType)))
                                    {
                                        updateInformationResponseType order_item = (updateInformationResponseType)order.Item;
                                        if (order_item.Item.GetType() == typeof(responseFullPaymentType))
                                        {
                                            responseFullPaymentType fullPayment = (responseFullPaymentType)order_item.Item;
                                            var document = documents_sent.Find(o => o.identifiers.userId == fullPayment.userID);
                                            //if (order.status == responseStatusEnum.Fail)
                                            //{
                                            //    document.status |= DocStatus.REPAYMENT_ERROR;
                                            //    document.status &= ~DocStatus.REPAYMENT_SUCCESS;
                                            //}
                                            //else
                                            //{
                                            //    document.status &= ~DocStatus.REPAYMENT_ERROR;
                                            //    document.status |= DocStatus.REPAYMENT_SUCCESS;
                                            //}
                                        }

                                        else if (order_item.Item.GetType() == typeof(responsePartialPaymentType))
                                        {
                                            responsePartialPaymentType partialPayment = (responsePartialPaymentType)order_item.Item;
                                            var document = documents_sent.Find(o => o.identifiers.userId == partialPayment.userID);
                                            //if (order.status == responseStatusEnum.Fail)
                                            //{
                                            //    document.status |= DocStatus.REPAYMENT_ERROR;
                                            //    document.status &= ~DocStatus.REPAYMENT_SUCCESS;
                                            //}
                                            //else
                                            //{
                                            //    document.status &= ~DocStatus.REPAYMENT_ERROR;
                                            //    document.status |= DocStatus.REPAYMENT_SUCCESS;
                                            //}
                                        }
                                    }
                                }
                            }
                            else
                            {
                                errorType error = (errorType)output.Item;
                                logger.LogMessage("Błąd :" + error.codeSpecified.ToString() + " " + error.Value);
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.LogMessage(ex.Message);
                        }
                    }
                    else
                    {
                        ret = 0;
                    }
                    break;
                }
            }
            return ret;
        }

        public int GetJobsForSendDataLite(Guid guid, Document document_sent, ref int successcount, ref int failcount, ref int errorcount, ref bool send_regulation_acceptance)
        {
            int ret = 1;
            GetJobsRequest getjobrequest = new GetJobsRequest();
            getjobrequest.JobId = guid;
            getjobrequest.Ticket = this.Ticket;
            Job[] jobs = null;
            try
            {
                jobs = importClient.GetJobs(getjobrequest);
            }
            catch (Exception ex)
            {
                logger.LogMessage("Error: GetJobsForSendDataLite.GetJobs \nrequest: \nJobId:" + getjobrequest.JobId.ToString() + "\nTicket" + getjobrequest.Ticket.ToString() + "\n" + ex.Message);
                return 1;
            }
            foreach (var job in jobs)
            {
                if (job.JobID == guid)
                {
                    if (job.Progress == 100)
                    {
                        try
                        {
                            GetChunkBagRequestEx chunbagrequestex = new GetChunkBagRequestEx();
                            chunbagrequestex.JobId = jobs[0].JobID;
                            chunbagrequestex.Ticket = this.Ticket;
                            chunbagrequestex.ChunkSize = 1000000;

                            var chunkbag = importClient.GetChunkBagEx(chunbagrequestex);
                            StringBuilder stringBuilder_xml = new StringBuilder();
                            for (int i = 1; i <= chunkbag.Count; i++)
                            {
                                DownloadChunkRequest downloadChunkRequest = new DownloadChunkRequest();
                                downloadChunkRequest.Number = i;
                                downloadChunkRequest.Ticket = this.Ticket;
                                downloadChunkRequest.ChunkBag = new ChunkBag();
                                downloadChunkRequest.ChunkBag.ID = chunkbag.ID;

                                stringBuilder_xml.Append(importClient.DownloadChunk(downloadChunkRequest));

                            }
                            string string_xml = stringBuilder_xml.ToString();
                            SaveResponseFile(string_xml, guid);
                            var output = DeserializeData(string_xml);

                            if (output.failCount > 0)
                            {
                                logger.LogXMLError(string_xml);
                            }

                            if (output.Item.GetType() == typeof(OutputInformationManagement))
                            {
                                OutputInformationManagement information = (OutputInformationManagement)output.Item;
                                foreach (var order in information.Order)
                                {
                                    if (order.Item.GetType() == typeof(addInformationResponseType))
                                    {
                                        addInformationResponseType order_item = (addInformationResponseType)order.Item;
                                        if (!ReferenceEquals(order_item.Item, null))
                                        {
                                            if (order_item.Item.GetType() == typeof(responseObligationType))
                                            {
                                                responseObligationType obligation = (responseObligationType)order_item.Item;
                                                
                                                if (order.status == responseStatusEnum.Success)
                                                {                                                    
                                                    successcount++;
                                                    document_sent.sent = true;
                                                    document_sent.send_date = DateTime.Now;
                                                }
                                                else
                                                {
                                                    
                                                    simpleResponseInformationType simple_response = (simpleResponseInformationType)order.Item;
                                                    document_sent.veta_errors = ValidateVetaResponseLite(simple_response);
                                                    document_sent.sent = false;
                                                }

                                            }
                                        }
                                        else
                                        {
                                            if (order.status == responseStatusEnum.Success && send_regulation_acceptance)
                                            {
                                                if (order.ID == "1")
                                                {
                                                    send_regulation_acceptance = false;
                                                }
                                            }
                                            else if (order.status == responseStatusEnum.Fail && send_regulation_acceptance)
                                            {
                                                if (order.ID == "1")
                                                {
                                                    send_regulation_acceptance = true;
                                                }
                                            }
                                        }
                                    }
                                    else if ((order.Item.GetType() == typeof(updateInformationResponseType)))
                                    {
                                        updateInformationResponseType order_item = (updateInformationResponseType)order.Item;
                                        if (order_item.Item.GetType() == typeof(responseFullPaymentType))
                                        {
                                            responseFullPaymentType fullPayment = (responseFullPaymentType)order_item.Item;
                                            var document = document_sent; //var document = documents_sent.Find(o => o.identifiers.userId == fullPayment.userID);
                                            
                                        }

                                        else if (order_item.Item.GetType() == typeof(responsePartialPaymentType))
                                        {
                                            responsePartialPaymentType partialPayment = (responsePartialPaymentType)order_item.Item;
                                            var document = document_sent;  //var document = documents_sent.Find(o => o.identifiers.userId == partialPayment.userID);
                                            
                                        }
                                    }
                                }
                            }
                            else
                            {
                                errorType error = (errorType)output.Item;
                                logger.LogMessage("Błąd :" + error.codeSpecified.ToString() + " " + error.Value);
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.LogMessage(ex.Message);
                        }
                    }
                    else
                    {
                        ret = 0;
                    }
                    break;
                }
            }
            return ret;
        }

        private string ValidateVetaResponse(simpleResponseInformationType simple_response, ref bool error)
        {
            string errors = "";
            if (!object.ReferenceEquals(simple_response.Error, null))
            {
                if (simple_response.Error.code == 23)
                {
                    string[] errors_eng = simple_response.Error.Value.Split('|');
                    foreach (var error_eng in errors_eng)
                    {
                        var err = vetaerrors.Find(o => o.eng == error_eng.Trim());
                        if(err != null)
                        {
                            switch(err.eng)
                            {
                                case "Obligation due date is invalid":
                                    {
                                        error = true;
                                    }break;
                                default:
                                    {
                                        //status |= err.status;
                                    }break;
                            }
                            errors += err.pl + ";";
                        }
                        /*for (int i = 0; i < KonektorMainForm.VetaValidationErrorsEng.Length; i++)
                        {
                            if (KonektorMainForm.VetaValidationErrorsEng[i].Trim() == error_eng.Trim())
                            {
                                errors += KonektorMainForm.VetaValidationErrorsPL[i] + ";";
                            }
                        }*/
                    }
                }
                else if(simple_response.Error.code == 5)
                {
                    errors += "Faktura o takim numerze jest już dodana;";
                    //status |= DocStatus.NO_DOCUMENT_DATA;
                    error = true;
                }
                else if(simple_response.Error.code == 69)
                {
                    errors += "Na tej fakturze Twoja firma widnieje jako kontrahent;";
                    //status |= DocStatus.NO_DOCUMENT_DATA;
                }
            }
            return errors;
        }

        private string ValidateVetaResponseLite(simpleResponseInformationType simple_response)
        {
            string errors = "";
            if (!object.ReferenceEquals(simple_response.Error, null))
            {
                if (simple_response.Error.code == 23)
                {
                    string[] errors_eng = simple_response.Error.Value.Split('|');
                    foreach (var error_eng in errors_eng)
                    {
                        var err = vetaerrors.Find(o => o.eng == error_eng.Trim());
                        if (err != null)
                        {
                            errors += err.pl + ";";
                        }
                        /*for (int i = 0; i < KonektorMainForm.VetaValidationErrorsEng.Length; i++)
                        {
                            if (KonektorMainForm.VetaValidationErrorsEng[i].Trim() == error_eng.Trim())
                            {
                                errors += KonektorMainForm.VetaValidationErrorsPL[i] + ";";
                            }
                        }*/
                    }
                }
                else if (simple_response.Error.code == 5)
                {
                    errors += "Faktura o takim numerze jest już dodana;";
                    //status |= DocStatus.NO_DOCUMENT_DATA;
                    //error = true;
                }
                else if (simple_response.Error.code == 69)
                {
                    errors += "Na tej fakturze Twoja firma widnieje jako kontrahent;";
                    //status |= DocStatus.NO_DOCUMENT_DATA;
                }
            }
            return errors;
        }

        public void DeserializeFile()
        {
            StreamReader sr = new StreamReader(@"D:\SkanujTo\KRD\XKK105\KG\KG\Communication\Response_1e5fdfd9-66b2-45fa-aaf9-20fdc88845fb.txt");
            string xml = sr.ReadToEnd();
            var output = DeserializeData(xml);
            bool send_regulation_acceptance = true;

            int ret = 1;
            GetJobsRequest getjobrequest = new GetJobsRequest();
            getjobrequest.GetAll = true;
            getjobrequest.Ticket = this.Ticket;

          
            if (output.Item.GetType() == typeof(OutputInformationManagement))
            {
                OutputInformationManagement information = (OutputInformationManagement)output.Item;
                foreach (var order in information.Order)
                {
                    if (order.Item.GetType() == typeof(addInformationResponseType))
                    {
                        addInformationResponseType order_item = (addInformationResponseType)order.Item;
                        if (!ReferenceEquals(order_item.Item, null))
                        {
                            if (order_item.Item.GetType() == typeof(responseObligationType))
                            {
                                responseObligationType obligation = (responseObligationType)order_item.Item;
                                
                                if (order.status == responseStatusEnum.Success)
                                {

                                }
                                else
                                {
                                    
                                    simpleResponseInformationType simple_response = (simpleResponseInformationType)order.Item;
                                    //DocStatus doc_status = DocStatus.NONE;
                                    /*string errors = ValidateVetaResponse(simple_response, ref doc_status);
                                    /if (errors.Length > 0)
                                    {
                                        
                                    }
                                    else
                                    {

                                    }*/
                                }

                            }
                        }
                        else
                        {
                            if (order.status == responseStatusEnum.Success && send_regulation_acceptance)
                            {
                                if (order.ID == "1")
                                {
                                    send_regulation_acceptance = false;
                                }
                            }
                            else if (order.status == responseStatusEnum.Fail && send_regulation_acceptance)
                            {
                                if (order.ID == "1")
                                {
                                    send_regulation_acceptance = true;
                                }
                            }
                        }
                    }
                    else if ((order.Item.GetType() == typeof(updateInformationResponseType)))
                    {
                        updateInformationResponseType order_item = (updateInformationResponseType)order.Item;
                        if (order_item.Item.GetType() == typeof(responseFullPaymentType))
                        {
                            responseFullPaymentType fullPayment = (responseFullPaymentType)order_item.Item;
                            if (order.status == responseStatusEnum.Fail)
                            {
                                
                            }
                            else
                            {
                                
                            }
                        }

                        else if (order_item.Item.GetType() == typeof(responsePartialPaymentType))
                        {
                            responsePartialPaymentType partialPayment = (responsePartialPaymentType)order_item.Item;
                            if (order.status == responseStatusEnum.Fail)
                            {
                            }
                            else
                            {
                            
                        }
                    }
                }
            }
            }
            else
            {
                errorType error = (errorType)output.Item;
                logger.LogMessage("Błąd :" + error.codeSpecified.ToString() + " " + error.Value);
            }
        }

        public void ChangeEnvironment(int environment)
        {
            if (environment == 0)
            {
                client.Url = @"https://services.krd.pl/Lucy/1.0/FairPay.svc";
                importClient.Url = @"https://services.krd.pl/Siddin/2.1/Import.asmx";
                measurer.SetService("https://services.krd.pl/Lily/v1");
            }
            else if (environment == 1)
            {
                client.Url = @"https://demo.krd.pl/Lucy/1.0/FairPay.svc";
                importClient.Url = @"https://demo.krd.pl/Siddin/2.1/Import.asmx";
                measurer.SetService("https://demo.krd.pl/Lily/v1");
            }
        }

        public void SendOperation(string id, string operation_type)
        {
            Dictionary<string, string> json = new Dictionary<string, string>();
            string login = StaticFunctions.Functions.Decrypt(XML.XMLConfig.GetLogin());
            json.Add("OperationDate", DateTime.Now.ToString());
            json.Add("PublicIP", publicip);
            json.Add("AppVersion", Utility.GetActualVersion().ToString());
            if (!string.IsNullOrEmpty(login)) 
            {
                json.Add("Login", login);
            }
            var res = measurer.SendOperation(id, operation_type, json);
        }


        public void SendWeeklyOperation(string id, string operation_type, Dictionary<string, string> json)
        {
            Dictionary<string, string> newjson = new Dictionary<string, string>();
            string login = StaticFunctions.Functions.Decrypt(XML.XMLConfig.GetLogin());
            newjson.Add("OperationDate", DateTime.Now.ToString());
            newjson.Add("PublicIP", publicip);
            newjson.Add("AppVersion", Utility.GetActualVersion().ToString());
            if (!string.IsNullOrEmpty(login))
            {
                newjson.Add("Login", login);
            }
            foreach (var elem in json)
            {
                newjson.Add(elem.Key, elem.Value);
            }
            var res = measurer.SendOperation(id, operation_type, newjson);

        }


        public void GetIP()
        {
            var result = Utility.GetPublicIP();
            if (result.status == 1)
            {
                publicip = result.msg;
                logger.LogInfo("Pobrano adres IP: " + result.msg);
            }
            else
            {
                logger.LogMessage("Błąd pobierania publicznego adresu IP. " + result.msg);
            }
            
        }

        private void tLogin_Tick(object sender, EventArgs e)
        {
            string s_login_time = XML.XMLConfig.GetLoginTime();

            string separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            double login_time = 0;
            if (s_login_time.Contains(",") && separator == ".")
            {
                s_login_time = s_login_time.Replace(",", ".");

            }
            else if (s_login_time.Contains(".") && separator == ",")
            {
                s_login_time = s_login_time.Replace(".", ",");
            }

            login_time = Convert.ToDouble(s_login_time);

            double interval = Math.Round((double)tLogin.Interval / 60000.0, 0);
            interval = Math.Round((double)interval / 60.0, 1);
            login_time += interval;
            login_time = Math.Round(login_time, 1);
            XML.XMLConfig.SetLoginTime(login_time.ToString());
        }
    }
}
