﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace STP_Lite.KRD.Validators
{
    public static class KrdEmailValidator
    {
        // Local part of email address acording to rfc document http://tools.ietf.org/html/rfc2822#section-3.4.1 
        // Without polish characters - not supported in sending emails by Andolin.
        // domain part are only alpha numeric characters, bacause other are not supported.
        public static readonly string EmailPattern = string.Format("^{0}([.]{0})*@{1}([-.]{1})*\\.{2}$", DotAtomPattern, AlphaNumericWithUnderscorePattern, AlphaNumericPattern);

        private const string DotAtomPattern = "[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+";

        private const string AlphaNumericWithUnderscorePattern = "[a-zA-Z_0-9_]+";

        private const string AlphaNumericPattern = "[a-zA-Z_0-9]+";

        private static readonly Regex _emailRegex = new Regex(EmailPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public static bool IsEmailValid(string email)
        {
            if (email == null)
            {
                throw new ArgumentNullException("email");
            }

            return _emailRegex.IsMatch(email);
        }
    }
}
