﻿using STP_Lite.KRD.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace STP_Lite.KRD.Validators
{
    public static class PhoneNumberValidator
    {
        private static readonly string _phoneRegex =
      @"^((?<countryCode>[+]?(48))?[ -]?(?<number>([0-9]{9})))$|^((?<countryCode>[+]?([0-9]{1,4}))?[ -]?(?<number>([0-9]{5,11})))$";

        private static HashSet<string> _allowedDirectionNumbers = new HashSet<string>
        {
            "12", // Kraków
            "13", // Krosno
            "14", // Tarnów
            "15", // Tarnobrzeg
            "16", // Przemyśl
            "17", // Rzeszów
            "18", // Nowy Sącz
            "22", // Warszawa
            "23", // Ciechanów
            "24", // Płock
            "25", // Siedlce
            "26", // MON
            "29", // Ostrołęka
            "32", // Katowice
            "33", // Bielsko-Biała
            "34", // Częstochowa
            "39", // usługi wykorzystujące technologię IP (d. Komertel)
            "41", // Kielce
            "42", // Łódź
            "43", // Sieradz
            "44", // Piotrków Trybunalski
            "46", // Skierniewice
            "47", // MSWiA
            "48", // Radom
            "52", // Bydgoszcz
            "54", // Włocławek
            "55", // Elbląg
            "56", // Toruń
            "58", // Gdańsk
            "59", // Słupsk
            "61", // Poznań
            "62", // Kalisz
            "63", // Konin
            "65", // Leszno
            "67", // Piła
            "68", // Zielona Góra
            "71", // Wrocław
            "74", // Wałbrzych
            "75", // Jelenia Góra
            "76", // Legnica
            "77", // Opole
            "81", // Lublin
            "82", // Chełm
            "83", // Biała Podlaska
            "84", // Zamość
            "85", // Białystok
            "86", // Łomża
            "87", // Suwałki
            "89", // Olsztyn
            "91", // Szczecin
            "94", // Koszalin
            "95", // Gorzów Wielkopolski
          };

        public static bool IsValid(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
            {
                return false;
            }

            // remove separating characters used to improve reading phone number (spaces, hyphens)
            string cleanedPhoneNumber = PhoneNormalizer.GetCleanedPhoneNumber(phoneNumber);

            if (string.IsNullOrEmpty(cleanedPhoneNumber))
            {
                return false;
            }

            if (PhoneNumberContainsUnexpectedCharacters(cleanedPhoneNumber))
            {
                return false;
            }

            if (!IsPolishPhoneNumber(cleanedPhoneNumber))
            {
                return true;
            }

            if (IsLandlineNumber(cleanedPhoneNumber))
            {
                return IsValidByType(cleanedPhoneNumber, PhoneType.Landline);
            }

            return IsValidByType(cleanedPhoneNumber, PhoneType.Mobile);
        }

        public static bool IsValidByType(string phoneNumber, PhoneType phoneType)
        {
            phoneNumber = PhoneNormalizer.GetCleanedPhoneNumber(phoneNumber);

            if (!IsPolishPhoneNumber(phoneNumber))
            {
                return Regex.IsMatch(phoneNumber, PhonePattern.NORMALIZEDphoneNumberBasicPattern);
            }

            // reject numbers containing incorrect characters
            if (!Regex.IsMatch(phoneNumber, PhonePattern.NORMALIZEDphoneNumberPolishBasicPattern))
            {
                return false;
            }

            switch (phoneType)
            {
                case PhoneType.Unknown:
                case PhoneType.Landline:
                case PhoneType.Fax:
                    return IsValidLandlinePhone(phoneNumber);

                case PhoneType.Mobile:
                    return IsValidMobilePhone(phoneNumber);

                default:
                    return true;
            }
        }

        private static bool PhoneNumberContainsUnexpectedCharacters(string phoneNumber)
        {
            return !Regex.IsMatch(phoneNumber, "^\\+?[0-9]*$");
        }

        private static bool IsValidLandlinePhone(string normalizedPhoneNumber)
        {
            if (!string.IsNullOrEmpty(normalizedPhoneNumber))
            {
                Regex regex = null;
                Match match = null;

                if (!IsPolishPhoneNumber(normalizedPhoneNumber))
                {
                    return true;
                }

                regex = new Regex(PhonePattern.PolishLandline);
                if (!IsValidPolishLandlineDirectionNumber(normalizedPhoneNumber))
                {
                    return false;
                }

                match = regex.Match(normalizedPhoneNumber);
                return match.Success;
            }

            return false;
        }

        private static bool IsPolishPhoneNumber(string fullCleanedPhoneNumber)
        {
            return fullCleanedPhoneNumber.StartsWith(@"+48");
        }

        private static bool IsValidMobilePhone(string mobilePhone)
        {
            string normalizedPhoneNumber = PhoneNormalizer.GetCleanedPhoneNumber(mobilePhone);
            string normalizedPolishMobileNumberWithoutCountryNumber = normalizedPhoneNumber.Substring(normalizedPhoneNumber.IndexOf("+48") + "+48".Length);

            return Regex.IsMatch(normalizedPhoneNumber, PhonePattern.PolishMobile)
              && _allowedDirectionNumbers.All(n => !normalizedPolishMobileNumberWithoutCountryNumber.StartsWith(n));
        }

        private static bool IsValidPolishLandlineDirectionNumber(string normalizedPhoneNumber)
        {
            // brak numeru kierunkowego miasta
            if (normalizedPhoneNumber.Length < @"+48717850204".Length)
            {
                return false;
            }

            if (IsPolishPhoneNumber(normalizedPhoneNumber))
            {
                return PolishLandlineNumberContainsDirectionNumber(normalizedPhoneNumber);
            }

            return false;
        }

        private static bool PolishLandlineNumberContainsDirectionNumber(string normalizedPhoneNumber)
        {
            //// Na wszelki wypadek: Replace()'owanie wyłącznie z uwagi na treść regex'ów. Gdyby nie one, możnaby założyć, że dostarczamy znormalizowany numer telefonu
            string landlineNumberWithoutCountryPrefix = normalizedPhoneNumber.Substring(normalizedPhoneNumber.IndexOf(@"+48") + @"+48".Length)
                                                        .Replace(" ", string.Empty)
                                                        .Replace("-", string.Empty);

            return _allowedDirectionNumbers.Any<string>(n => landlineNumberWithoutCountryPrefix.StartsWith(n));
        }

        private static bool IsLandlineNumber(string normalizedPolishPhoneNumber)
        {
            return PolishLandlineNumberContainsDirectionNumber(normalizedPolishPhoneNumber);
        }
    }
}
