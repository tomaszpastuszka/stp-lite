﻿namespace STP_Lite.KRD.Validators
{
    public enum PhoneType
    {
        Unknown = -1,

        Landline = 0,

        Mobile = 1,

        Fax = 2
    }
}
