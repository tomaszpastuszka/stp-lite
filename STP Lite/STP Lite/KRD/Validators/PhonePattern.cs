﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STP_Lite.KRD.Validators
{
    public static class PhonePattern
    {
        public static readonly string Phone = @"^((?<countryCode>[+]?(48))?[ -]?(?<number>([0-9]{9})))$|^((?<countryCode>[+]?([0-9]{1,4}))?[ -]?(?<number>([0-9]{5,11})))$";

        public static readonly string PolishMobile = @"^((?<countryCode>[+]?(48))?[ -]?(?<number>([0-9]{9,10})))$";
        public static readonly string PolishLandline = @"^((?<countryCode>[+]?(48))?[ -]?(?<direction>([0-9]{2}))?(?<number>([0-9]{7})))$";

        //// Hopefully, there's no shorter (landline/fax/mobile) phone number in the world :)
        public static readonly string NORMALIZEDphoneNumberBasicPattern = @"^([+]?[0-9pw]{9,})$";
        public static readonly string NORMALIZEDphoneNumberPolishBasicPattern = @"^((([+]?(48))[0-9]{9,})|([0-9]{9,}))$";
    }
}
