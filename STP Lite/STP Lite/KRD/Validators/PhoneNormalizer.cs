﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace STP_Lite.KRD.Validators
{
    public class PhoneNormalizer
    {
        public static string GetCleanedPhoneNumber(string number)
        {
            const int PolishPhoneNumberLength = 9;

            string cleaned = number
              .Replace(" ", string.Empty)
              .Replace("-", string.Empty)
              .Replace("(", string.Empty)
              .Replace(")", string.Empty)
              .Replace("wew.", string.Empty)
              .Replace("wew", string.Empty)
              .Replace("w.", string.Empty)
              .Replace("w", string.Empty)
              .Replace(@"/", string.Empty);

            if (string.IsNullOrEmpty(cleaned))
            {
                return string.Empty;
            }

            bool isPolishNumber = cleaned.Length >= PolishPhoneNumberLength
                                  && (cleaned.StartsWith("+48") || (cleaned.StartsWith("48")
                                                                    && cleaned.Length >= "48717850204".Length) // ponieważ Radom ma numer kier. 48
                                                                || cleaned.StartsWith("0048"));

            // wycinam 0 z numeru +48 071 123456
            if (isPolishNumber)
            {
                if (cleaned.StartsWith("+480"))
                {
                    cleaned = cleaned.Remove(0, "+480".Length);
                    cleaned = cleaned.Insert(0, "+48");
                }
                else if (cleaned.StartsWith("480"))
                {
                    cleaned = cleaned.Remove(0, "480".Length);
                    cleaned = cleaned.Insert(0, "+48");
                }
                else if (cleaned.StartsWith("00480"))
                {
                    cleaned = cleaned.Remove(0, "00480".Length);
                    cleaned = cleaned.Insert(0, "+48");
                }
                else if (cleaned.StartsWith("48"))
                {
                    cleaned = cleaned.Remove(0, "48".Length);
                    cleaned = cleaned.Insert(0, "+48");
                }
            }

            // jeżeli mamy 48 na poczatku, opcjonalnie '+', tzn. jest postaci 48numertelefonu lub +48numertelefonu
            if (cleaned.Length >= PolishPhoneNumberLength
                && cleaned.StartsWith("+48"))
            {
                cleaned = cleaned.Substring(cleaned.IndexOf("48") + "48".Length);
            }

            // jeżeli mamy 0 na poczatku bez '+' i jest postaci 0numertelefonu lub 00numertelefonumiędzynarodowego
            if (cleaned.Length >= PolishPhoneNumberLength
                && cleaned.StartsWith("0")
                && !PhoneNumberContainsUnexpectedCharacters(cleaned))
            {
                if (cleaned.StartsWith("00"))
                {
                    cleaned = @"+" + cleaned.Substring("00".Length);
                }
                else
                {
                    cleaned = cleaned.Substring("0".Length);
                }
            }

            if (cleaned[0] != '+')
            {
                cleaned = "+48" + cleaned;
            }

            return cleaned;
        }

        private static bool PhoneNumberContainsUnexpectedCharacters(string phoneNumber)
        {
            return !Regex.IsMatch(phoneNumber, "^\\+?[0-9]*$");
        }
    }
}
