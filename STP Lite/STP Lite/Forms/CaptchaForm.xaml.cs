﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using STP_Lite.OtherStructures;

namespace STP_Lite.Forms
{
    /// <summary>
    /// Interaction logic for WaitForm.xaml
    /// </summary>
    public partial class CaptchaForm : Window
    {
        MainWindow mwindow = null;
        string tax_number = "";
        public CaptchaForm(Point position, MainWindow mwindow, BitmapImage inputCaptcha, string nip)
        {
            InitializeComponent();
            this.mwindow = mwindow;
            double widthScrean = System.Windows.SystemParameters.PrimaryScreenWidth;
            double heightScrean = System.Windows.SystemParameters.PrimaryScreenHeight;
            this.Left = widthScrean - position.X;
            this.Top = heightScrean - position.Y;            
            this.Topmost = true;

            imgCaptcha.Source = inputCaptcha;
            tax_number = nip;

            
        }

        private void tbCaptcha_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(tbCaptcha.Text.Length == 5)
            {
                bNext.IsEnabled = true;    
            }
            else
            {
                bNext.IsEnabled = false;
            }
        }

        private void bNext_Click(object sender, RoutedEventArgs e)
        {
            if (!object.ReferenceEquals(mwindow.mainPageHandler, null))
            {
                //mwindow.mainPageHandler.textSourceCaptcha = tbCaptcha.Text;
                mwindow.mainPageHandler.RunGetDataFromGUS(tax_number, tbCaptcha.Text);
            }
            this.Close();

        }
    }

    
}
