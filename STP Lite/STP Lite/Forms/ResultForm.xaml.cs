﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace STP_Lite.Forms
{
    /// <summary>
    /// Interaction logic for ResultForm.xaml
    /// </summary>
    public partial class ResultForm : Window
    {
        public MainWindow mwindow = null;
        public ResultForm(string info, string errors, MainWindow mwindow)
        {
            InitializeComponent();
            this.mwindow = mwindow;
            if (info == "Succes")
            {
                tbSuccess.Visibility = System.Windows.Visibility.Visible;
                tbError.Visibility = System.Windows.Visibility.Hidden;
                this.Title = "FP Easy  - Success!";

            }
            else if (info == "Error")
            {
                //this.Height = 290;

                tbSuccess.Visibility = System.Windows.Visibility.Hidden;
                tbError.Visibility = System.Windows.Visibility.Visible;
                                
                this.Title = "FP Easy - Error!";

                if (!string.IsNullOrEmpty(errors))
                {
                    tbError.Text += " \n\n " + errors;
                }                
            }
        }
    }
}
