﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace STP_Lite.Forms
{
    /// <summary>
    /// Interaction logic for WaitForm.xaml
    /// </summary>
    public partial class NoLoginForm : Window
    {
        Pages.DocumentPage dockPage = null;
        public NoLoginForm(Pages.DocumentPage dockPage)
        {
            InitializeComponent();
            this.dockPage = dockPage;
            

        }

        private void bNext_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void bOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

       
       

        private void bToTray_Click(object sender, RoutedEventArgs e)
        {
            dockPage.noLoginResult = "Close";
            this.Close();
        }

        private void bZaloguj_Click(object sender, RoutedEventArgs e)
        {
            dockPage.noLoginResult = "Loguj";
            this.Close();
        }

        

        




    }
}
