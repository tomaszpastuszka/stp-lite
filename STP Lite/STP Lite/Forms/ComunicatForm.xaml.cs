﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace STP_Lite.Forms
{
    /// <summary>
    /// Interaction logic for WaitForm.xaml
    /// </summary>
    public partial class ComunicatForm : Window
    {
        MainWindow mwindow = null;
        public ComunicatForm(string info, string errors, MainWindow mwindow)
        {
            InitializeComponent();
            this.mwindow = mwindow;
            if (info == "Succes")
            {
                
                tbKomunikat.Visibility = System.Windows.Visibility.Hidden;
                tbKomunikat2.Visibility = System.Windows.Visibility.Hidden;
                this.Title = "FP Easy  - Success!";
                bSendNext.Visibility = System.Windows.Visibility.Visible;
                //bSprawdz.Visibility = System.Windows.Visibility.Visible;
                tbSuccess.Visibility = System.Windows.Visibility.Visible;
                //tbTextLeft.Visibility = System.Windows.Visibility.Visible;
                //tbTextRight.Visibility = System.Windows.Visibility.Visible;
                //tbTextRight2.Visibility = System.Windows.Visibility.Visible;


            }
            else if (info == "Error")
            {
                this.Height = 290;

                bOK.Visibility = System.Windows.Visibility.Visible;
                //bSprawdz.Visibility = System.Windows.Visibility.Hidden;
                bSendNext.Visibility = System.Windows.Visibility.Hidden;
                bPobierz.Visibility = System.Windows.Visibility.Hidden;
                tbSuccess.Visibility = System.Windows.Visibility.Hidden;
                //tbTextLeft.Visibility = System.Windows.Visibility.Hidden;
                //tbTextRight.Visibility = System.Windows.Visibility.Hidden;
                //tbTextRight2.Visibility = System.Windows.Visibility.Hidden;
                tbKomunikat2.Visibility = System.Windows.Visibility.Visible;
                tbKomunikat.Visibility = System.Windows.Visibility.Visible;


                this.Title = "FP Easy - Error!";

                if (!string.IsNullOrEmpty(errors))
                {
                    tbKomunikat2.Text += " \n\n " + errors;
                }
                else
                {
                    tbKomunikat2.Text += " \n Sprawdź poprawność wprowadzonych danych i spróbuj ponownie. \n\n "+
                        "Jeżeli twoim płatnikiem jest jednoosobowa działalność gospodarcza, \nupewnij się, że w nazwie występuje Imię i Nazwisko płatnika.";
                }
            }

        }

        private void bNext_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void bOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void bPobierz_Click(object sender, RoutedEventArgs e)
        {

            Process.Start("http://fairpay.pl/konektor");
            this.Close();
        }

        private void bSprawdz_Click(object sender, RoutedEventArgs e)
        {
            string env = XML.XMLConfig.GetEnvironment();
            if (env == "0")
            {
                Process.Start("https://panel.kaczmarskigroup.pl");
            }
            else if (env == "1")
            {
                Process.Start("https://demo.krd.pl/Client2.0/WhiteLabel/Launch");
            }
            
            this.Close();
        }

        private void bToTray_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void bSendNext_Click(object sender, RoutedEventArgs e)
        {
            mwindow.ShowFromTray();
            this.Close();
        }

       

        

        




    }
}
