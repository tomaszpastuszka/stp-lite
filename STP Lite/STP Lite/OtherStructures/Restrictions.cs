﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STP_Lite.OtherStructures
{
    class Restrictions
    {
        public bool correctFullNumber = false;
        public bool correctNetAmount = false;
        public bool correctGrossAmount = false;
        public bool correctBankAccount = false;
        public bool correctCreateDate = false;
        public bool correctDueDate = false;
        public bool correctContractorName = false;
        public bool correctContractorAddres = false;
        public bool correctContractorAddresNr = false;
        public bool correctContractorAddresFlat = false;
        public bool correctPostCode = false;
        public bool correctCity = false;
        public bool correctPhone = false;
        public bool correctEmail = false;
    }
}
