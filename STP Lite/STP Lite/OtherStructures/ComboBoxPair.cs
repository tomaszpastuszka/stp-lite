﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STP_Lite.OtherStructures
{
    public class ComboBoxPair
    {
        public string _Key { get; set; }
        public string _Value { get; set; }

        public ComboBoxPair(string _key, string _value)
        {
            this._Key = _key;
            this._Value = _value;

        }
    }
}
