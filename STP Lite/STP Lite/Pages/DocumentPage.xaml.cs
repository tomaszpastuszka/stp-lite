﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using STP_Lite.KRD;
using STP_Lite.KRD.Validators;
using STP_Lite.Veta;
using STP_Lite.Structures;
using System.Text.RegularExpressions;
using STP_Lite.OtherStructures;
using STP_Lite.XML;
using STP_Lite.KrdStructures;
using System.Globalization;
using Api;

namespace STP_Lite.Pages
{
    /// <summary>
    /// Interaction logic for DocumentPage.xaml
    /// </summary>
    public partial class DocumentPage : Page
    {
        KRDApi krdapi = null;
        FPApi fpapi = null;
        MainWindow mwindow = null;
        Document document = new Document();
        Contractor contractor = new Contractor();
        Restrictions restr = new Restrictions();
        bool initialized = false;
        DateTime creationDate;
        DateTime dueDate;
        public bool underEditing = false;
        public string noLoginResult = "";
        public static string tollTipRecoSending = "Zwiń";
        public static string tollTipRecoNoSending = "Rezygnuj i Zwiń";
        public ToolTip toolTipRedo = null;
        public ToolTip toolTipRedoSending = null;
        public DocumentPage(KRDApi krdapi, FPApi fpapi, MainWindow mwindow)
        {
            InitializeComponent();
            this.krdapi = krdapi;
            this.fpapi = fpapi;
            this.mwindow = mwindow;

            initialized = true;

            creationDate = new DateTime();
            dueDate = new DateTime();

            dtpCreationDate.DisplayDateStart = DateTime.Parse(XMLConfig.GetLicenseAcceptation());
            dtpCreationDate.DisplayDateEnd = DateTime.Now;
            dtpCreationDate.SelectedDate = DateTime.Now.Date;
            creationDate = DateTime.Now;

           
            //dtpDueDate.SelectedDate = DateTime.Now.Date.AddDays(4);
            dtpDueDate.DisplayDateStart = DateTime.Now.Date.AddDays(4);//dtpDueDate.SelectedDate;//DateTime.Parse(XMLConfig.GetLicenseAcceptation());
            mwindow.logger.LogInfo("Załadowano DocumentPage");
            
            toolTipRedo = new System.Windows.Controls.ToolTip();
            toolTipRedo.Content = tollTipRecoNoSending;
            
            toolTipRedoSending = new System.Windows.Controls.ToolTip();
            toolTipRedoSending.Content = tollTipRecoSending;
            bRedo.ToolTip = toolTipRedo;
            
        }
       
        private void tbDockNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            //
            if (initialized)
            {
                if (tbDockNumber.Text.Length > 0 && tbDockNumber.Text.Length <= 128 && tbDockNumber.Text != "Numer faktury")
                {
                    tbDockNumber.BorderBrush = Brushes.Blue;
                    document.full_number = tbDockNumber.Text;
                    restr.correctFullNumber = true;
                    tbDockNumberError.Visibility = System.Windows.Visibility.Hidden;
                    if (AllFildsCorrect())
                    {
                        bSend.IsEnabled = true;
                    }
                    else
                    {
                        bSend.IsEnabled = false;
                    }
                }
                else
                {
                    tbDockNumber.BorderBrush = Brushes.Red;
                    restr.correctFullNumber = false;
                    bSend.IsEnabled = false;
                    if (underEditing)
                    {
                        tbDockNumberError.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        tbDockNumberError.Visibility = System.Windows.Visibility.Hidden;
                    }

                    if (tbDockNumber.Text.Length == 0)
                    {
                        tbDockNumberError.Text = "Zbyt krótki numer dokumentu";
                    }
                    else if (tbDockNumber.Text.Length > 128)
                    {
                        tbDockNumberError.Text = "Zbyt długi numer dokumentu";
                    }
                }
            }
        }

        private void tbNetto_TextChanged(object sender, TextChangedEventArgs e)
        {
            //
            
            if (initialized)
            {
                bool bad_char = false;
                string netAmount = Utility.RemoveNoCorrectChars(tbNetto.Text, out bad_char);
                string separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                if (netAmount.Contains(",") && separator == ".")
                {
                    netAmount = netAmount.Replace(",", ".");

                }
                else if (netAmount.Contains(".") && separator == ",")
                {
                    netAmount = netAmount.Replace(".", ",");
                }
                if (!string.IsNullOrEmpty(tbNetto.Text) && double.TryParse(netAmount, out document.net_amount.amount) && document.net_amount.amount > 0 && tbNetto.Text != "Kwota netto")
                {                    
                    tbNetto.BorderBrush = Brushes.Blue;
                    restr.correctNetAmount = true;
                    tbNettoError.Visibility = System.Windows.Visibility.Hidden;
                    if (AllFildsCorrect())
                    {
                        bSend.IsEnabled = true;
                    }
                    else
                    {
                        bSend.IsEnabled = false;
                    }

                    //tbGross_TextChanged(tbGross, null);
                }
                else
                {                    
                    tbNetto.BorderBrush = Brushes.Red;
                    restr.correctNetAmount = false;
                    bSend.IsEnabled = false;
                    if (underEditing)
                    {
                        tbNettoError.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        tbNettoError.Visibility = System.Windows.Visibility.Hidden;
                    }

                    if (string.IsNullOrEmpty(tbNetto.Text) || tbNetto.Text == " PLN")
                    {
                        tbNettoError.Text = "Proszę podać kwotę netto";
                    }
                    else if (document.net_amount.amount <= 0.0)
                    {
                        tbNettoError.Text = "Kwota netto nie może być 0";
                    }
                    else if (bad_char)
                    {
                        //tbNettoError.Text = "Niedozwolony znak";
                    }
                    
                }

                if (netAmount != tbNetto.Text)
                {
                    tbNetto.Text = netAmount + " PLN";
                    tbNetto.SelectionStart = tbNetto.Text.Length - 4;
                }
                else
                {
                    if (!tbNetto.Text.Contains("PLN"))
                    {
                        tbNetto.AppendText(" PLN");
                        tbNetto.SelectionStart = tbNetto.Text.Length - 4;
                    }
                }
            }
        }

        private void tbGross_TextChanged(object sender, TextChangedEventArgs e)
        {
            //
            if (initialized)
            {
                bool bad_char = false;
                string grossAmount = Utility.RemoveNoCorrectChars(tbGross.Text, out bad_char);
                string separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                if (grossAmount.Contains(",") && separator == ".")
                {
                    grossAmount = grossAmount.Replace(",", ".");

                }
                else if (grossAmount.Contains(".") && separator == ",")
                {
                    grossAmount = grossAmount.Replace(".", ",");
                }
                //var parse = double.TryParse(grossAmount, out document.gross_amount.amount);
                //var p1 = document.gross_amount.amount > document.net_amount.amount;
                //var p2 = !string.IsNullOrEmpty(tbGross.Text);
                if (!string.IsNullOrEmpty(tbGross.Text) && double.TryParse(grossAmount, out document.gross_amount.amount) && document.gross_amount.amount >= document.net_amount.amount)
                {
                    
                    //tbGross.Text = document.gross_amount.amount.ToString();
                    tbGross.BorderBrush = Brushes.Blue;
                    restr.correctGrossAmount = true;
                    tbGrossError.Visibility = System.Windows.Visibility.Hidden;
                    if (AllFildsCorrect())
                    {
                        bSend.IsEnabled = true;
                    }
                    else
                    {
                        bSend.IsEnabled = false;
                    }     
                }
                else
                {
                    tbGross.BorderBrush = Brushes.Red;
                    restr.correctGrossAmount = false;
                    bSend.IsEnabled = false;
                    if (underEditing)
                    {
                        tbGrossError.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        tbGrossError.Visibility = System.Windows.Visibility.Hidden;
                    }
                   
                    if (document.gross_amount.amount == 0.0)
                    {
                        tbGrossError.Text = "Kwota brutto nie może być 0";
                    }
                    else if(document.gross_amount.amount < document.net_amount.amount)
                    {
                        tbGrossError.Text = "Kwota brutto musi być równa lub większa od kwoty netto";
                    }
                    else 
                    {
                        tbGrossError.Visibility = Visibility.Hidden;
                        restr.correctGrossAmount = true;
                    }
                    //tbNetto_TextChanged(tbNetto, null);
                }
                if (grossAmount != tbGross.Text)
                {
                    tbGross.Text = grossAmount + " PLN";
                    tbGross.SelectionStart = tbGross.Text.Length - 4;
                }
                else
                {
                    if (!tbGross.Text.Contains("PLN"))
                    {
                        tbGross.AppendText(" PLN");
                        tbGross.SelectionStart = tbGross.Text.Length - 4;
                    }
                }
            }
        }

        private void tbBankAccount_TextChanged(object sender, TextChangedEventArgs e)
        {
            //
            if (initialized)
            {
                string bank_account = Utility.RemoveNoCorrectCharsBankAccount(tbBankAccount.Text);
                bank_account = bank_account.Trim();
                if (!string.IsNullOrEmpty(bank_account) && Utility.GetCountDigitsInBankAccount(bank_account) == 26)
                {
                    tbBankAccount.BorderBrush = Brushes.Blue;
                    document.bank_account = bank_account;
                    contractor.bank_account = bank_account;
                    restr.correctBankAccount = true;
                    tbBankAccountError.Visibility = System.Windows.Visibility.Hidden;
                    if (AllFildsCorrect())
                    {
                        bSend.IsEnabled = true;
                    }
                    else
                    {
                        bSend.IsEnabled = false;
                    }
                }
                else
                {
                    tbBankAccount.BorderBrush = Brushes.Red;
                    restr.correctBankAccount = false;
                    bSend.IsEnabled = false;
                    contractor.bank_account = "";
                    if (underEditing)
                    {
                        tbBankAccountError.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        tbBankAccountError.Visibility = System.Windows.Visibility.Hidden;
                    }
                    if (Utility.GetCountDigitsInBankAccount(bank_account) < 26)
                    {
                        tbBankAccountError.Text = "Numer konta musi mieć 26 znaków. Numer zbyt krótki.";
                    }
                    else if(Utility.GetCountDigitsInBankAccount(bank_account) > 26)
                    {
                        tbBankAccountError.Text = "Numer konta musi mieć 26 znaków. Numer zbyt długi.";
                    }
                    
                }

                if (bank_account != tbBankAccount.Text && tbBankAccount.Text != "Numer konta")
                {
                    tbBankAccount.Text = bank_account;
                }
            }
        }

        private void tbContractorName_TextChanged(object sender, TextChangedEventArgs e)
        {
            //
            if (initialized)
            {
                if (tbContractorName.Text.Length > 0 && tbContractorName.Text.Length <= 128 && tbContractorName.Text != "Nazwa płatnika")
                {
                    tbContractorName.BorderBrush = Brushes.Blue;
                    contractor.name = tbContractorName.Text;
                    restr.correctContractorName = true;
                    tbContractorNameError.Visibility = System.Windows.Visibility.Hidden;
                    if (AllFildsCorrect())
                    {
                        bSend.IsEnabled = true;
                    }
                    else
                    {
                        bSend.IsEnabled = false;
                    }
                }
                else
                {
                    tbContractorName.BorderBrush = Brushes.Red;
                    restr.correctContractorName = false;
                    bSend.IsEnabled = false;
                    if (underEditing)
                    {
                        tbContractorNameError.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        tbContractorNameError.Visibility = System.Windows.Visibility.Hidden;
                    }
                    if (tbContractorName.Text.Length == 0)
                    {
                        tbContractorNameError.Text = "Zbyt krótka nazwa kontrahenta";
                    }
                    else if (tbContractorName.Text.Length > 128)
                    {
                        tbContractorNameError.Text = "Zbyt gługa nazwa kontrahenta";
                    }
                }
            }
        }

        private void tbAdress_TextChanged(object sender, TextChangedEventArgs e)
        {
            //
            if (initialized)
            {
                if (tbAdress.Text.Length > 0 && tbAdress.Text.Length <= 46 && tbAdress.Text != "Adres płatnika")
                {
                    tbAdress.BorderBrush = Brushes.Blue;
                    contractor.address.street = tbAdress.Text;
                    restr.correctContractorAddres = true;
                    tbAdressError.Visibility = System.Windows.Visibility.Hidden;
                    if (AllFildsCorrect())
                    {
                        bSend.IsEnabled = true;
                    }
                    else
                    {
                        bSend.IsEnabled = false;
                    }

                }
                else
                {
                    tbAdress.BorderBrush = Brushes.Red;
                    restr.correctContractorAddres = false;
                    bSend.IsEnabled = false;
                    if (underEditing)
                    {
                        tbAdressError.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        tbAdressError.Visibility = System.Windows.Visibility.Hidden;
                    }
                    if (tbAdress.Text.Length == 0)
                    {
                        tbAdressError.Text = "Zbyt krótki adres kontrahenta";
                    }
                    else if (tbAdress.Text.Length > 46)
                    {
                        tbAdressError.Text = "Zbyt długi adres kontrahenta";
                    }
                }
            }
        }

        private void tbPostCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            //
            if (initialized)
            {
                if (Regex.IsMatch(tbPostCode.Text, @"[0-9]{2}-[0-9]{3}", RegexOptions.IgnoreCase))
                {
                    
                    tbPostCode.BorderBrush = Brushes.Blue;
                    contractor.address.post_code = tbPostCode.Text;
                    restr.correctPostCode = true;
                    tbPostCodeError.Visibility = System.Windows.Visibility.Hidden;
                    if (AllFildsCorrect())
                    {
                        bSend.IsEnabled = true;
                    }
                    else
                    {
                        bSend.IsEnabled = false;
                    }
                }
                else
                {
                    tbPostCode.BorderBrush = Brushes.Red;
                    restr.correctPostCode = false;
                    bSend.IsEnabled = false;
                    if (underEditing)
                    {
                        tbPostCodeError.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        tbPostCodeError.Visibility = System.Windows.Visibility.Hidden;
                    }
                    tbPostCodeError.Text = "Wprowadź kod pocztowy w formacie XX-XXX";
                }
            }
        }

        private void tbCity_TextChanged(object sender, TextChangedEventArgs e)
        {
            //
            if (initialized)
            {
                if (tbCity.Text.Length > 0 && tbCity.Text.Length <= 32 && tbCity.Text != "Miasto")
                {
                    tbCity.BorderBrush = Brushes.Blue;
                    contractor.address.city = tbCity.Text;
                    contractor.address.post = tbCity.Text;
                    restr.correctCity = true;
                    tbCityError.Visibility = System.Windows.Visibility.Hidden;
                    if (AllFildsCorrect())
                    {
                        bSend.IsEnabled = true;
                    }
                    else
                    {
                        bSend.IsEnabled = false;
                    }
                }
                else
                {
                    tbCity.BorderBrush = Brushes.Red;
                    restr.correctCity = false;
                    bSend.IsEnabled = false;
                    if (underEditing)
                    {
                        tbCityError.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        tbCityError.Visibility = System.Windows.Visibility.Hidden;
                    }
                    if (tbCity.Text.Length == 0)
                    {
                        tbCityError.Text = "Zbyt krótka nazwa miejscowości";
                    }
                    else if (tbCity.Text.Length > 32)
                    {
                        tbCityError.Text = "Zbyt długa nazwa miejscowości";
                    }
                }
            }
        }

        private void tbPhone_TextChanged(object sender, TextChangedEventArgs e)
        {
            //
            if (initialized)
            {
                string phone = Utility.ClearPhoneNumber(tbPhone.Text);
                if (PhoneNumberValidator.IsValid(tbPhone.Text) || string.IsNullOrEmpty(tbPhone.Text) || tbPhone.Text == "Telefon")
                {
                    tbPhone.BorderBrush = Brushes.Blue;
                    if (tbPhone.Text != "Telefon")
                    {
                        contractor.contact.phone = tbPhone.Text;
                    }
                    else
                    {
                        contractor.contact.phone = "";
                    }
                    restr.correctPhone = true;
                    tbPhoneError.Visibility = System.Windows.Visibility.Hidden;
                    if (AllFildsCorrect())
                    {
                        bSend.IsEnabled = true;
                    }
                    else
                    {
                        bSend.IsEnabled = false;
                    }
                }
                else
                {
                    tbPhone.BorderBrush = Brushes.Red;
                    restr.correctPhone = false;
                    bSend.IsEnabled = false;
                    if (underEditing)
                    {
                        tbPhoneError.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        tbPhoneError.Visibility = System.Windows.Visibility.Hidden;
                    }
                    tbPhoneError.Text = "Niepoprawny numer telefonu";
                    contractor.contact.phone = "";

                }
                if (phone != tbPhone.Text && tbPhone.Text != "Telefon")
                {
                    tbPhone.Text = phone;
                    tbPhone.SelectionStart = tbPhone.Text.Length;
                }
            }
        }

        private void tbeMail_TextChanged(object sender, TextChangedEventArgs e)
        {
            //
            if (initialized)
            {
                string email = tbeMail.Text.Trim();
                if (KrdEmailValidator.IsEmailValid(email))
                {
                    tbeMail.BorderBrush = Brushes.Blue;
                    contractor.contact.email = email;
                    restr.correctEmail = true;
                    tbeMailError.Visibility = System.Windows.Visibility.Hidden;
                    if (AllFildsCorrect())
                    {
                        bSend.IsEnabled = true;
                    }
                    else
                    {
                        bSend.IsEnabled = false;
                    }
                    
                }
                else
                {
                    tbeMail.BorderBrush = Brushes.Red;
                    restr.correctEmail = false;
                    bSend.IsEnabled = false;
                    contractor.contact.email = "";
                    if (underEditing)
                    {
                        tbeMailError.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        tbeMailError.Visibility = System.Windows.Visibility.Hidden;
                    }
                    tbeMailError.Text = "Niepoprawny adres email";
                }
            }
        }

        private bool AllFildsCorrect()
        {
            
            mwindow.correctNIPMainPage = mwindow.ParseNIP(ref mwindow.NIPFromMainPage);
            if(restr.correctBankAccount &&
                restr.correctCity &&
                restr.correctContractorAddres &&
                restr.correctContractorAddresNr &&                
                restr.correctContractorName &&
                restr.correctCreateDate &&
                restr.correctDueDate &&
                restr.correctEmail &&
                restr.correctFullNumber &&
                restr.correctGrossAmount &&
                restr.correctNetAmount &&
                restr.correctPostCode &&
                mwindow.correctNIPMainPage)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void dtpCreationDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (initialized)
            {
                if (!object.ReferenceEquals(dtpCreationDate.SelectedDate, null))
                {
                    DateTime acceptationDate = DateTime.Parse(XMLConfig.GetLicenseAcceptation());
                    DateTime acceptationDateFullDay = new DateTime(acceptationDate.Year, acceptationDate.Month, acceptationDate.Day);

                    if (dtpCreationDate.SelectedDate.HasValue && (dtpCreationDate.SelectedDate.Value >= acceptationDateFullDay) && (dtpCreationDate.SelectedDate.Value < DateTime.Now.AddDays(1)))
                    {
                        creationDate = dtpCreationDate.SelectedDate.Value;
                        restr.correctCreateDate = true;                        
                        document.creation_date = creationDate;
                        dtpCreationDate.BorderBrush = Brushes.Blue;
                        //if(creationDate.AddDays(4) >= DateTime.Now.Date.AddDays(-3))
                        //{
                            dtpDueDate.DisplayDateStart = DateTime.Now.Date.AddDays(4);
                        //}
                        //else
                        //{
                           // dtpDueDate.DisplayDateStart = DateTime.Now.Date.AddDays(-3);
                        //}
                    }
                    else
                    {
                        dtpCreationDate.BorderBrush = Brushes.Red;
                        
                    }
                    
                }

            }
        }

        private void dtpDueDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (initialized)
            {
                if (!object.ReferenceEquals(dtpDueDate.SelectedDate, null) )
                {
                    
                    dueDate = dtpDueDate.SelectedDate.Value;
                    if ((dueDate - creationDate).TotalDays > 3)
                    {
                        dtpDueDate.BorderBrush = Brushes.Blue;
                        document.due_date = dueDate;
                        restr.correctDueDate = true;
                        if (AllFildsCorrect())
                        {
                            bSend.IsEnabled = true;
                        }
                        else
                        {
                            bSend.IsEnabled = false;
                        }
                    }
                    else
                    {
                        dtpDueDate.BorderBrush = Brushes.Red;
                        restr.correctDueDate = false;
                        bSend.IsEnabled = false;
                        
                    }
                }
                //dtpDueDateError.Text = "Wybierz datę płatności dokumentu";
                

            }
        }

        private void tbNr_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (initialized)
            {
                if (tbNr.Text.Length > 0 && tbNr.Text.Length <= 8)
                {
                    tbNr.BorderBrush = Brushes.Blue;
                    contractor.address.building = tbNr.Text;
                    restr.correctContractorAddresNr = true;
                    tbNrError.Visibility = System.Windows.Visibility.Hidden;
                    if (AllFildsCorrect())
                    {
                        bSend.IsEnabled = true;
                    }
                    else
                    {
                        bSend.IsEnabled = false;
                    }
                }
                else
                {
                    tbNr.BorderBrush = Brushes.Red;
                    restr.correctContractorAddresNr = false;
                    bSend.IsEnabled = false;
                    if (underEditing)
                    {
                        tbNrError.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        tbNrError.Visibility = System.Windows.Visibility.Hidden;
                    }
                    if (tbCity.Text.Length == 0)
                    {
                        tbNrError.Text = "Wymagany jest numer budynku";
                    }
                    else if (tbCity.Text.Length > 8)
                    {
                        tbNrError.Text = "Zbyt długi numer";
                    }
                }
            }
        }

        private void tbFlat_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (initialized)
            {
                if (tbFlat.Text.Length > 0 && tbFlat.Text.Length <= 8)
                {
                    tbFlat.BorderBrush = Brushes.Blue;
                    if (tbFlat.Text != "Lokal")
                    {
                        contractor.address.flat = tbFlat.Text;
                    }
                    restr.correctContractorAddresFlat = true;
                    //tbDockNumberError.Visibility = System.Windows.Visibility.Hidden;
                    if (AllFildsCorrect())
                    {
                        bSend.IsEnabled = true;
                    }
                    else
                    {
                        bSend.IsEnabled = false;
                    }
                }
                else
                {
                    tbFlat.BorderBrush = Brushes.Red;
                    restr.correctContractorAddresFlat = false;
                    bSend.IsEnabled = false;
                }
            }
        }
        public void DisableDocPageControls()
        {
            tbAdress.IsEnabled = false;
            tbDockNumber.IsEnabled = false;
            tbBankAccount.IsEnabled = false;
            tbNetto.IsEnabled = false;
            tbGross.IsEnabled = false;
            tbContractorName.IsEnabled = false;
            tbNr.IsEnabled = false;
            tbFlat.IsEnabled = false;
            tbPhone.IsEnabled = false;
            tbeMail.IsEnabled = false;
            tbPostCode.IsEnabled = false;
            tbCity.IsEnabled = false;
            bSend.IsEnabled = false;

            dtpCreationDate.IsEnabled = false;
            dtpCreationDate.BorderBrush = System.Windows.Media.Brushes.Gray;
            dtpCreationDate.Background = System.Windows.Media.Brushes.LightGray;

            dtpDueDate.IsEnabled = false;
            dtpDueDate.BorderBrush = System.Windows.Media.Brushes.Gray;
            dtpDueDate.Background = System.Windows.Media.Brushes.LightGray;
        }

        public void EnableDocPageControls()
        {
            
            this.tbAdress.IsEnabled = true;
            this.tbDockNumber.IsEnabled = true;
            this.tbBankAccount.IsEnabled = true;
            this.tbNetto.IsEnabled = true;
            this.tbGross.IsEnabled = true;
            this.tbContractorName.IsEnabled = true;
            this.tbNr.IsEnabled = true;
            this.tbFlat.IsEnabled = true;
            this.tbPhone.IsEnabled = true;
            this.tbeMail.IsEnabled = true;
            this.tbPostCode.IsEnabled = true;
            this.tbCity.IsEnabled = true;
            this.bSend.IsEnabled = true;
            this.dtpCreationDate.IsEnabled = true;
            this.dtpDueDate.IsEnabled = true;
            
        }

        private void bSend_Click(object sender, RoutedEventArgs e)
        {
            
            if (mwindow.loggedIn)
            {
                bRedo.ToolTip = tollTipRecoSending;
                DisableDocPageControls();
                mwindow.mainPageHandler.DisableMainPageControls();
                mwindow.logger.LogInfo("Tworzenie dokumentu do wysłania do KRD");
                contractor.SetContractorType();
                if (string.IsNullOrEmpty(contractor.tax_number) && mwindow.correctNIPMainPage)
                {
                    contractor.tax_number = mwindow.NIPFromMainPage;
                    document.contractor_tax_number = mwindow.NIPFromMainPage;
                }
                document.identifiers.userId = mwindow.comm.GenerateUserId(document.full_number);
                contractor.address.country = mwindow.GetCountryCode("Polska");
                //contractor.bank_account = tbBankAccount.Text;

                //if (mwindow.correctNIPMainPage)
                //{
                //    contractor.tax_number = mwindow.NIPFromMainPage;
                //}
                Document obligation = CreateObligation(document, contractor);
                
                mwindow.SendObligation(obligation);
                
                Result res = null;
                var con = mwindow.contractors.Find(o => o.tax_number == contractor.tax_number);
                if (object.ReferenceEquals(con, null))
                {
                    mwindow.logger.LogInfo("Próba zapisania kontrahenta do bazy danych");
                    res = mwindow.comm.SaveContractor(contractor);
                    if (res.status == 1)
                    {
                        mwindow.logger.LogInfo("Udany zapis kontrahenta do bazy danych");
                    }
                    else
                    {
                        mwindow.logger.LogInfo("Nieudany zapis kontrahenta do bazy danych \n" + res.msg);
                        mwindow.logger.LogMessage("Nieudany zapis kontrahenta do bazy danych" + res.msg);
                    }
                    if (!object.ReferenceEquals(mwindow.mainPageHandler, null))
                    {
                        mwindow.mainPageHandler.PrepareComboBoxContractors();
                        mwindow.mainPageHandler.SetDEfaultState();
                    }
                }
                else
                {
                    mwindow.logger.LogInfo("Próba aktualizacji kontrahenta w bazie danych");
                    res = mwindow.comm.UpdateContractor(contractor);
                    if (res.status == 1)
                    {
                        mwindow.logger.LogInfo("Udany update kontrahenta do bazy danych");
                    }
                    else
                    {
                        mwindow.logger.LogInfo("Nieudany update kontrahenta do bazy danych \n" + res.msg);
                        mwindow.logger.LogMessage("Nieudany update kontrahenta do bazy danych" + res.msg);
                    }
                }
                mwindow.comm.GetContractors(ref mwindow.contractors);
            }
            else
            {
                
                mwindow.logger.LogInfo("Próba wysłania dokumentu przez niezalogowanego uzytkownika");
                Forms.NoLoginForm noLoginForm = new Forms.NoLoginForm(this);

                noLoginForm.ShowDialog();
                mwindow.AnimateWindowWidthRight(false);
                if (noLoginResult == "Close")
                {
                    mwindow.logger.LogInfo("Ładowanie głownego okna do programu");
                    Pages.MainPage mainPage = new MainPage(krdapi, fpapi, mwindow);
                    mwindow.mainPageHandler = mainPage;
                    mwindow.mainFrame.NavigationService.Navigate(mainPage);
                    if (!object.ReferenceEquals(mwindow.mainPageHandler, null))
                    {
                        mwindow.mainPageHandler.SetDEfaultState();
                    }
                    mwindow.ShowFromTray();

                }
                else if (noLoginResult == "Loguj")
                {
                    mwindow.logger.LogInfo("Ładowanie okna logowania do programu");
                    Pages.LoginPage logPage = new LoginPage(krdapi, fpapi, mwindow);
                    mwindow.mainFrame.NavigationService.Navigate(logPage);
                }
                
                
            }

        }
        private Document CreateObligation(Document doc, Contractor contr)
        {
            mwindow.logger.LogInfo("Tworzenie dokumentu: CreateObligation");
            Document document = new Document();
            document = doc;
            document.contractor = contr;
            return document;
        }

        public void FillDocumentFields(Document doc)
        {
            mwindow.logger.LogInfo("Wypełnianie pól dokumentu");
            if (!string.IsNullOrEmpty(doc.full_number))
            {
                tbDockNumber.Text = doc.full_number;
            }
            if (!string.IsNullOrEmpty(doc.bank_account))
            {
                tbBankAccount.Text = doc.bank_account;
            }
            if (!object.ReferenceEquals(doc.net_amount, null))
            {
                tbNetto.Text = doc.net_amount.amount.ToString();
            }
            if (!object.ReferenceEquals(doc.gross_amount, null))
            {
                tbGross.Text = doc.gross_amount.amount.ToString();
            }
            if (!object.ReferenceEquals(doc.creation_date, null))
            {
                dtpCreationDate.SelectedDate = doc.creation_date;
            }
            if (!object.ReferenceEquals(doc.due_date, null))
            {
                dtpDueDate.SelectedDate = doc.due_date;
            }
            FillContractorFields(doc.contractor);

        }

        public void FillContractorFields(Contractor contr)
        {
            mwindow.logger.LogInfo("Wypełnianie pól kontrahenta");
            this.contractor = contr;
            if (!string.IsNullOrEmpty(contr.name))
            {
                tbContractorName.Text = contr.name;
            }
            if (!string.IsNullOrEmpty(contr.address.street))
            {
                tbAdress.Text = contr.address.street;
            }
            if (!string.IsNullOrEmpty(contr.address.building))
            {
                tbNr.Text = contr.address.building;
            }
            if (!string.IsNullOrEmpty(contr.address.flat))
            {
                tbFlat.Text = contr.address.flat;
            }
            if (!string.IsNullOrEmpty(contr.address.post_code))
            {
                string postCodeResult = "";
                string postCode = contr.address.post_code;
                if (!postCode.Contains('-'))
                {
                    //dodać mylśnik
                    for (int i = 0; i < postCode.Length; i++)
                    {
                        if (i == 2)
                        {
                            postCodeResult += "-" + postCode[i];
                        }
                        else
                        {
                            postCodeResult += postCode[i];
                        }
                    }
                    contr.address.post_code = postCodeResult;
                    tbPostCode.Text = postCodeResult;

                }
                else
                {
                    tbPostCode.Text = contr.address.post_code;
                }
                
            }
            if (!string.IsNullOrEmpty(contr.address.city))
            {
                tbCity.Text = contr.address.city;
            }
            if (!string.IsNullOrEmpty(contr.contact.phone))
            {
                tbPhone.Text = contr.contact.phone;
            }
            if (!string.IsNullOrEmpty(contr.contact.email))
            {
                tbeMail.Text = contr.contact.email;
            }
            if (!string.IsNullOrEmpty(contr.bank_account))
            {
                tbBankAccount.Text = contr.bank_account;
            }
        }

        private void tbDockNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            underEditing = true;
            TextBox tb = (TextBox)sender;
            if (tb.Text.Contains("Numer faktury"))
            {
                tb.Text = string.Empty;
            }
            if (!restr.correctFullNumber)
            {
                tbDockNumberError.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                tbDockNumberError.Visibility = System.Windows.Visibility.Hidden;
            }
            //tb.GotFocus -= tbDockNumber_GotFocus;
        }

        private void tbDockNumber_LostFocus(object sender, RoutedEventArgs e)
        {
            underEditing = false;
            TextBox tb = (TextBox)sender;
            if (string.IsNullOrEmpty(tb.Text))
            {
                tb.Text = "Numer faktury";
            }
            //tbDockNumberError.Visibility = System.Windows.Visibility.Hidden;
            
        }

        private void tbNetto_GotFocus(object sender, RoutedEventArgs e)
        {
            underEditing = true;
            TextBox tb = (TextBox)sender;
            if (tb.Text.Contains("Kwota netto"))
            {
                tb.Text = string.Empty;
            }
            if (!restr.correctNetAmount)
            {
                tbNettoError.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                tbNettoError.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void tbNetto_LostFocus(object sender, RoutedEventArgs e)
        {
            underEditing = false;
            TextBox tb = (TextBox)sender;
            if (string.IsNullOrEmpty(tb.Text) || (tb.Text.Contains("PLN") && tb.Text.Length < 5 ))
            {
                tb.Text = "Kwota netto";
            }
            //tbNettoError.Visibility = System.Windows.Visibility.Hidden;
            tbGross_TextChanged(tbGross, null);
        }

        private void tbGross_GotFocus(object sender, RoutedEventArgs e)
        {
            underEditing = true;
            TextBox tb = (TextBox)sender;
            if (tb.Text.Contains("Kwota brutto"))
            {
                tb.Text = string.Empty;
            }
            if (!restr.correctGrossAmount)
            {
                tbGrossError.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                tbGrossError.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void tbGross_LostFocus(object sender, RoutedEventArgs e)
        {
            underEditing = false;
            TextBox tb = (TextBox)sender;
            if (string.IsNullOrEmpty(tb.Text) || (tb.Text.Contains("PLN") && tb.Text.Length < 5))
            {
                tb.Text = "Kwota brutto";
            }
            //tbGrossError.Visibility = System.Windows.Visibility.Hidden;
        }

        private void tbBankAccount_GotFocus(object sender, RoutedEventArgs e)
        {
            underEditing = true;
            TextBox tb = (TextBox)sender;
            if (tb.Text.Contains("Numer konta"))
            {
                tb.Text = string.Empty;
            }
            if (!restr.correctBankAccount)
            {
                tbBankAccountError.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                tbBankAccountError.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void tbBankAccount_LostFocus(object sender, RoutedEventArgs e)
        {
            underEditing = false;
            TextBox tb = (TextBox)sender;
            if (string.IsNullOrEmpty(tb.Text))
            {
                tb.Text = "Numer konta";
            }
            //tbBankAccountError.Visibility = System.Windows.Visibility.Hidden;
        }

        private void tbContractorName_GotFocus(object sender, RoutedEventArgs e)
        {
            underEditing = true;
            TextBox tb = (TextBox)sender;
            if (tb.Text.Contains("Nazwa płatnika"))
            {
                tb.Text = string.Empty;
            }
            if (!restr.correctContractorName)
            {
                tbContractorNameError.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                tbContractorNameError.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void tbContractorName_LostFocus(object sender, RoutedEventArgs e)
        {
            underEditing = false;
            TextBox tb = (TextBox)sender;
            if (string.IsNullOrEmpty(tb.Text))
            {
                tb.Text = "Nazwa płatnika";
            }
            //tbContractorNameError.Visibility = System.Windows.Visibility.Hidden;
        }

        private void tbAdress_GotFocus(object sender, RoutedEventArgs e)
        {
            underEditing = true;
            TextBox tb = (TextBox)sender;
            if (tb.Text.Contains("Adres płatnika"))
            {
                tb.Text = string.Empty;
            }
            if (!restr.correctContractorAddres)
            {
                tbAdressError.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                tbAdressError.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void tbAdress_LostFocus(object sender, RoutedEventArgs e)
        {
            underEditing = false;
            TextBox tb = (TextBox)sender;
            if (string.IsNullOrEmpty(tb.Text))
            {
                tb.Text = "Adres płatnika";
            }
            tbAdressError.Visibility = System.Windows.Visibility.Hidden;
        }

        private void tbNr_GotFocus(object sender, RoutedEventArgs e)
        {

            underEditing = true;
            TextBox tb = (TextBox)sender;
            if (tb.Text.Contains("Nr"))
            {
                tb.Text = string.Empty;
            }
            if (!restr.correctContractorAddresNr)
            {
                tbNrError.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                tbNrError.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void tbNr_LostFocus(object sender, RoutedEventArgs e)
        {
            underEditing = false;
            TextBox tb = (TextBox)sender;
            if (string.IsNullOrEmpty(tb.Text))
            {
                tb.Text = "Nr";
            }
            //tbNrError.Visibility = System.Windows.Visibility.Hidden;
        }

        private void tbFlat_GotFocus(object sender, RoutedEventArgs e)
        {
            underEditing = true;
            TextBox tb = (TextBox)sender;
            if (tb.Text.Contains("Lokal"))
            {
                tb.Text = string.Empty;
            }
            //if (!restr.correctContractorAddresFlat)
            //{
            //    //tbFlat Error.Visibility = System.Windows.Visibility.Visible;
            //}
            //else
            //{
            //    //tbDockNumberError.Visibility = System.Windows.Visibility.Hidden;
            //}
        }

        private void tbFlat_LostFocus(object sender, RoutedEventArgs e)
        {
            underEditing = false;
            TextBox tb = (TextBox)sender;
            if (string.IsNullOrEmpty(tb.Text))
            {
                tb.Text = "Lokal";
            }
            //if (!restr.correctContractorAddresFlat)
            //{
            //    //tbFlat Error.Visibility = System.Windows.Visibility.Visible;
            //}
            //else
            //{
            //    //tbF.Visibility = System.Windows.Visibility.Hidden;
            //}
        }

        private void tbPostCode_GotFocus(object sender, RoutedEventArgs e)
        {
            underEditing = true;
            TextBox tb = (TextBox)sender;
            if (tb.Text.Contains("Kod"))
            {
                tb.Text = string.Empty;
            }
            if (!restr.correctPostCode)
            {
                tbPostCodeError.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                tbPostCodeError.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void tbPostCode_LostFocus(object sender, RoutedEventArgs e)
        {
            underEditing = false;
            TextBox tb = (TextBox)sender;
            if (string.IsNullOrEmpty(tb.Text))
            {
                tb.Text = "Kod";
            }
            //tbPostCodeError.Visibility = System.Windows.Visibility.Hidden;
        }

        private void tbCity_GotFocus(object sender, RoutedEventArgs e)
        {
            underEditing = true;
            TextBox tb = (TextBox)sender;
            if (tb.Text.Contains("Miasto"))
            {
                tb.Text = string.Empty;
            }
            if (!restr.correctCity)
            {
                tbCityError.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                tbCityError.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void tbCity_LostFocus(object sender, RoutedEventArgs e)
        {
            underEditing = false;
            TextBox tb = (TextBox)sender;
            if (string.IsNullOrEmpty(tb.Text))
            {
                tb.Text = "Miasto";
            }
            //tbCityError.Visibility = System.Windows.Visibility.Hidden;
        }

        private void tbPhone_GotFocus(object sender, RoutedEventArgs e)
        {
            underEditing = true;
            TextBox tb = (TextBox)sender;
            if (tb.Text.Contains("Telefon"))
            {
                tb.Text = string.Empty;
            }
            if (!restr.correctPhone)
            {
                tbPhoneError.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                tbPhoneError.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void tbPhone_LostFocus(object sender, RoutedEventArgs e)
        {
            underEditing = false;
            TextBox tb = (TextBox)sender;
            if (string.IsNullOrEmpty(tb.Text))
            {
                tb.Text = "Telefon";
            }
            //tbPhoneError.Visibility = System.Windows.Visibility.Hidden;
        }

        private void tbeMail_GotFocus(object sender, RoutedEventArgs e)
        {
            underEditing = true;
            TextBox tb = (TextBox)sender;
            if (tb.Text.Contains("E-mail"))
            {
                tb.Text = string.Empty;
            }
            if (!restr.correctEmail)
            {
                tbeMailError.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                tbeMailError.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void tbeMail_LostFocus(object sender, RoutedEventArgs e)
        {
            underEditing = false;
            TextBox tb = (TextBox)sender;
            if (string.IsNullOrEmpty(tb.Text))
            {
                tb.Text = "E-mail";
            }
            //tbeMailError.Visibility = System.Windows.Visibility.Hidden;
        }

        private void bRedo_Click(object sender, RoutedEventArgs e)
        {
            mwindow.logger.LogInfo("Rezygnacja z wprowadzania danych dokumentu: Strzałka BACK");
            mwindow.foundContrator = null;
            contractor = null;
            if (!object.ReferenceEquals(mwindow.mainPageHandler, null))
            {
                mwindow.mainPageHandler.SetDEfaultState();
            }
            mwindow.AnimateWindowWidthRight(false);
        }

       









    }
}
