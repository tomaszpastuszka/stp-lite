﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using STP_Lite.KRD;
using STP_Lite.Structures;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

namespace STP_Lite.Pages
{
    /// <summary>
    /// Interaction logic for DocumentListPage.xaml
    /// </summary>
    /// 
    public partial class DocumentListPage : Page
    {
        MainWindow mwindow = null;
        KRDApi krdapi = null;
        public DocumentListPage(MainWindow mwindow, KRDApi krdapi)
        {
            InitializeComponent();
            this.krdapi = krdapi;
            this.mwindow = mwindow;
            mwindow.logger.LogInfo("Próba pobrania wysłanych dokumentów z bazy danych");
            mwindow.comm.GetDocuments(ref mwindow.documents);

            FillDataGridView(mwindow.documents, dgDocuments);
            

        }

        public void FillDataGridView(List<Document> docs, DataGrid dg)
        {
            mwindow.logger.LogInfo("Wypełnianie danych na liście dokumentów");
            DataTable table = new DataTable();
            //table.Columns.Add("Numer faktury");
            //table.Columns.Add("Kwota brutto");
            //table.Columns.Add("Data wystawienia");
            //table.Columns.Add("Termin spłaty");
            //table.Columns.Add("Kontrahent");
            //table.Columns.Add("NIP kontrahenta");
            //table.Columns.Add("Data wysłania");

            //table.Columns[0].DataType.
            //dg.Rows.Add();
            //DataRow row = table.NewRow();
            
            //dg.ItemsSource = DataTable.AsDataView();
            for (int i = 0; i < docs.Count; i++)
            {
                object[] row = new object[7];
                dgRow drow = new dgRow();
                drow.full_number = docs[i].full_number;
                drow.gross_amount = docs[i].gross_amount.amount;
                drow.creation_date = docs[i].creation_date.ToShortDateString();
                drow.due_date = docs[i].due_date.ToShortDateString();
                drow.contractor_name = docs[i].contractor_name;
                drow.tax_number = docs[i].contractor_tax_number;
                drow.send_date = docs[i].send_date.ToShortDateString();


                //row = dg.row
                //row[0] = docs[i].full_number;
                //row[1] = docs[i].gross_amount.amount;
                //row[2] = docs[i].creation_date.ToShortDateString();
                //row[3] = docs[i].due_date.ToShortDateString();
                //row[4] = docs[i].contractor_name;
                //row[5] = docs[i].contractor_tax_number;
                //row[6] = docs[i].send_date.ToShortDateString();

                //table.Rows.Add(row);  
                dg.Items.Add(drow);
            }
            
            //dg.ItemsSource = table.DefaultView;

        }

        private void bRedo_Click(object sender, RoutedEventArgs e)
        {            
            mwindow.AnimateWindowWidthRight(false);
            
        }

        private void bManage_Click(object sender, RoutedEventArgs e)
        {
            string env = XML.XMLConfig.GetEnvironment();
            if (env == "0")
            {
                Process.Start("https://panel.kaczmarskigroup.pl");
            }
            else if (env == "1")
            {
                Process.Start("https://demo.krd.pl/Client2.0/mod/FairPayPlus/ObligationList/Browse");
            }
        }

        
    }

    public class dgRow
    {
        public string full_number { get; set; }
        public double gross_amount { get; set; }
        public string creation_date { get; set; }
        public string due_date { get; set; }
        public string contractor_name { get; set; }
        public string tax_number { get; set; }
        public string send_date { get; set; }
    }
}
