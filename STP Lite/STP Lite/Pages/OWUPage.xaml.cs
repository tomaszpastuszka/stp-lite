﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using STP_Lite.KRD;
using System.IO;
using STP_Lite.XML;
using Api;
using System.Net;
using Api.Models;

namespace STP_Lite.Pages
{
    /// <summary>
    /// Interaction logic for LicensePage.xaml
    /// </summary>
    public partial class OWUPage : Page
    {
        string regulation_file = @"C:\Users\" + Environment.UserName + @"\Documents\FPEASY\regulation.pdf";

        string regId = "";
        Regulations regulations = null;
        MainWindow mwindow = null;
        KRDApi krdapi = null;
        FPApi fpapi = null;
        string login = "";
        string password = "";

        public OWUPage(KRDApi krdapi, FPApi fpapi, MainWindow mwindow)
        {
            InitializeComponent();
            this.krdapi = krdapi;
            this.fpapi = fpapi;
            this.mwindow = mwindow;

            regulations = fpapi.GetRegulations();


            regId = XMLConfig.GetRegulationsId();
            if (string.IsNullOrEmpty(regId) || regulations.regulationsId != regId)
            {
                try
                {
                    WebClient webClient = new WebClient();
                    webClient.DownloadFile(new Uri(regulations.regulationsUrl), regulation_file);
                }
                catch (Exception ex)
                {

                }
            }

            //if (!File.Exists(regulation_file))
            //{
            //    mwindow.Regulation = krdapi.GetRegulation();
            //    File.WriteAllBytes(regulation_file, mwindow.Regulation.Data);
            //}
        }

        private void cbOWU_CheckedChanged(object sender, RoutedEventArgs e)
        {

            if (cbOWU.IsChecked.Value && owu2.IsChecked.Value)
            {
                bAccept.IsEnabled = true;
            }
            else
            {
                bAccept.IsEnabled = false;
            }
            if (!cbOWU.IsChecked.Value)
            {
                bAccept.IsEnabled = false;
            }
        }

        private void bAccept_Click(object sender, RoutedEventArgs e)
        {
            if (owu2.IsChecked.Value)
            {
                XMLConfig.SetRegDateAccept(DateTime.Now);
            }
            //akceptacja
            var result = fpapi.SendRegulationsAcceptance(regulations.regulationsId, regulations.regulationsUrl);
            if (result == null || result.Count == 0 || result.ContainsKey("11406"))
            {
                XMLConfig.SetOWUAcceptation("1");
                XMLConfig.SetRegulationsId(regulations.regulationsId);
                //wysłąnie owu?

                Pages.MainPage mainPage = new Pages.MainPage(krdapi, fpapi, mwindow);
                mwindow.mainPageHandler = mainPage;
                mwindow.mainFrame.NavigationService.Navigate(mainPage);
            }

        }

        private void TextBlock_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (File.Exists(regulation_file))
            {
                System.Diagnostics.Process.Start(regulation_file);
            }
        }

        private void owu2_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (cbOWU.IsChecked.Value && owu2.IsChecked.Value)
            {
                bAccept.IsEnabled = true;
            }
            else
            {
                bAccept.IsEnabled = false;
            }
            if (!owu2.IsChecked.Value)
            {
                bAccept.IsEnabled = false;
            }
        }
    }
}
