﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using STP_Lite.KRD;
using System.IO;
using STP_Lite.XML;
using Api;

namespace STP_Lite.Pages
{
    /// <summary>
    /// Interaction logic for LicensePage.xaml
    /// </summary>
    public partial class LicensePage : Page
    {
       
        MainWindow mwindow = null;
        KRDApi krdapi = null;
        FPApi fpapi = null;
        string login = "";
        string password = "";
        
        public LicensePage( KRDApi krdapi, FPApi fpapi, MainWindow mwindow)
        {
            InitializeComponent();
            this.krdapi = krdapi;
            this.fpapi = fpapi;
            this.mwindow = mwindow;
            

            
        }
        private void cbLicence_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (cbLicence.IsChecked.Value)
            {
                bAccept.IsEnabled = true;
                mwindow.AnimateWindowWidthRight(false);
            }
            else
            {
                bAccept.IsEnabled = false;
            }
        }

        

        private void bAccept_Click(object sender, RoutedEventArgs e)
        {
            //akcepracja

            XMLConfig.SetLicenseAcceptation(DateTime.Now.ToString());
            XMLConfig.SetLicenseVersion(mwindow.License_Ver.ToString());
            mwindow.AnimateWindowWidthRight(false);
            krdapi.SendOperation(XMLConfig.GetAppID(), "LicenseAccepted");            
            Pages.LoginPage loginPage = new Pages.LoginPage(krdapi, fpapi, mwindow);
            mwindow.mainFrame.NavigationService.Navigate(loginPage);

        }

        private void TextBlock_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //Wyświetl pełną treść licencji


        }

        private void tbShowLic_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Pages.ViewPage view = new ViewPage(mwindow, krdapi, "LICENCE");
            mwindow.dockFrame.NavigationService.Navigate(view);
            mwindow.AnimateWindowWidthLeft();
        }
    }
}
