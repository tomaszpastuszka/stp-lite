﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using STP_Lite.KRD;
using STP_Lite.Structures;
using System.Data;
using System.Data.Common;

namespace STP_Lite.Pages
{
    /// <summary>
    /// Interaction logic for DocumentListPage.xaml
    /// </summary>
    public partial class ViewPage : Page
    {
        MainWindow mwindow = null;
        KRDApi krdapi = null;
        public ViewPage(MainWindow mwindow, KRDApi krdapi, string view)
        {
            InitializeComponent();
            this.krdapi = krdapi;
            this.mwindow = mwindow;

            switch (view)
            {
                case "":
                    {

                    }break;
                case "LICENCE":
                    {
                        tbLicense.Visibility = System.Windows.Visibility.Visible;
                    } break;
            }

            
            

        }

        

        private void bRedo_Click(object sender, RoutedEventArgs e)
        {
            mwindow.AnimateWindowWidthRight(false);
            
        }
    }
}
