﻿using STP_Lite.XML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using STP_Lite.KRD;
using Api;
using Api.Models;

namespace STP_Lite.Pages
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        KRDApi krdapi = null;
        FPApi fpapi = null;
        MainWindow mwindow = null;
        public LoginPage(KRDApi krdapi, FPApi fpapi, MainWindow mwindow)
        {
            InitializeComponent();
            this.krdapi = krdapi;
            this.fpapi = fpapi;
            this.mwindow = mwindow;

            string login = StaticFunctions.Functions.Decrypt(XMLConfig.GetLogin());
            tbLogin.Text = login;

            if (mwindow.loggedIn)
            {
                bBack.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                bBack.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void bLogin_Click(object sender, RoutedEventArgs e)
        {
            string login = StaticFunctions.Functions.Decrypt(XMLConfig.GetLogin());
            string password = StaticFunctions.Functions.Decrypt(XMLConfig.GetPassword());

            if (string.IsNullOrEmpty(login))
            {
                login = tbLogin.Text;
                fpapi.SetUser(login);
            }

            if (string.IsNullOrEmpty(password) || password != tbPassword.Password)
            {
                password = tbPassword.Password;
                fpapi.SetPassword(password);
            }

            if (fpapi.Authorize(login, password))
            {
                Regulations regulations = fpapi.GetRegulations();
                //var regulation = krdapi.LoginToKrdService(login, password);
                if ((regulations != null && regulations.regulationsId != XMLConfig.GetRegulationsId()) || string.IsNullOrEmpty(XMLConfig.GetRegDateAccept()))
                //if (!(regulation.RegulationAcceptedSpecified && regulation.RegulationAccepted) && (regulation.CanManageObligationsSpecified && regulation.CanManageObligations) && (regulation.HasContractOriginalSpecified && regulation.HasContractOriginalSpecified))
                {
                    mwindow.logger.LogInfo("Wysyłanie komunikatu: UserLogin");
                    krdapi.SendOperation(XMLConfig.GetAppID(), "UserLogin");
                    krdapi.LastLogin = DateTime.Now;
                    mwindow.SetLoggedIn(true);
                    if (tbLogin.Text != StaticFunctions.Functions.Decrypt(XMLConfig.GetLogin()))
                    {
                        XMLConfig.SetLogin(StaticFunctions.Functions.Encrypt(tbLogin.Text));
                    }
                    if (password != StaticFunctions.Functions.Decrypt(XMLConfig.GetPassword()))
                    {
                        XMLConfig.SetPassword(StaticFunctions.Functions.Encrypt(password));
                    }

                    mwindow.logger.LogInfo("Ładowanie okna akceptacji OWU");
                    Pages.OWUPage owuPage = new OWUPage(krdapi, fpapi, mwindow);
                    mwindow.mainFrame.NavigationService.Navigate(owuPage);


                }
                else if (regulations != null && regulations.regulationsId == XMLConfig.GetRegulationsId())
                {
                    mwindow.logger.LogInfo("Wysyłanie komunikatu: UserLogin");
                    krdapi.SendOperation(XMLConfig.GetAppID(), "UserLogin");
                    krdapi.LastLogin = DateTime.Now;
                    mwindow.SetLoggedIn(true);
                    if (tbLogin.Text != StaticFunctions.Functions.Decrypt(XMLConfig.GetLogin()))
                    {
                        XMLConfig.SetLogin(StaticFunctions.Functions.Encrypt(tbLogin.Text));
                    }
                    if (password != StaticFunctions.Functions.Decrypt(XMLConfig.GetPassword()))
                    {
                        XMLConfig.SetPassword(StaticFunctions.Functions.Encrypt(password));
                    }

                    //XMLConfig.SetEnvironment(cbEnvironment.SelectedIndex.ToString());
                    mwindow.logger.LogInfo("Poprawnie zalogowano użytkownika, login: " + login);


                    Pages.MainPage mainPage = new MainPage(krdapi, fpapi, mwindow);
                    mwindow.mainPageHandler = mainPage;
                    mwindow.mainFrame.NavigationService.Navigate(mainPage);


                }
            }
            else
            {
                MessageBox.Show("Podano nieprawidłowy login lub hasło");
            }
            //else if (!(regulation.RegulationAcceptedSpecified && regulation.RegulationAccepted) && !(regulation.CanManageObligationsSpecified && regulation.CanManageObligations) && (regulation.HasContractOriginalSpecified && regulation.HasContractOriginal))
            //{
            //    MessageBox.Show("Wybacz, ale by korzystać z konektora Systemu FairPay musisz aneksować swoją umowę. Skontaktuj się z obsługą FairPay pod numerem ...");
            //}

            //else if (!regulation.RegulationAcceptedSpecified && !regulation.CanManageObligationsSpecified && !regulation.HasContractOriginalSpecified)
            //{
            //    MessageBox.Show("Wybacz, ale wpisałeś niepoprawne dane lub nie posiadasz dostępu do usługi");
            //}

            //else if (!(regulation.RegulationAcceptedSpecified && regulation.RegulationAccepted) && (regulation.CanManageObligationsSpecified && regulation.CanManageObligations) && !(regulation.HasContractOriginalSpecified && regulation.HasContractOriginal))
            //{
            //    MessageBox.Show("Przykro nam, ale Twoja umowa nie została jeszcze zarejestrowana. Proszę, skontaktuj się ze swoim opiekunem pod numerem ...");
            //}

            //else if ((regulation.RegulationAcceptedSpecified && regulation.RegulationAccepted) && !(regulation.CanManageObligationsSpecified && regulation.CanManageObligations) && (regulation.HasContractOriginalSpecified && regulation.HasContractOriginal))
            //{
            //    MessageBox.Show("Nie masz uprawnień, by przekazywać faktury.");
            //}
            XMLConfig.SetFirstRun("0");
        }



        private void bNoLogin_Click(object sender, RoutedEventArgs e)
        {
            Pages.MainPage mainPage = new MainPage(krdapi, fpapi, mwindow);
            mwindow.mainPageHandler = mainPage;
            mwindow.mainFrame.NavigationService.Navigate(mainPage);
            mwindow.loggedIn = false;
        }

        private void bBack_Click(object sender, RoutedEventArgs e)
        {
            if (mwindow.loggedIn)
            {
                Pages.MainPage mainPage = new MainPage(krdapi, fpapi, mwindow);
                mwindow.mainPageHandler = mainPage;
                mwindow.mainFrame.NavigationService.Navigate(mainPage);
            }
        }
    }
}
