﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using STP_Lite.KRD;
using STP_Lite.GUS;
using STP_Lite.Structures;
using STP_Lite.OtherStructures;
using System.Diagnostics;
using System.IO;
using System.Xml.Linq;
using System.Windows.Resources;
using System.Windows.Media.Animation;
using System.ComponentModel;
using System.Threading;
using Api;

namespace STP_Lite.Pages
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        KRDApi krdapi = null;
        FPApi fpapi = null;
        MainWindow mwindow = null;
        GUSApi gusapi = null;
        Comm comm = null;
        Pages.DocumentPage dockPage = null;
        long contractorNIP;
        bool initialized = false;
        bool nipFromCombo = false;
        public List<ComboBoxPair> comboBoxPairs = null;
        bool confirmCaptcha = false;
        Storyboard sb = null;
        ImageBrush myBrush = new ImageBrush();
        BackgroundWorker bwgGetCaptcha = null;
        BackgroundWorker bwgGetDataFromGUS = null;
        BitmapImage imgSourceCaptcha = new BitmapImage();
        public string textSourceCaptcha = "";

        public MainPage(KRDApi krdapi, FPApi fpapi, MainWindow mwindow)
        {
            mwindow.logger.LogInfo("Załadowano MainPage");
            InitializeComponent();


            this.krdapi = krdapi;
            this.fpapi = fpapi;
            this.mwindow = mwindow;
            this.comm = mwindow.comm;
            //this.gusapi = new GUSApi(mwindow);


            bwgGetCaptcha = new BackgroundWorker();
            bwgGetCaptcha.DoWork += bwgGetCaptcha_DoWork;
            bwgGetCaptcha.RunWorkerCompleted += bwgGetCaptcha_Completed;

            bwgGetDataFromGUS = new BackgroundWorker();
            bwgGetDataFromGUS.DoWork += bwgGetDataFromGUS_DoWork;
            bwgGetDataFromGUS.RunWorkerCompleted += bwgGetDataFromGUS_Completed;

            //var uiDisp = Dispatcher.CurrentDispatcher;
            //bwgGetCaptcha.DoWork += (sender, e) => uiDisp.BeginInvoke(new Action(() => { UpdateCaptcha(e.Result as ImageSource); }));

            PrepareComboBoxContractors();
            sb = this.FindResource("StoryboardWait") as Storyboard;

            //cbContractors.MouseWheel += cbContractors_MouseWheel;
            cbContractors.PreviewMouseWheel += cbContractors_PreviewMouseWheel;
            initialized = true;

        }

        void cbContractors_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
        }



        private void tbNIP_TextChanged(object sender, TextChangedEventArgs e)
        {
            string nip = "";
            string tempNIP = tbNIP.Text;
            tempNIP = tempNIP.Replace("PL", "");
            if (initialized)
            {
                try
                {
                    if (!string.IsNullOrEmpty(tbNIP.Text) && tbNIP.Text != "lub wpisz NIP")
                    {
                        tempNIP = Utility.RemoveIncorrectCharactersFromNIP(tempNIP);
                        nip = Utility.ClearNip(tempNIP);
                    }


                    if (mwindow.ParseNIP(ref nip))
                    {
                        mwindow.NIPFromMainPage = nip;
                        mwindow.correctNIPMainPage = true;
                        tbNIP.BorderBrush = Brushes.RoyalBlue;
                        if (!nipFromCombo)
                        {
                            mwindow.foundContrator = mwindow.contractors.Find(o => o.tax_number == nip);
                            if (!object.ReferenceEquals(mwindow.foundContrator, null))
                            {
                                mwindow.SetFoundContractorData(mwindow.foundContrator);

                                if (!mwindow.big_window)
                                {
                                    mwindow.AnimateWindowWidthLeft();
                                }

                            }
                            else
                            {
                                bNext.IsEnabled = true;
                            }
                        }
                    }
                    else
                    {
                        if (tbNIP.Text != "lub wpisz NIP")
                        {
                            tbNIP.BorderBrush = Brushes.Red;
                            bNext.IsEnabled = false;
                        }
                        else
                        {
                            tbNIP.BorderBrush = Brushes.White;
                        }
                    }
                    if (tempNIP != tbNIP.Text)
                    {
                        tbNIP.Text = tempNIP;
                        tbNIP.SelectionStart = tbNIP.Text.Length;
                    }
                }
                catch (Exception ex)
                {
                    mwindow.logger.LogMessage("Błąd obsługi tbNIP \n" + ex.Message);
                }
            }
        }

        private void bNext_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var regulations = fpapi.GetRegulations();
                if (!mwindow.loggedIn || (regulations != null && regulations.regulationsId == XML.XMLConfig.GetRegulationsId()))
                {
                    if (!string.IsNullOrEmpty(tbNIP.Text) && tbNIP.Text != "lub wpisz NIP")
                    {

                        bNext.IsEnabled = false;
                        ShowWaitAnimation();
                        mwindow.sending = true;

                        string nip = Utility.ClearNip(tbNIP.Text.Trim());
                        tbNIP.Text = nip;
                        string arg = "";

                        if (object.ReferenceEquals(gusapi, null))
                        {
                            gusapi = new GUSApi(mwindow);
                        }

                        if (!gusapi.SessionExist())
                        {
                            mwindow.logger.LogInfo("Sesja wygasła. Ponowne logowanie");
                            gusapi.Login();
                        }

                        RunGetDataFromGUS(nip);

                        //if (!bwgGetCaptcha.IsBusy)
                        //{
                        //    arg = nip.ToString();
                        //    bwgGetCaptcha.RunWorkerAsync(arg);
                        //}
                        //else
                        //{
                        //    bNext.IsEnabled = true;
                        //}
                    }
                }
                else if(mwindow.loggedIn)
                {
                    mwindow.AcceptRegulation = false;
                    XML.XMLConfig.SetOWUAcceptation("0");
                    MessageBox.Show("Uwaga!!! Zmieniły się ogólne warunki umowy. Zostaniesz przeniesiony do okna akceptacji OWU");
                    Pages.OWUPage owuPage = new OWUPage(krdapi, fpapi, mwindow);
                    mwindow.mainFrame.NavigationService.Navigate(owuPage);
                }
            }
            catch (Exception ex)
            {
                mwindow.logger.LogMessage("Błąd obsługi bNext \n" + ex.Message);
            }
            /*
        else 
        {

            mwindow.foundContrator = new Contractor();
            mwindow.SetFoundContractorData(mwindow.foundContrator);
            if (!mwindow.big_window)
            {
                mwindow.AnimateWindowWidthLeft(mwindow);
            }
            else
            {
                mwindow.AnimateWindowWidthLeft(mwindow);
            }
            bNext.IsEnabled = false;
        }
        */

        }

        public void PrepareComboBoxContractors()
        {
            mwindow.contractors.Clear();
            comm.GetContractors(ref mwindow.contractors);
            List<Contractor> SortedList = mwindow.contractors.OrderBy(o => o.name).ToList();
            comboBoxPairs = new List<ComboBoxPair>();
            ComboBoxPair comboPair = null;
            comboPair = new ComboBoxPair("Wybierz płatnika", "0");
            comboBoxPairs.Add(comboPair);
            foreach (var contractor in SortedList)
            {
                comboPair = new ComboBoxPair(contractor.name, contractor.tax_number);
                comboBoxPairs.Add(comboPair);
            }
            cbContractors.DisplayMemberPath = "_Key";
            cbContractors.SelectedValuePath = "_Value";
            cbContractors.ItemsSource = comboBoxPairs;
            //cbContractors.Text = "Lista płatników";

        }

        public void PrepareComboBoxContractorsAfterAddContractor(Contractor contr)
        {
            mwindow.contractors.Clear();
            comm.GetContractors(ref mwindow.contractors);
            List<Contractor> SortedList = mwindow.contractors.OrderBy(o => o.name).ToList();
            comboBoxPairs = new List<ComboBoxPair>();
            ComboBoxPair comboPair = null;
            comboPair = new ComboBoxPair("Wybierz płatnika", "0");
            comboBoxPairs.Add(comboPair);
            foreach (var contractor in SortedList)
            {
                comboPair = new ComboBoxPair(contractor.name, contractor.tax_number);
                comboBoxPairs.Add(comboPair);
            }
            cbContractors.DisplayMemberPath = "_Key";
            cbContractors.SelectedValuePath = "_Value";
            cbContractors.ItemsSource = comboBoxPairs;
            //cbContractors.Text = "Lista płatników";
            cbContractors.SelectedValue = contr.tax_number;


        }

        private void cbContractors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cbContractors.SelectedIndex == 0)
                {
                    SetDEfaultState();
                }


                var regulations = fpapi.GetRegulations();
                if (!mwindow.loggedIn || (regulations != null && regulations.regulationsId == XML.XMLConfig.GetRegulationsId()))
                {
                    string nip = (string)cbContractors.SelectedValue;



                    if (nip != "0")
                    {

                        Contractor c = mwindow.contractors.Find(o => o.tax_number == nip);

                        dockPage = new DocumentPage(krdapi, fpapi, mwindow);

                        if (!object.ReferenceEquals(c, null))
                        {
                            //mwindow.dockFrame.Navigated += dockFrame_Navigated;
                            nipFromCombo = true;
                            tbNIP.Text = nip;
                            dockPage.FillContractorFields(c);
                            //mwindow.SetFoundContractorData(c);
                            //mwindow.dockFrame.NavigationService.Navigate(dockPage);
                            //mwindow.AnimateWindowWidthRight(true, dockPage);
                            mwindow.AnimationDoubleSlide(dockPage);

                            nipFromCombo = false;
                            bNext.IsEnabled = false;
                        }
                    }
                    else
                    {
                        tbNIP_LostFocus(tbNIP, null);
                        if (mwindow.big_window)
                        {
                            mwindow.AnimateWindowWidthRight(false);
                        }
                    }
                }
                else if(mwindow.loggedIn)
                {
                    mwindow.AcceptRegulation = false;
                    XML.XMLConfig.SetOWUAcceptation("0");
                    MessageBox.Show("Uwaga!!! Zmieniły się ogólne warunki umowy. Zostaniesz przeniesiony do okna akceptacji OWU");
                    Pages.OWUPage owuPage = new OWUPage(krdapi, fpapi, mwindow);
                    mwindow.mainFrame.NavigationService.Navigate(owuPage);
                }
            }
            catch (Exception ex)
            {
                mwindow.logger.LogMessage("Błąd obsługi cbContractors \n" + ex.Message);
            }

        }

        private void tbNIP_GotFocus(object sender, RoutedEventArgs e)
        {
            if (initialized)
            {
                TextBox tb = (TextBox)sender;

                SetDEfaultState();
                mwindow.AnimateWindowWidthRight(false);
                tb.Text = string.Empty;
            }
        }

        private void tbNIP_LostFocus(object sender, RoutedEventArgs e)
        {
            if (initialized)
            {
                TextBox tb = (TextBox)sender;
                if (string.IsNullOrEmpty(tb.Text) || tb.Text == "0")
                {
                    tb.Text = "lub wpisz NIP";
                }

            }

        }

        private void bToKRD_Click(object sender, RoutedEventArgs e)
        {
            var env = XML.XMLConfig.GetEnvironment();
            if (env == "0")
            {
                mwindow.logger.LogInfo("Wciśnięto przycisk \"Sprawdź w krajowym rejestrze długów BIG\"");
                Process.Start("https://panel.kaczmarskigroup.pl");
                mwindow.logger.LogInfo("Otworzono przeglądarkę ze stroną: https://panel.kaczmarskigroup.pl");
                mwindow.logger.sendMail();
            }
            else if (env == "1")
            {
                mwindow.logger.LogInfo("Wciśnięto przycisk \"Sprawdź w krajowym rejestrze długów BIG\"");
                Process.Start("https://demo.krd.pl/Client2.0/Search/StartSearching");
                mwindow.logger.LogInfo("Otworzono przeglądarkę ze stroną: https://demo.krd.pl/Client2.0/Search/StartSearching");
                mwindow.logger.sendMail();
            }
        }



        private void bSeatings_Click(object sender, RoutedEventArgs e)
        {
            mwindow.loggedIn = false;
            fpapi.Logout();
            mwindow.logger.LogInfo("Wciśnięto przycisk \"Seatings\"");
            if (mwindow.big_window)
            {
                mwindow.AnimateWindowWidthRight(false);
            }

            Pages.LoginPage loginPage = new Pages.LoginPage(krdapi, fpapi, mwindow);
            mwindow.mainFrame.NavigationService.Navigate(loginPage);

            //Pages.ViewPage view = new ViewPage(mwindow, krdapi, "LICENCE");
            //mwindow.dockFrame.NavigationService.Navigate(view);
            //mwindow.AnimateWindowWidthLeft();

            //Pages.LicensePage loginPage = new Pages.LicensePage(krdapi, mwindow);
            //mwindow.mainFrame.NavigationService.Navigate(loginPage);
            //Forms.ComunicatForm c = new Forms.ComunicatForm("Error", "", mwindow);
            //c.ShowDialog();
        }

        public Contractor GetContractorDataFromGUS(string tax_number)
        {
            mwindow.logger.LogInfo("Uruchomiono funkcję: GetContractorDataFromGUS");
            XDocument documentXML = null;
            XDocument documentXMLRaport = null;
            string simpleData = "";
            string fullData = "";
            Contractor contractor = null;
            try
            {

                mwindow.logger.LogInfo("Próba pobrania podstawowych danych o kontrahencie z GUS: GetDane");
                try
                {
                    documentXML = gusapi.GetDane(tax_number);
                }
                catch (Exception ex)
                {
                    mwindow.logger.LogMessage("Błąd pobierania podstawowych danych o kontrahencie z GUS: GetDane \n" + ex.Message);
                }
                if (!object.ReferenceEquals(documentXML, null))
                {
                    string regon = documentXML.Root.Element("dane").Element("Regon").Value;
                    string type = documentXML.Root.Element("dane").Element("Typ").Value;
                    string silos = documentXML.Root.Element("dane").Element("SilosID").Value;
                    string raportName = "";
                    switch (silos)
                    {
                        case "":
                            {
                                raportName = "";
                            } break;
                        case "1":
                            {

                                raportName = "PublDaneRaportDzialalnoscFizycznejCeidg";
                            } break;
                        case "2":
                            {

                                raportName = "PublDaneRaportDzialalnoscFizycznejRolnicza";
                            } break;
                        case "3":
                            {

                                raportName = "PublDaneRaportDzialalnoscFizycznejPozostala";
                            } break;
                        case "4":
                            {

                                raportName = "PublDaneRaportDzialalnoscFizycznejWKrupgn";
                            } break;
                        case "6":
                            {
                                raportName = "PublDaneRaportPrawna";
                            } break;

                    }
                    if (!string.IsNullOrEmpty(raportName))
                    {
                        try
                        {
                            documentXMLRaport = gusapi.GetFullRaport(regon, raportName);
                            if (string.IsNullOrEmpty(documentXMLRaport.ToString()))
                            {
                                mwindow.logger.LogMessage("Metoda GetFullRaport nic nie zwróciła");
                            }
                        }
                        catch (Exception ex)
                        {
                            mwindow.logger.LogMessage("Błąd pobierania pełnych danych o kontrahencie z GUS: GetFullRaport \n" + ex.Message);
                        }
                        if (!object.ReferenceEquals(documentXMLRaport, null))
                        {
                            contractor = new Contractor();
                            if (type == "P" || type == "LP")
                            {
                                //prawna
                                contractor.name = documentXMLRaport.Root.Element("dane").Element("praw_nazwa").Value.Trim();
                                contractor.tax_number = documentXMLRaport.Root.Element("dane").Element("praw_nip").Value;
                                contractor.acronym = documentXMLRaport.Root.Element("dane").Element("praw_nazwaSkrocona").Value.Trim();
                                contractor.address.post_code = documentXMLRaport.Root.Element("dane").Element("praw_adSiedzKodPocztowy").Value.Trim();
                                contractor.address.post = documentXMLRaport.Root.Element("dane").Element("praw_adSiedzMiejscowoscPoczty_Nazwa").Value.Trim();
                                contractor.address.building = documentXMLRaport.Root.Element("dane").Element("praw_adSiedzNumerNieruchomosci").Value.Trim();
                                contractor.address.flat = documentXMLRaport.Root.Element("dane").Element("praw_adSiedzNumerLokalu").Value.Trim();
                                contractor.contact.phone = documentXMLRaport.Root.Element("dane").Element("praw_numerTelefonu").Value.Trim();
                                contractor.contact.email = documentXMLRaport.Root.Element("dane").Element("praw_adresEmail").Value.Trim();
                                contractor.address.country = documentXMLRaport.Root.Element("dane").Element("praw_adSiedzKraj_Nazwa").Value.Trim();
                                contractor.address.city = documentXMLRaport.Root.Element("dane").Element("praw_adSiedzMiejscowosc_Nazwa").Value.Trim();
                                contractor.address.street = documentXMLRaport.Root.Element("dane").Element("praw_adSiedzUlica_Nazwa").Value.Trim();

                                if (string.IsNullOrEmpty(contractor.address.street))
                                {
                                    contractor.address.street = contractor.address.city;
                                }

                            }
                            else
                            {
                                //fizyczna
                                contractor.name = documentXMLRaport.Root.Element("dane").Element("fiz_nazwa").Value.Trim();
                                contractor.tax_number = tax_number;
                                contractor.acronym = documentXMLRaport.Root.Element("dane").Element("fiz_nazwaSkrocona").Value.Trim();
                                contractor.address.post_code = documentXMLRaport.Root.Element("dane").Element("fiz_adSiedzKodPocztowy").Value.Trim();
                                contractor.address.post = documentXMLRaport.Root.Element("dane").Element("fiz_adSiedzMiejscowoscPoczty_Nazwa").Value.Trim();
                                contractor.address.building = documentXMLRaport.Root.Element("dane").Element("fiz_adSiedzNumerNieruchomosci").Value.Trim();
                                contractor.address.flat = documentXMLRaport.Root.Element("dane").Element("fiz_adSiedzNumerLokalu").Value.Trim();
                                contractor.contact.phone = documentXMLRaport.Root.Element("dane").Element("fiz_numerTelefonu").Value.Trim();
                                //var mail = documentXMLRaport.Root.Element("dane").Element("fiz_adresEmail").Value.Trim();
                                contractor.contact.email = documentXMLRaport.Root.Element("dane").Element("fiz_adresEmail").Value.Trim();
                                contractor.address.country = documentXMLRaport.Root.Element("dane").Element("fiz_adSiedzKraj_Nazwa").Value.Trim();
                                contractor.address.city = documentXMLRaport.Root.Element("dane").Element("fiz_adSiedzMiejscowosc_Nazwa").Value.Trim();
                                contractor.address.street = documentXMLRaport.Root.Element("dane").Element("fiz_adSiedzUlica_Nazwa").Value.Trim();

                                if (string.IsNullOrEmpty(contractor.address.street))
                                {
                                    contractor.address.street = contractor.address.city;
                                }
                            }
                        }
                    }

                }
                else
                {
                    mwindow.logger.LogMessage("Metoda GetDane nic nie zwróciła");
                }


            }
            catch (Exception ex)
            {
                mwindow.logger.LogMessage("Błąd pobierania kontrahenta. sid = " + gusapi.sid + "\" NIP:  " + tax_number + " \n" + ex.Message);
            }




            return contractor;
        }

        private void mGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void Page_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void bOK_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Process.Start("https://www3.krd.pl/client2.0/Authentication/Login");
        }

        private void bDockList_Click(object sender, RoutedEventArgs e)
        {
            Pages.DocumentListPage docListPage = new DocumentListPage(mwindow, krdapi);

            if (mwindow.dockFrame.Content.GetType() != docListPage.GetType())
            {
                mwindow.AnimationDoubleSlide(docListPage);
            }
            else
            {
                if (mwindow.big_window)
                {
                    mwindow.AnimateWindowWidthRight(false);
                }
                else
                {
                    mwindow.AnimateWindowWidthLeft(docListPage);
                }

            }

        }

        //void dockFrame_Navigated(object sender, NavigationEventArgs e)
        //{

        //    if (initialized)
        //    {
        //        mwindow.AnimateWindowWidthLeft();
        //    }
        //    initialized = true;
        //}


        private void bToTray_Click(object sender, RoutedEventArgs e)
        {
            if (mwindow.WindowState == System.Windows.WindowState.Normal)
            {
                mwindow.AnimateWindowWidthRight(false);
                if (!mwindow.sending)
                {
                    SetDEfaultState();
                }
                //mwindow.Hide();
                mwindow.ShowInTaskbar = true;
                mwindow.WindowState = System.Windows.WindowState.Minimized;
            }
        }

        public void SetDEfaultState()
        {
            cbContractors.SelectedIndex = 0;
            tbNIP.Text = "lub wpisz NIP";
            bNext.IsEnabled = false;
        }

        public void SetInternetConnection(bool isNet)
        {
            Uri oUri = null;
            BitmapImage bi = null;


            if (isNet)
            {
                //oUri = new Uri("/STP Lite;component/Resources/Network_Connection.png", UriKind.RelativeOrAbsolute);
                //bi = new BitmapImage(oUri);
                //netImg.Source = bi;
                netImg.Visibility = System.Windows.Visibility.Hidden;

            }
            else
            {
                netImg.Visibility = System.Windows.Visibility.Visible;
                oUri = new Uri("/FP Easy;component/Resources/NoNetwork_Connection.png", UriKind.RelativeOrAbsolute);
                bi = new BitmapImage(oUri);
                netImg.Source = bi;
            }
        }

        public void ShowWaitAnimation()
        {
            ellipse.Visibility = System.Windows.Visibility.Visible;
            if (!object.ReferenceEquals(sb, null))
            {
                sb.Begin();
            }
        }

        public void HideWaitAnimation()
        {
            ellipse.Visibility = System.Windows.Visibility.Hidden;
            if (!object.ReferenceEquals(sb, null))
            {
                sb.Stop();
            }
        }

        private void bwgGetCaptcha_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            string option = (string)e.Result;

            if (option == "CAPTCHA")
            {
                mwindow.logger.LogInfo("bwgGetCaptcha zwróciła Captcha");
                Point position = new Point(500, 400);
                Forms.CaptchaForm cForm = new Forms.CaptchaForm(position, mwindow, imgSourceCaptcha, tbNIP.Text);
                cForm.ShowDialog();

                //RunGetDataFromGUS(Utility.ClearNip(tbNIP.Text.Trim()));

                bNext.IsEnabled = false;
            }
            else if (option == "DANE")
            {
                mwindow.logger.LogInfo("bwgGetCaptcha zwróciła dane kontrahenta");
                if (!object.ReferenceEquals(mwindow.foundContrator, null))
                {
                    mwindow.comm.SaveContractor(mwindow.foundContrator);
                    mwindow.SetFoundContractorData(mwindow.foundContrator);
                    PrepareComboBoxContractorsAfterAddContractor(mwindow.foundContrator);
                    tbNIP.Text = mwindow.foundContrator.tax_number;
                    mwindow.AnimationDoubleSlide(null);
                    mwindow.sending = false;
                    HideWaitAnimation();

                }
            }


        }

        private void bwgGetCaptcha_DoWork(object sender, DoWorkEventArgs e)
        {
            mwindow.logger.LogInfo("Podjęcie próby pobrania Captcha");
            string arg = (string)e.Argument;

            if (object.ReferenceEquals(gusapi, null))
            {
                gusapi = new GUSApi(mwindow);
            }

            if (!gusapi.SessionExist())
            {
                mwindow.logger.LogInfo("Sesja wygasła. Ponowne logowanie");
                gusapi.Login();
            }

            Dispatcher.Invoke(() =>
            {
                var img = gusapi.GetCaptcha();
                //UpdateCaptcha(img);
                imgSourceCaptcha = img;
            });

            if (!object.ReferenceEquals(imgSourceCaptcha, null))
            {
                e.Result = "CAPTCHA";
                return;
            }
            else
            {
                mwindow.foundContrator = GetContractorDataFromGUS(arg);
                e.Result = "DANE";
                return;
            }

        }

        private void bwgGetDataFromGUS_Completed(object sender, RunWorkerCompletedEventArgs e)
        {

            //HideWaitAnimation();
            if (!object.ReferenceEquals(mwindow.foundContrator, null))
            {
                mwindow.logger.LogInfo("Znaleziono dane kontrahenta w funkcji bwgGetDataFromGUS");
                mwindow.comm.SaveContractor(mwindow.foundContrator);
                mwindow.SetFoundContractorData(mwindow.foundContrator);
                PrepareComboBoxContractorsAfterAddContractor(mwindow.foundContrator);
                tbNIP.Text = mwindow.foundContrator.tax_number;
                mwindow.AnimationDoubleSlide(null);



            }
            else
            {
                //gdy nie znajdzie danych
                mwindow.logger.LogInfo("Nie znaleziono danych kontrahenta w funkcji bwgGetDataFromGUS");
                mwindow.foundContrator = new Contractor();
                mwindow.SetFoundContractorData(mwindow.foundContrator);
                mwindow.AnimationDoubleSlide(null);
                if (!object.ReferenceEquals(mwindow.mainPageHandler, null))
                {
                    mwindow.mainPageHandler.tbNIP.Text = (string)e.Result;
                }

            }
            mwindow.sending = false;
            HideWaitAnimation();
        }

        private void bwgGetDataFromGUS_DoWork(object sender, DoWorkEventArgs e)
        {
            mwindow.logger.LogInfo("Podjęcie próby pobrania danych kontrahenta w funkcji bwgGetDataFromGUS");
            string args = e.Argument as string;

            string[] tabArg = args.Split(';');


            if (true)//gusapi.CheckCaptcha(tabArg[1]))
            {
                mwindow.foundContrator = GetContractorDataFromGUS(tabArg[0]);
            }
            else
            {
                System.Windows.MessageBox.Show("Błąd we wprowadzonym kodzie captcha!");
            }
            e.Result = tabArg[0];

        }

        private void UpdateCaptcha(BitmapImage img)
        {
            //image1.Source = img;
        }

        public void RunGetDataFromGUS(string nip, string textCaptcha  = "")
        {
            string args = nip + ";" + textCaptcha;
            if (!bwgGetDataFromGUS.IsBusy)
            {                
                bwgGetDataFromGUS.RunWorkerAsync(args);
            }

        }

        public void DisableMainPageControls()
        {
            //IsEnabled = false;
            tbNIP.IsEnabled = false;
            cbContractors.IsEnabled = false;
            bNext.IsEnabled = false;
            bSeatings.IsEnabled = false;
            bToKRD.IsEnabled = false;
        }

        public void EnableMainPageControls()
        {
            IsEnabled = true;
            tbNIP.IsEnabled = true;
            cbContractors.IsEnabled = true;
            bNext.IsEnabled = false;
            bSeatings.IsEnabled = true;
            bToKRD.IsEnabled = true;
        }
    }
}
