﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STP_Lite.XML;
using System.Windows.Forms;
using STP_Lite.Structures;
using System.Data.OleDb;
using STP_Lite.KrdStructures;


namespace STP_Lite
{
    public class Comm
    {
        public string databasepath = @"C:\Users\" + Environment.UserName + @"\Documents\FPEASY\database.mdb";
        public Logger logger = new Logger();
        //public List<Module> modules = new List<Module>();

        public Result GetContractor(int tax_number)
        {
            Result res = new Result();

            OleDbDataReader reader = null;
            OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + XMLConfig.databasefileName + "; Jet OLEDB:Engine Type=5");
            connection.Open();
            //int integ_type = (int)document.integration_type;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = connection;
            cmd.Transaction = connection.BeginTransaction();
            cmd.CommandText = "SELECT * FROM CONTRACTORS WHERE tax_number = " + tax_number.ToString();
            Document document = new Structures.Document();
            try
            {
                reader = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                return res;
            }
            while (reader.Read())
            {
                document.contractor.external_id = Convert.ToInt32(reader["id"].ToString());
                //document.contractor.integration_type = (IntegType)Convert.ToInt32(reader["integration_type"]);
                document.contractor.name = reader["name"].ToString();
                document.contractor.tax_number = reader["tax_number"].ToString();
                document.contractor.address.building = reader["nr"].ToString();
                document.contractor.address.flat = reader["flat"].ToString();
                document.contractor.address.city = reader["city"].ToString();
                document.contractor.address.post_code = reader["post_code"].ToString();
                document.contractor.contact.phone = reader["phone"].ToString();
                document.contractor.contact.email = reader["email"].ToString();
                document.contractor.contact.name = reader["contact_firstname"].ToString();
                document.contractor.contact.surname = reader["contact_lastname"].ToString();
                document.contractor.contact.role = reader["contact_role"].ToString();
                //if (!string.IsNullOrEmpty(reader["FirstName"].ToString()) && !string.IsNullOrEmpty(reader["LastName"].ToString()))
                //{
                //    document.contractor.firstname = reader["FirstName"].ToString();
                //    document.contractor.lastname = reader["LastName"].ToString();
                //    document.contractor.entreprenuer = true;
                //}

            }
            reader.Close();
            connection.Close();

            res.status = 1;
            return res;
        }

        public Result GetContractors(ref List<Contractor> contractors)
        {

            Result res = new Result();            
            OleDbDataReader reader = null;
            OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                res.status = 0;
                res.msg = "Błąd otwierania bazy danych.";
                logger.LogMessage("Błąd otwierania bazy danych w funkcji GetContractors. " + ex.Message);
                return res;
            }



            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = connection;
            cmd.Transaction = connection.BeginTransaction();
            cmd.CommandText = "SELECT * FROM Contractors";

            try
            {
                reader = cmd.ExecuteReader();
                contractors.Clear();
                while (reader.Read())
                {
                    Contractor contr = new Contractor();

                    contr.external_id = Convert.ToInt32(reader["id"]);
                    contr.acronym = reader["acronym"].ToString();
                    contr.name = reader["name"].ToString();
                    contr.tax_number = reader["tax_number"].ToString();
                    contr.address.street = reader["address"].ToString();
                    contr.address.building = reader["nr"].ToString();
                    contr.address.flat = reader["flat"].ToString();
                    contr.address.city = reader["city"].ToString();
                    contr.address.post_code = reader["post_code"].ToString();
                    contr.address.post = reader["post"].ToString();
                    contr.address.country = reader["country"].ToString();
                    contr.contact.email = reader["email"].ToString();
                    contr.contact.phone = reader["phone"].ToString();
                    contr.bank_account = reader["bank_account"].ToString();

                    contractors.Add(contr);

                }



                res.status = 1;
            }
            catch (Exception ex)
            {
                res.status = 0;
                logger.LogMessage("Błąd czytania danych z bazy danych w funkcji GetContractors. " + ex.Message);
                return res;
            }
            
            return res;
        }

        public Result SaveContractor(Contractor contractor)
        {
            Result res = new Result();
            OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + XMLConfig.databasefileName + "; Jet OLEDB:Engine Type=5");
            connection.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = connection;
            cmd.Transaction = connection.BeginTransaction();
            cmd.CommandText = "SELECT COUNT(tax_number) FROM Contractors WHERE tax_number = \"" + contractor.tax_number.ToString() +"\"";
            var count = cmd.ExecuteScalar();

            if (Convert.ToInt32(count) == 0)
            {
                cmd.CommandText = "INSERT INTO Contractors(name, tax_number, address, nr, flat, city, post_code, email, phone, bank_account, creation_date, post, country, last_modify) " +
                                                   "VALUES(@name, @tax_number, @address, @nr, @flat, @city, @post_code, @email, @phone, @bank_account, @creation_date, @post, @country, @last_modify)";

                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("?", contractor.name);
                cmd.Parameters.AddWithValue("?", contractor.tax_number);
                cmd.Parameters.AddWithValue("?", contractor.address.street);
                cmd.Parameters.AddWithValue("?", contractor.address.building);
                cmd.Parameters.AddWithValue("?", contractor.address.flat);
                cmd.Parameters.AddWithValue("?", contractor.address.city);
                cmd.Parameters.AddWithValue("?", contractor.address.post_code);
                cmd.Parameters.AddWithValue("?", contractor.contact.email);
                cmd.Parameters.AddWithValue("?", contractor.contact.phone);
                cmd.Parameters.AddWithValue("?", contractor.bank_account);
                cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
                cmd.Parameters.AddWithValue("?", contractor.address.post);
                cmd.Parameters.AddWithValue("?", contractor.address.country);
                cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
                
                try
                {
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    res.status = 1;
                }
                catch (Exception ex)
                {
                    res.status = 0;
                    res.msg = "Błąd w funkcji SaveContractor. " + ex.Message;
                    logger.LogMessage("Błąd zapisu do bazy danych w funkcji SaveContractor. " + ex.Message);
                }
            }
            //else
            //{
            //    res = UpdateExcludeContractor(cmd, contractor);
            //}
            cmd.Transaction.Commit();
            connection.Close();

            return res;
        }

        public Result SaveSentDocument(Document document)
        {
            Result res = new Result();
            OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + XMLConfig.databasefileName + "; Jet OLEDB:Engine Type=5");
            connection.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = connection;
            cmd.Transaction = connection.BeginTransaction();

            cmd.CommandText = "INSERT INTO SentDocuments(full_number, gross_amount, net_amount, bank_account, doc_currency, creation_date, sent, send_date, user_id, due_date, contractor_tax_number, contractor_name) " +
                                             "VALUES(@full_number, @gross_amount, @net_amount, @bank_account, @doc_currency, @creation_date, @sent, @send_date, @user_id, @due_date, @contractor_tax_number, @contractor_name)";

            cmd.Parameters.AddWithValue("?", document.full_number);
            cmd.Parameters.AddWithValue("?", document.gross_amount.amount);
            cmd.Parameters.AddWithValue("?", document.net_amount.amount);
            cmd.Parameters.AddWithValue("?", document.bank_account);
            cmd.Parameters.AddWithValue("?", document.gross_amount.currency);
            cmd.Parameters.AddWithValue("?", document.creation_date);
            cmd.Parameters.AddWithValue("?", document.sent);
            cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
            cmd.Parameters.AddWithValue("?", document.identifiers.userId);
            cmd.Parameters.AddWithValue("?", document.due_date);
            cmd.Parameters.AddWithValue("?", document.contractor.tax_number);
            cmd.Parameters.AddWithValue("?", document.contractor.name);

            try
            {
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                res.status = 1;
            }
            catch (Exception ex)
            {
                res.status = 0;
                res.msg = "Błąd w funkcji SaveSentDocument. " + ex.Message;
                logger.LogMessage("Błąd zapisu do bazy danych w funkcji SaveSentDocument. " + ex.Message);
            }

            cmd.Transaction.Commit();
            connection.Close();
            return res;
        }

        public Result UpdateContractor(Contractor contractor)
        {
            Result ret = new Result();

            OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                ret.status = 0;
                ret.msg = "Błąd otwierania bazy danych.";
                logger.LogMessage("Błąd otwierania bazy danych w funkcji UpdateContractor. " + ex.Message);
                return ret;
            }

            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = connection;
            cmd.Transaction = connection.BeginTransaction();
            cmd.CommandText = "UPDATE Contractors SET last_modify = @last_modify, name = @name, " +
                                                    "tax_number = @tax_number, address = @address, nr = @nr, flat = @flat, city = @city, post_code = @post_code, " +
                                                    "email = @email, phone = @phone, post = @post, bank_account = @bank_account " +
                              "WHERE tax_number = @tax_number";

            
            cmd.Parameters.AddWithValue("?", DateTime.Now.ToShortDateString());
            cmd.Parameters.AddWithValue("?", contractor.name);
            cmd.Parameters.AddWithValue("?", contractor.tax_number);
            cmd.Parameters.AddWithValue("?", contractor.address.street);
            cmd.Parameters.AddWithValue("?", contractor.address.building);
            cmd.Parameters.AddWithValue("?", contractor.address.flat);
            cmd.Parameters.AddWithValue("?", contractor.address.city);
            cmd.Parameters.AddWithValue("?", contractor.address.post_code);
            cmd.Parameters.AddWithValue("?", contractor.contact.email);
            cmd.Parameters.AddWithValue("?", contractor.contact.phone);
            cmd.Parameters.AddWithValue("?", contractor.address.post);
            cmd.Parameters.AddWithValue("?", contractor.bank_account); 

            //where
            cmd.Parameters.AddWithValue("?", contractor.tax_number);
              
            try
            {
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();


            }
            catch (Exception ex)
            {
                ret.status = 0;
                //ret.msg = "Błąd aktualizacji danych kontrahenta.";
                logger.LogMessage("Błąd w funkcji UpdateContractor. " + ex.Message);
                logger.LogMessage("Błąd zapisu do bazy danych w funkcji UpdateContractor. " + ex.Message);
            }
                       

            cmd.Transaction.Commit();
            cmd.Connection.Close();
            ret.status = 1;

            return ret;
        }

        public Result GetDocuments(ref List<Document> documents)
        {
            Result res = new Result();
            OleDbDataReader reader = null;
            OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                res.status = 0;
                //res.msg = "Błąd otwierania bazy danych.";
                logger.LogMessage("Błąd otwierania bazy danych w funkcji GetContractors. " + ex.Message);
                return res;
            }



            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = connection;
            cmd.Transaction = connection.BeginTransaction();
            cmd.CommandText = "SELECT id, full_number, gross_amount, net_amount, bank_account, doc_currency, creation_date, " +
                              "send_date, sent, user_id, contractor_tax_number, contractor_id, contractor_name, due_date " +
                              "FROM SentDocuments";
                              

            try
            {
                reader = cmd.ExecuteReader();
                documents.Clear();
                while (reader.Read())
                {
                    Document doc = new Document();

                    doc.external_id = Convert.ToInt32(reader["id"]);
                    doc.full_number = reader["full_number"].ToString();
                    doc.gross_amount.amount = Convert.ToDouble(reader["gross_amount"]);
                    doc.net_amount.amount = Convert.ToDouble(reader["net_amount"]);
                    doc.bank_account = reader["bank_account"].ToString();
                    doc.gross_amount.currency = reader["doc_currency"].ToString();
                    doc.net_amount.currency = reader["doc_currency"].ToString();
                    doc.creation_date = DateTime.Parse(reader["creation_date"].ToString());
                    doc.send_date = DateTime.Parse(reader["send_date"].ToString());
                    doc.identifiers.userId = reader["user_id"].ToString();
                    //doc.contractor_id = Convert.ToInt32(reader["contractor_id"]);
                    doc.contractor_tax_number = reader["contractor_tax_number"].ToString();
                    doc.sent = Convert.ToBoolean(reader["sent"]);
                    doc.contractor_name = reader["contractor_name"].ToString();
                    doc.due_date = DateTime.Parse(reader["due_date"].ToString());
                                      

                    documents.Add(doc);

                }



                res.status = 1;
            }
            catch (Exception ex)
            {
                res.status = 0;
                logger.LogMessage("Błąd czytania danych z bazy danych w funkcji GetDocuments. " + ex.Message);
                return res;
            }

            return res;
        }

        public string GenerateUserId(string full_number)
        {
            string sguid = StaticFunctions.Functions.Hash(full_number);
            return sguid;
        }

        public Result UpdateSentDate(Document document)
        {
            OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
            connection.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = connection;
            cmd.Transaction = connection.BeginTransaction();

            Result ret = new Result();
            cmd.CommandText = "UPDATE Documents SET send_date = @send_date, last_modify = @last_modify WHERE external_id = @external_id";
            cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
            cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
            cmd.Parameters.AddWithValue("?", document.external_id);
            //cmd.Parameters.AddWithValue("?", Convert.ToInt32(document.integration_type));
            cmd.Parameters.AddWithValue("?", document.company_id);

            try
            {
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                ret.status = 1;
            }
            catch (Exception ex)
            {
                ret.status = 0;
                ret.msg = "Błąd aktualizacji send_date.";
                logger.LogMessage("Błąd w funkcji UpdateSentDate. " + ex.Message);

            }
            cmd.Transaction.Commit();
            connection.Close();
            return ret;
        }

        public void UpdateWeeklyReportValues(OleDbCommand cmd, int sent_count, double login_time, int week_id)
        {
            //cmd.CommandText = "UPDATE WeeklyStatistics SET dictionary_value = '" + qualified_count.ToString() + "' WHERE dictionary_key = 'qualified' AND week_id = " + week_id.ToString();
            //cmd.ExecuteNonQuery();

            cmd.CommandText = "UPDATE WeeklyStatistics SET dictionary_value = '" + sent_count.ToString() + "' WHERE dictionary_key = 'delivered' AND week_id = " + week_id.ToString();
            cmd.ExecuteNonQuery();

            //cmd.CommandText = "UPDATE WeeklyStatistics SET dictionary_value = '" + lack_count.ToString() + "' WHERE dictionary_key = 'lacks' AND week_id = " + week_id.ToString();
            //cmd.ExecuteNonQuery();

            cmd.CommandText = "UPDATE WeeklyStatistics SET dictionary_value = '" + login_time.ToString() + "' WHERE dictionary_key = 'logged_in_hrs' AND week_id = " + week_id.ToString();
            cmd.ExecuteNonQuery();

            //cmd.CommandText = "UPDATE WeeklyStatistics SET dictionary_value = '" + bufor_count.ToString() + "' WHERE dictionary_key = 'bufor' AND week_id = " + week_id.ToString();
            //cmd.ExecuteNonQuery();

        }

        public Dictionary<string, string> PrepareWeeklyReport(ref int week_id)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
            connection.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = connection;
            cmd.Transaction = connection.BeginTransaction();
            cmd.CommandText = "SELECT MAX(creation_date) AS max_creation_date, week_id FROM WeeklyStatistics WHERE sent = 0 GROUP BY week_id";
            var reader = cmd.ExecuteReader();
            DateTime creation_date = DateTime.Now;
            if (reader.Read())
            {
                week_id = Convert.ToInt32(reader["week_id"]);
                creation_date = Convert.ToDateTime(reader["max_creation_date"]);
            }
            reader.Close();

            double days = (DateTime.Now - creation_date).TotalDays;
            if (days >= 7.0)//daniel
            {
                DateTime from = creation_date;
                DateTime to = from.AddDays(7);

                //int qualified_count = GetCountOfQualifiedDocuments(from, to);
                int sent_count = GetCountOfSentDocuments(from, to);
                //int lack_count = GetCountOfLackDocuments(from, to);
                //int bufor_count = GetCountOfBuforDocuments(from, to);
                double login_time = Convert.ToDouble(XMLConfig.GetLoginTime());
                UpdateWeeklyReportValues(cmd, sent_count, login_time, week_id);

                cmd.CommandText = "SELECT * FROM WeeklyStatistics WHERE week_id = " + week_id.ToString();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ret.Add(reader["dictionary_key"].ToString(), reader["dictionary_value"].ToString());
                }
            }

            connection.Close();
            return ret;
        }

        public void GenerateWeeklyStatistics()
        {
            OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
            connection.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = connection;
            cmd.Transaction = connection.BeginTransaction();
            int week_id = 0;

            cmd.CommandText = "SELECT MAX(week_id) FROM WeeklyStatistics";
            var result = cmd.ExecuteScalar();

            if (object.ReferenceEquals(result, null))
            {
                week_id = 1;
            }
            else
            {
                week_id = Convert.ToInt32(result) + 1;
            }

            //cmd.CommandText = "INSERT INTO WeeklyStatistics (week_id, dictionary_key, dictionary_value, sent, creation_date) VALUES(" + week_id + ", 'qualified', '0', 0, '" + DateTime.Now + "')";
            //cmd.ExecuteNonQuery();
            //cmd.CommandText = "INSERT INTO WeeklyStatistics (week_id, dictionary_key, dictionary_value, sent, creation_date) VALUES(" + week_id + ", 'bufor', '0', 0, '" + DateTime.Now + "')";
            //cmd.ExecuteNonQuery();
            cmd.CommandText = "INSERT INTO WeeklyStatistics (week_id, dictionary_key, dictionary_value, sent, creation_date) VALUES (" + week_id + ", 'delivered', '0', 0, '" + DateTime.Now + "')";
            cmd.ExecuteNonQuery();
            //cmd.CommandText = "INSERT INTO WeeklyStatistics (week_id, dictionary_key, dictionary_value, sent, creation_date) VALUES(" + week_id + ", 'lacks', '0', 0, '" + DateTime.Now + "')";
            //cmd.ExecuteNonQuery();
            cmd.CommandText = "INSERT INTO WeeklyStatistics (week_id, dictionary_key, dictionary_value, sent, creation_date) VALUES(" + week_id + ", 'logged_in_hrs', '0', 0, '" + DateTime.Now + "')";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "INSERT INTO WeeklyStatistics (week_id, dictionary_key, dictionary_value, sent, creation_date) VALUES(" + week_id + ", 'nb_of_loggins', '0', 0, '" + DateTime.Now + "')";
            cmd.ExecuteNonQuery();

            cmd.Transaction.Commit();
            connection.Close();
        }

        public void UpdateWeeklyStatisticsSentDate(int week_id)
        {
            OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
            connection.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = connection;
            cmd.Transaction = connection.BeginTransaction();
            cmd.CommandText = "UPDATE WeeklyStatistics SET sent = true, sent_date = '" + DateTime.Now.ToString() + "' WHERE week_id = " + week_id.ToString();
            cmd.ExecuteNonQuery();
            cmd.Transaction.Commit();
            connection.Close();
        }

        //public void ConnectToApi()
        //{
        //    foreach (var module in modules)
        //    {
        //        if (module.identstr == "optima")
        //        {
        //            if (object.ReferenceEquals(OptimaAssembliesList, null))
        //            {
        //                var res = StaticFunctions.Functions.GetOptimaInstallationPath();
        //                if (res.status == 1)
        //                {
        //                    OptimaAssembliesList = new OptimaDynamicAssembliesList();
        //                    OptimaAssembliesList.GetListOfAssemblies(res.msg);
        //                }
        //                else if (res.status == 0)
        //                {
        //                    logger.LogInfo(res.msg);
        //                    MessageBox.Show("Sprawdź czy zainstalowano system Optima");
        //                    return;
        //                }
        //            }
        //            if (!Integrations.ContainsKey(Convert.ToInt32(module.id)))
        //            {
        //                Integrations.Add(Convert.ToInt32(module.id), new ApiOptima(OptimaAssembliesList));
        //                Integrations[Convert.ToInt32(module.id)].Integration(module);
        //                logger.LogInfo("Załadowano api Optima: " + module.id);
        //            }
        //        }

        //        if (module.identstr == "subiektgt")
        //        {
        //            if (!Integrations.ContainsKey(Convert.ToInt32(module.id)))
        //            {
        //                Integrations.Add(Convert.ToInt32(module.id), new ApiSubiektGT());
        //                Integrations[Convert.ToInt32(module.id)].Integration(module);
        //                logger.LogInfo("Załadowano api SubiektGT: " + module.id);
        //            }
        //        }
        //        if (module.identstr == "wfmag")
        //        {
        //            if (!Integrations.ContainsKey(Convert.ToInt32(module.id)))
        //            {
        //                Integrations.Add(Convert.ToInt32(module.id), new ApiWFMag());
        //                Integrations[Convert.ToInt32(module.id)].Integration(module);
        //                logger.LogInfo("Załadowano api WF-Mag: " + module.id);
        //            }
        //        }
        //        if (module.identstr == "symfonia")
        //        {
        //            if (!Integrations.ContainsKey(Convert.ToInt32(module.id)))
        //            {
        //                Integrations.Add(Convert.ToInt32(module.id), new ApiSymfonia());
        //                Integrations[Convert.ToInt32(module.id)].Integration(module);
        //                logger.LogInfo("Załadowano api Symfonia: " + module.id);
        //            }
        //        }
        //        if (module.identstr == "enova")
        //        {
        //            if (!Integrations.ContainsKey(Convert.ToInt32(module.id)))
        //            {
        //                Integrations.Add(Convert.ToInt32(module.id), new ApiEnova());
        //                Integrations[Convert.ToInt32(module.id)].Integration(module);
        //                logger.LogInfo("Załadowano api Enova: " + module.id);
        //            }
        //        }
        //        if (module.identstr == "excel")
        //        {
        //            if (!Integrations.ContainsKey(Convert.ToInt32(module.id)))
        //            {
        //                Integrations.Add(Convert.ToInt32(module.id), new ApiFile());
        //                Integrations[Convert.ToInt32(module.id)].Integration(module);
        //                logger.LogInfo("Załadowano api Excel: " + module.id);
        //            }
        //        }
        //        if (module.identstr == "speed")
        //        {
        //            if (!Integrations.ContainsKey(Convert.ToInt32(module.id)))
        //            {
        //                Integrations.Add(Convert.ToInt32(module.id), new ApiSPEED());
        //                Integrations[Convert.ToInt32(module.id)].Integration(module);
        //                logger.LogInfo("Załadowano api InterLAN SPEED: " + module.id);
        //            }
        //        }
        //        if (module.identstr == "xl")
        //        {
        //            if (!Integrations.ContainsKey(Convert.ToInt32(module.id)))
        //            {
        //                Integrations.Add(Convert.ToInt32(module.id), new ApiXL());
        //                Integrations[Convert.ToInt32(module.id)].Integration(module);
        //                logger.LogInfo("Załadowano api Comarch XL: " + module.id);
        //            }
        //        }

        //    }
        //}

        //public void DisconnectFromApi()
        //{
        //    foreach (var module in modules)
        //    {
        //        Integrations[Convert.ToInt32(module.id)].Logout();
        //    }
        //}

        //public void GetConfiguration()
        //{
        //    modules = XMLConfig.GetList();
        //    if (modules.Count == 0)
        //    {
        //        AddModulesForm addmodulesf = new AddModulesForm();
        //        addmodulesf.ShowDialog();
        //    }

        //    modules = XMLConfig.GetList();
        //}

        //public Result GetCompanies(ref Dictionary<int, Company> companies, bool user, bool no_internet)
        //{
        //    Result ret = new Result();
        //    ret.status = 1;
        //    List<Company> lcompanies = new List<Company>();
        //    foreach (var module in modules)
        //    {
        //        if (!companies.ContainsKey(Convert.ToInt32(module.id)))
        //        {
        //            if (user || no_internet)
        //            {
        //                companies.Add(Convert.ToInt32(module.id), new Company(Convert.ToInt32(module.company_id), (IntegType)Enum.Parse(typeof(IntegType), module.identstr), module.company_name));
        //            }
        //            else
        //            {
        //                DateTime now = DateTime.Now;
        //                string[] tab_time = module.synchro_time.Split(':');
        //                if (now.Hour == Convert.ToInt32(tab_time[0]) && now.Minute == Convert.ToInt32(tab_time[1]))
        //                {
        //                    companies.Add(Convert.ToInt32(module.id), new Company(Convert.ToInt32(module.company_id), (IntegType)Enum.Parse(typeof(IntegType), module.identstr), module.company_name));
        //                }
        //            }
        //        }
        //    }

        //    return ret;
        //}

        //public Result GetContractors(Dictionary<int, Company> companies, ref Dictionary<Company, List<Contractor>> contractors)
        //{
        //    Result ret = new Result();

        //    foreach (var company in companies)
        //    {
        //        List<Contractor> lcontractors = new List<Contractor>();
        //        var module = modules.Find(o => o.id == company.Key.ToString());
        //        Integrations[company.Key].LoginUser(module.login, module.pass, module.server, module.database, module.export_dir);
        //        ret = Integrations[company.Key].GetContracors(company.Value, ref lcontractors);
        //        if (ret.status == 1)
        //        {
        //            if (!contractors.ContainsKey(company.Value))
        //            {
        //                contractors.Add(company.Value, lcontractors);
        //            }
        //            else
        //            {
        //                contractors[company.Value] = lcontractors;
        //            }
        //        }
        //        Integrations[company.Key].Logout();
        //    }
        //    return ret;
        //}

        //static public int GetContractorIdByTaxId(string tax_id, IntegType integration_type)
        //{
        //    int contractor_id = 0;

        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + XMLConfig.databasefileName + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    int integ_type = (int)integration_type;
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    cmd.CommandText = "SELECT external_id FROM CONTRACTORS WHERE tax_number = '" + tax_id + "' AND integration_type = " + integ_type.ToString();
        //    object result;
        //    try
        //    {
        //        result = cmd.ExecuteScalar();
        //    }
        //    catch (Exception ex)
        //    {
        //        return contractor_id;
        //    }


        //    if(result == null)
        //    {
        //        cmd.CommandText = "SELECT MAX(external_id) + 1 FROM CONTRACTORS WHERE integration_type = " + integ_type.ToString();
        //        result = cmd.ExecuteScalar();
        //        if (result == DBNull.Value)
        //        {
        //            contractor_id = 1;
        //        }
        //        else
        //        {
        //            contractor_id = Convert.ToInt32(result);
        //        }
        //    }
        //    else
        //    {
        //        contractor_id = Convert.ToInt32(result);
        //    }
        //    cmd.Transaction.Commit();
        //    connection.Close();

        //    return contractor_id;
        //}

        //static public string ValidateContractorData(ref Document document)
        //{
        //    string ret = "";
        //    if (document.obligation.entrepreneur != null)
        //    {
        //        if (string.IsNullOrEmpty(document.obligation.entrepreneur.name))
        //        {
        //            ret += "Brak nazwy kontrahenta;";
        //        }

        //        if (string.IsNullOrEmpty(document.obligation.entrepreneur.firstname))
        //        {
        //            ret += "Brak imienia kontrahenta;";
        //        }

        //        if (string.IsNullOrEmpty(document.obligation.entrepreneur.surname))
        //        {
        //            ret += "Brak nazwiska kontrahenta;";
        //        }

        //        if (string.IsNullOrEmpty(document.obligation.entrepreneur.tax_number))
        //        {
        //            ret += "Brak nipu kontrahenta;";
        //        }


        //        ret += document.obligation.entrepreneur.seat_address.ValidateAddress(document.obligation.entrepreneur.seat_address.street, document.obligation.entrepreneur.seat_address.building,
        //                    document.obligation.entrepreneur.seat_address.flat, document.obligation.entrepreneur.seat_address.city, document.obligation.entrepreneur.seat_address.post_code,
        //                    document.obligation.entrepreneur.seat_address.post, document.obligation.entrepreneur.seat_address.country, document.obligation.entrepreneur.seat_address.country);



        //        if (string.IsNullOrEmpty(document.obligation.entrepreneur.contact_data.email))
        //        {
        //            ret += "Niepoprawny adres email osoby kontaktowej;";
        //        }

        //        if (!string.IsNullOrEmpty(document.obligation.entrepreneur.contact_data.phone))
        //        {
        //            if (!KRD.Validators.PhoneNumberValidator.IsValid(document.obligation.entrepreneur.contact_data.phone))
        //            {
        //                ret += "Niepoprawny numer telefonu osoby kontaktowej;";
        //            }
        //        }
        //        else
        //        {
        //            ret += "Brak numeru telefonu osoby kontaktowej";
        //        }
        //    }
        //    else
        //    {
        //        if (string.IsNullOrEmpty(document.obligation.legal_person.name))
        //        {
        //            ret += "Brak nazwy kontrahenta;";
        //        }

        //        if (string.IsNullOrEmpty(document.obligation.legal_person.tax_number))
        //        {
        //            ret += "Brak nipu kontrahenta;";
        //        }


        //        ret += document.obligation.legal_person.seat_address.ValidateAddress(document.obligation.legal_person.seat_address.street, document.obligation.legal_person.seat_address.building,
        //                    document.obligation.legal_person.seat_address.flat, document.obligation.legal_person.seat_address.city, document.obligation.legal_person.seat_address.post_code,
        //                    document.obligation.legal_person.seat_address.post, document.obligation.legal_person.seat_address.country, document.obligation.legal_person.seat_address.country);



        //        if (string.IsNullOrEmpty(document.obligation.legal_person.contact_data.email))
        //        {
        //            ret += "Niepoprawny adres email osoby kontaktowej;";
        //        }

        //        if (!string.IsNullOrEmpty(document.obligation.legal_person.contact_data.phone))
        //        {
        //            if (!KRD.Validators.PhoneNumberValidator.IsValid(document.obligation.legal_person.contact_data.phone))
        //            {
        //                ret += "Niepoprawny numer telefonu osoby kontaktowej;";
        //            }
        //        }
        //        else
        //        {
        //            ret += "Brak numeru telefonu osoby kontaktowej";
        //        }
        //    }
        //    return ret;
        //}

        //public Result GetContractorsWithLacks(ref List<int> contrWithLacks)
        //{
        //    Result res = new Result();
        //    List<int> outList = new List<int>();
        //    OleDbDataReader reader = null;
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");

        //    try
        //    {
        //        connection.Open();
        //    }
        //    catch (Exception ex)
        //    {
        //        res.status = 0;
        //        res.msg = "Błąd otwierania bazy danych.";
        //        logger.LogMessage("Błąd otwierania bazy danych w funkcji GetContractorsWithLacks. " + ex.Message);
        //        return res;
        //    }

        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();

        //    cmd.CommandText = "Select k.external_id from Contractors AS k left join Documents AS d ON k.external_id =  d.contractor_id where ((d.status \\ (2^1)) mod 2 > 0);";
        //    reader = cmd.ExecuteReader();

        //    while (reader.Read())
        //    {
        //        contrWithLacks.Add(Convert.ToInt32(Convert.ToInt32(reader["external_id"])));
        //    }


        //    return res;
        //}

        //public Result GetDocumentForShowStatus(ref List<Document> documents, bool advanced)
        //{
        //    Dictionary<int, Document> tmp_documents = new Dictionary<int, Document>();
        //    Result res = new Result();
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");

        //    try
        //    {
        //        connection.Open();
        //    }
        //    catch (Exception ex)
        //    {
        //        res.status = 0;
        //        res.msg = "Błąd otwierania bazy danych.";
        //        logger.LogMessage("Błąd otwierania bazy danych w funkcji GetDocumentForShowStatus. " + ex.Message);
        //        return res;
        //    }

        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    DateTime oneYearLeft = new DateTime(DateTime.Now.Year - 1, DateTime.Now.Month, 1);
        //    foreach (var mod in modules)
        //    {
        //        IntegType integ_type = (IntegType)Enum.Parse(typeof(IntegType), mod.identstr);
        //        if (!advanced)
        //        {
        //            cmd.CommandText = "SELECT external_id, integration_type, company, company_name, status, full_number, contractor_name, issue_date, gross_amount, creation_date, lacks, contractor_tax_number, bank_account, contractor_id, due_date, net_amount, doc_currency, veta_error FROM Documents " +
        //                                      "WHERE company = " + mod.company_id + " AND integration_type = " + ((int)integ_type).ToString() + " AND company_name = '" + mod.company_name + "' AND creation_date > @date";
        //            cmd.Parameters.AddWithValue("?", oneYearLeft.ToShortDateString());
        //        }
        //        else
        //        {
        //            cmd.CommandText = "SELECT external_id, integration_type, company, company_name, status, full_number, contractor_name, issue_date, gross_amount, creation_date, lacks, contractor_tax_number, bank_account, contractor_id, due_date, net_amount, doc_currency, veta_error FROM Documents " +
        //                                      "WHERE company = " + mod.company_id + " AND integration_type = " + ((int)integ_type).ToString() + " AND company_name  = '" + mod.company_name + "'";
        //        }
        //        var reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            Document doc = new Document();
        //            doc.external_id = Convert.ToInt32(reader["external_id"]);
        //            doc.company_id = Convert.ToInt32(reader["company"]);
        //            doc.company_name = reader["company_name"].ToString();
        //            doc.status = (DocStatus)Convert.ToInt32(reader["status"]);
        //            doc.contractor_name = reader["contractor_name"].ToString();
        //            var issue_read = reader["issue_date"];
        //            if (issue_read != DBNull.Value)
        //            {
        //                doc.issue_date = Convert.ToDateTime(issue_read);
        //            }
        //            doc.full_number = reader["full_number"].ToString();
        //            doc.gross_amount.amount = Convert.ToDouble(reader["gross_amount"]);
        //            doc.net_amount.amount = Convert.ToDouble(reader["net_amount"]);
        //            doc.gross_amount.currency =reader["doc_currency"].ToString();
        //            doc.net_amount.currency = reader["doc_currency"].ToString();

        //            var crea_read = reader["creation_date"];
        //            if (crea_read != DBNull.Value)
        //            {
        //                doc.creation_date = Convert.ToDateTime(crea_read);
        //            }
        //            doc.lacks = reader["lacks"].ToString();
        //            doc.contractor_tax_number = reader["contractor_tax_number"].ToString();
        //            doc.contractor_id = Convert.ToInt32(reader["contractor_id"]);
        //            doc.integration_type = (IntegType)Convert.ToInt32(reader["integration_type"]);
        //            var due_read = reader["due_date"];
        //            if (due_read != DBNull.Value)
        //            {
        //                doc.due_date = Convert.ToDateTime(due_read);
        //            }
        //            doc.bank_account = reader["bank_account"].ToString();
        //            doc.veta_error = Convert.ToBoolean(reader["veta_error"]);

        //            documents.Add(doc);
        //        }
        //        reader.Close();
        //        cmd.Parameters.Clear();
        //    }
        //    res.status = 1;
        //    return res;
        //}

        //public int GetDocumentsLacksCount(List<Document> docs)
        //{
        //    return docs.FindAll(o => o.lacks.Length > 0).Count();
        //}

        //public List<Document> SelectDocuments(List<Document> docs, bool allDocs, bool cfalified, bool excluded, ref int allDocsCount, ref int cfalifiesDocsCount, ref int excludedDocsCount)
        //{
        //    List<Document> outputDocumentList = new List<Document>();
        //    var temp_list_not = docs.FindAll(o => !o.status.HasFlag(DocStatus.EXCLUDED));
        //    var temp_list_exclude = docs.FindAll(o => o.status.HasFlag(DocStatus.EXCLUDED));


        //    allDocsCount = docs.Count();
        //    cfalifiesDocsCount = temp_list_not.Count();
        //    excludedDocsCount = temp_list_exclude.Count();


        //    if (allDocs)
        //    {
        //        outputDocumentList.AddRange(docs);
        //    }
        //    else if (cfalified)
        //    {

        //        if (!object.ReferenceEquals(temp_list_not, null))
        //        {
        //            outputDocumentList.AddRange(temp_list_not);
        //        }
        //        else
        //        {
        //            outputDocumentList.Clear();
        //        }
        //    }
        //    else if (excluded)
        //    {


        //        if (!object.ReferenceEquals(temp_list_exclude, null))
        //        {
        //            outputDocumentList.AddRange(temp_list_exclude);
        //        }
        //        else
        //        {
        //            outputDocumentList.Clear();
        //        }
        //    }



        //    return outputDocumentList;
        //}
        ////public Result GetFilteredDocuments(string filtr, string stats, bool advanced, DateTime at, DateTime to, ref List<Document> documents)
        //{
        //    Result ret = new Result();
        //    List<Document> outputDocuments = new List<Document>();
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    try
        //    {
        //        connection.Open();
        //    }
        //    catch (Exception ex)
        //    {
        //        ret.status = 0;
        //        ret.msg = "Błąd otwierania bazy danych.";
        //        logger.LogMessage("Błąd otwierania bazy danych w funkcji GetExcludedContractors. " + ex.Message);
        //        return ret;
        //    }

        //    OleDbCommand cmd = new OleDbCommand();







        //    return ret;
        //}

        //public Result UpdatetContractorsInLocalDatabase(Dictionary<int, Company> companies, Dictionary<Company, List<Contractor>> contractors)
        //{
        //    Result res = new Result();
        //    List<Contractor> contractorsToAdd = new List<Contractor>();
        //    List<Contractor> contractorsToUpdate = new List<Contractor>();
        //    OleDbDataReader reader = null;
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    try
        //    {
        //        connection.Open();
        //    }
        //    catch (Exception ex)
        //    {
        //        res.status = 0;
        //        res.msg = "Błąd otwierania bazy danych.";
        //        logger.LogMessage("Błąd otwierania bazy danych w funkcji UpdatetContractorsInLocalDatabase. " + ex.Message);
        //        return res;
        //    }

        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();


        //    foreach (var company in companies)
        //    {
        //        var module = modules.Find(o => o.id == company.Key.ToString());
        //        int integration_type = (int)Enum.Parse(typeof(IntegType), module.identstr);
        //        cmd.CommandText = "SELECT external_id, integration_type, company FROM Contractors WHERE integration_type = " + integration_type.ToString() + 
        //                          " AND company = " + module.company_id + " AND company_name = '" + module.company_name + "'";
        //        reader = cmd.ExecuteReader();
        //        List<int> exist_in_base = new List<int>();
        //        while (reader.Read())
        //        {
        //            exist_in_base.Add(Convert.ToInt32(reader["external_id"]));
        //        }
        //        reader.Close();



        //        if (exist_in_base.Count() == 0)
        //        {
        //            contractorsToAdd = contractors[company.Value];
        //            break;
        //        }
        //        else
        //        {
        //            foreach (var contractor in contractors[company.Value])
        //            {
        //                if (exist_in_base.Contains(contractor.external_id))
        //                {
        //                    contractorsToUpdate.Add(contractor);
        //                }
        //                else
        //                {
        //                    contractorsToAdd.Add(contractor);
        //                }
        //            }
        //        }
        //    }


        //    foreach (var contractor in contractorsToAdd)
        //    {
        //        cmd.CommandText = "INSERT INTO Contractors(external_id, integration_type, company, company_name, name, acronym, tax_number, address, nr, flat, city, " +
        //            "post, post_code, country, email, phone1, phone2, phone3, deleted, creation_date, last_modify, excluded, firstName, lastName, contact_firstname, contact_lastname, contact_role) " +
        //            "VALUES(@external_id, @integration_type, @company, @company_name, @name, @acronym, @tax_number, @address, @nr, @flat, @city, @post, @post_code, @country, @email, " +
        //            "@phone1, @phone2, @phone3, @deleted, @creation_date, @last_modify, @excluded, @firstName, @lastName, @contact_firstname, @contact_lastname, @contact_role)";

        //        cmd.Parameters.AddWithValue("?", contractor.external_id);
        //        cmd.Parameters.AddWithValue("?", Convert.ToInt32(contractor.integration_type));
        //        cmd.Parameters.AddWithValue("?", Convert.ToInt32(contractor.company_id));
        //        cmd.Parameters.AddWithValue("?", contractor.company_name);
        //        cmd.Parameters.AddWithValue("?", contractor.name);
        //        cmd.Parameters.AddWithValue("?", contractor.acronym);
        //        cmd.Parameters.AddWithValue("?", contractor.tax_number);
        //        cmd.Parameters.AddWithValue("?", contractor.address.street);
        //        cmd.Parameters.AddWithValue("?", contractor.address.building);
        //        cmd.Parameters.AddWithValue("?", contractor.address.flat);
        //        cmd.Parameters.AddWithValue("?", contractor.address.city);
        //        cmd.Parameters.AddWithValue("?", contractor.address.post);
        //        cmd.Parameters.AddWithValue("?", contractor.address.post_code);
        //        cmd.Parameters.AddWithValue("?", contractor.address.country);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.email);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.phone);
        //        cmd.Parameters.AddWithValue("?", "");
        //        cmd.Parameters.AddWithValue("?", "");
        //        cmd.Parameters.AddWithValue("?", contractor.deleted);
        //        cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
        //        cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
        //        cmd.Parameters.AddWithValue("?", contractor.excluded);
        //        cmd.Parameters.AddWithValue("?", contractor.firstname);
        //        cmd.Parameters.AddWithValue("?", contractor.lastname);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.name);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.surname);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.role);

        //        try
        //        {
        //            cmd.ExecuteNonQuery();
        //            cmd.Parameters.Clear();
        //        }
        //        catch (Exception ex)
        //        {
        //            logger.LogMessage("Błąd dodania kontrahenta do bazy Konektora. " + ex.Message);
        //        }
        //    }
        //    foreach (var contractor in contractorsToUpdate)
        //    {
        //        cmd.CommandText = "UPDATE Contractors SET deleted = @deleted, last_modify = @last_modify, name = @name, " +
        //                           "tax_number = @tax_number, address = @address, nr = @nr, flat = @flat, city = @city, post = @post, post_code = @post_code, " +
        //                           "country = @country, email = @email, phone1 = @phone1, firstName = @firstName, lastName = @lastName, " +
        //                           "contact_firstname = @contact_firstname, contact_lastname = @contact_lastname, contact_role = @contact_role " +
        //                           "WHERE external_id = @external_id AND integration_type = @integration_type AND company = @company AND updated = 0";

        //        cmd.Parameters.AddWithValue("?", contractor.deleted);
        //        cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
        //        cmd.Parameters.AddWithValue("?", contractor.name);
        //        cmd.Parameters.AddWithValue("?", contractor.tax_number);
        //        cmd.Parameters.AddWithValue("?", contractor.address.street);
        //        cmd.Parameters.AddWithValue("?", contractor.address.building);
        //        cmd.Parameters.AddWithValue("?", contractor.address.flat);
        //        cmd.Parameters.AddWithValue("?", contractor.address.city);
        //        cmd.Parameters.AddWithValue("?", contractor.address.post);
        //        cmd.Parameters.AddWithValue("?", contractor.address.post_code);
        //        cmd.Parameters.AddWithValue("?", contractor.address.country);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.email);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.phone);
        //        cmd.Parameters.AddWithValue("?", contractor.firstname);
        //        cmd.Parameters.AddWithValue("?", contractor.lastname);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.name);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.surname);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.role);
        //        cmd.Parameters.AddWithValue("?", contractor.external_id);
        //        cmd.Parameters.AddWithValue("?", Convert.ToInt32(contractor.integration_type));
        //        cmd.Parameters.AddWithValue("?", Convert.ToInt32(contractor.company_id));


        //        try
        //        {
        //            cmd.ExecuteNonQuery();
        //            cmd.Parameters.Clear();
        //        }
        //        catch (Exception ex)
        //        {
        //            logger.LogMessage("Błąd podczas aktualizacji kontrahenta w bazie Konektora. " + ex.Message);
        //        }

        //        cmd.CommandText = "update Documents left join Contractors on Contractors.external_id = Documents.contractor_id " +
        //                          "set Documents.contractor_name = @contractor_name, Documents.contractor_tax_number = @contractor_tax_number " +
        //                          "where Documents.contractor_id = @contractor_id and Contractors.updated = 0;";

        //        cmd.Parameters.AddWithValue("?", contractor.name);
        //        cmd.Parameters.AddWithValue("?", contractor.tax_number);

        //        cmd.Parameters.AddWithValue("?", contractor.external_id);

        //        try
        //        {
        //            cmd.ExecuteNonQuery();
        //            cmd.Parameters.Clear();
        //        }
        //        catch (Exception ex)
        //        {
        //            logger.LogMessage("Błąd podczas aktualizacji danych kontrahenta w tabeli documenów w bazie Konektora. " + ex.Message);
        //        }

        //    }
        //    cmd.Transaction.Commit();
        //    connection.Close();
        //    return res;
        //}



        //public Result GetExcludedContractors(ref Dictionary<Company, List<Contractor>> integcontractors, ref Dictionary<Company, List<Contractor>> excluded_contractors)
        //{
        //    Result ret = new Result();
        //    foreach (var integcontr in integcontractors)
        //    {
        //        if(integcontr.Value.Count > 0)
        //        {
        //            var excluded_list = integcontr.Value.FindAll(o => o.excluded == true);
        //            if(excluded_list != null)
        //            {
        //                if(!excluded_contractors.ContainsKey(integcontr.Key))
        //                {
        //                    excluded_contractors.Add(integcontr.Key, excluded_list);
        //                }
        //                else
        //                {
        //                    excluded_contractors[integcontr.Key].AddRange(excluded_list);
        //                }
        //            }
        //        }
        //    }
        //    ret.status = 1;

        //    /*excluded_contractors.Clear();
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    try
        //    {
        //        connection.Open();
        //    }
        //    catch (Exception ex)
        //    {
        //        ret.status = 0;
        //        ret.msg = "Błąd otwierania bazy danych.";
        //        logger.LogMessage("Błąd otwierania bazy danych w funkcji GetExcludedContractors. " + ex.Message);
        //        return ret;
        //    }

        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    cmd.CommandText = "SELECT company, company_name, integration_type, external_id, excluded FROM Contractors WHERE deleted = no and excluded = yes";
        //    try
        //    {
        //        var reader = cmd.ExecuteReader();

        //        while (reader.Read())
        //        {
        //            Company cmpkey = new Company();
        //            cmpkey.id = Convert.ToInt32(reader["company"]);
        //            cmpkey.integ_type = (IntegType)Convert.ToInt32(reader["integration_type"]);
        //            cmpkey.name = reader["company_name"].ToString();
        //            if (integcontractors.ContainsKey(cmpkey))
        //            {
        //                if (!excluded_contractors.ContainsKey(cmpkey))
        //                {
        //                    excluded_contractors.Add(cmpkey, new List<Contractor>());
        //                }

        //                var excluded_contractor = integcontractors[cmpkey].Find(o => o.external_id == Convert.ToInt32(reader["external_id"]));
        //                excluded_contractor.excluded = Convert.ToBoolean(reader["excluded"]);
        //                if (!object.ReferenceEquals(excluded_contractor, null))
        //                {
        //                    excluded_contractor.deleted = false;
        //                }
        //                excluded_contractors[cmpkey].Add(excluded_contractor);

        //            }
        //        }
        //        reader.Close();
        //        cmd.Transaction.Commit();
        //        ret.status = 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        ret.status = 0;
        //        ret.msg = "Bład pobierania wykluczonych kontrahentów.";
        //        logger.LogMessage("Błąd w funkcji GetExcludedContractors. " + ex.Message);
        //    }
        //    connection.Close();*/
        //    return ret;
        //}



        //public static Result SaveContractorAfterExclude(Contractor contractor)
        //{
        //    Result res = new Result();
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + XMLConfig.databasefileName + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    cmd.CommandText = "SELECT COUNT(tax_number) FROM Contractors WHERE tax_number = '" + contractor.tax_number + "' AND company = " + contractor.company_id + " AND integration_type = " + Convert.ToInt32(contractor.integration_type).ToString();
        //    var count = cmd.ExecuteScalar();

        //    if (Convert.ToInt32(count) == 0)
        //    {
        //        cmd.CommandText = "INSERT INTO Contractors(external_id, integration_type, company, company_name, name, acronym, tax_number, address, nr, flat, city, " +
        //        "post_code, email, phone1, phone2, phone3, deleted, creation_date, last_modify, excluded, updated, post, country, firstname, lastname, contact_firstname, contact_lastname, contact_role) " +
        //        "VALUES(@external_id, @integration_type, @company, @company_name, @name, @acronym, @tax_number, @address, @nr, @flat, @city, @post_code, @email, " +
        //        "@phone1, @phone2, @phone3, @deleted, @creation_date, @last_modify, @excluded, @updated, @post, @country, @firstname, @lastname, @contact_firstname, @contact_lastname, @contact_role)";

        //        cmd.Parameters.AddWithValue("?", contractor.external_id);
        //        cmd.Parameters.AddWithValue("?", Convert.ToInt32(contractor.integration_type));
        //        cmd.Parameters.AddWithValue("?", Convert.ToInt32(contractor.company_id));
        //        cmd.Parameters.AddWithValue("?", contractor.company_name);
        //        cmd.Parameters.AddWithValue("?", contractor.name);
        //        cmd.Parameters.AddWithValue("?", contractor.acronym);
        //        cmd.Parameters.AddWithValue("?", contractor.tax_number);
        //        cmd.Parameters.AddWithValue("?", contractor.address.street);
        //        cmd.Parameters.AddWithValue("?", contractor.address.building);
        //        cmd.Parameters.AddWithValue("?", contractor.address.flat);
        //        cmd.Parameters.AddWithValue("?", contractor.address.city);
        //        cmd.Parameters.AddWithValue("?", contractor.address.post_code);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.email);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.phone);
        //        cmd.Parameters.AddWithValue("?", "");
        //        cmd.Parameters.AddWithValue("?", "");
        //        cmd.Parameters.AddWithValue("?", contractor.deleted);
        //        cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
        //        cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
        //        cmd.Parameters.AddWithValue("?", contractor.excluded);
        //        bool updated = false;
        //        cmd.Parameters.AddWithValue("?", updated);
        //        cmd.Parameters.AddWithValue("?", contractor.address.post);
        //        cmd.Parameters.AddWithValue("?", contractor.address.country);
        //        cmd.Parameters.AddWithValue("?", contractor.firstname);
        //        cmd.Parameters.AddWithValue("?", contractor.lastname);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.name);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.surname);
        //        cmd.Parameters.AddWithValue("?", contractor.contact.role);

        //        try
        //        {
        //            cmd.ExecuteNonQuery();
        //            cmd.Parameters.Clear();
        //            res.status = 1;
        //        }
        //        catch (Exception ex)
        //        {
        //            res.status = 0;
        //            res.msg = "Błąd w funkcji SetContractorAsExcluded. " + ex.Message;
        //        }
        //    }
        //    //else
        //    //{
        //    //    res = UpdateExcludeContractor(cmd, contractor);
        //    //}
        //    cmd.Transaction.Commit();
        //    connection.Close();

        //    return res;
        //}

        //public Result UpdateDocument(Document document)
        //{
        //    Result res = new Result();

        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    try
        //    {
        //        connection.Open();
        //    }
        //    catch (Exception ex)
        //    {
        //        res.status = 0;
        //        res.msg = "Błąd otwierania bazy danych.";
        //        logger.LogMessage("Błąd otwierania bazy danych w funkcji UpdateDocument. " + ex.Message);
        //        return res;
        //    }

        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    cmd.CommandText = "UPDATE Documents SET status = @status, last_modify = @last_modify, full_number = @full_number, gross_amount = @gross_amount, " +
        //                           "issue_date = @issue_date, bank_account = @bank_account, due_date = @due_date, updated = @updated, net_amount = @net_amount, doc_currency = @doc_currency " +
        //                           "WHERE external_id = @external_id AND integration_type = @integration_type AND company = @company";

        //    document.status &= ~DocStatus.NO_DOCUMENT_DATA;
        //    cmd.Parameters.AddWithValue("?", (int)document.status);
        //    cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
        //    cmd.Parameters.AddWithValue("?", document.full_number);
        //    cmd.Parameters.AddWithValue("?", document.gross_amount.amount.ToString());
        //    cmd.Parameters.AddWithValue("?", document.creation_date.ToShortDateString());
        //    cmd.Parameters.AddWithValue("?", document.bank_account);
        //    cmd.Parameters.AddWithValue("?", document.due_date.ToShortDateString());
        //    bool updated = true;
        //    cmd.Parameters.AddWithValue("?", updated);
        //    cmd.Parameters.AddWithValue("?", document.net_amount.amount);
        //    cmd.Parameters.AddWithValue("?", document.gross_amount.currency);

        //    cmd.Parameters.AddWithValue("?", document.external_id);
        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(document.integration_type));
        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(document.company_id));

        //    try
        //    {
        //        cmd.ExecuteNonQuery();               
        //        cmd.Transaction.Commit();
        //        cmd.Parameters.Clear();
        //        res.status = 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        res.status = 0;
        //        res.msg = "Błąd aktualizacji danych kontrahenta.";
        //        logger.LogMessage("Błąd w funkcji UpdateDocument. " + ex.Message);

        //    }

        //    return res;
        //}



        //public static Result UpdateExcludeContractor(OleDbCommand cmd, Contractor contractor)
        //{
        //    Result ret = new Result();
        //    cmd.CommandText = "UPDATE Contractors SET deleted = @deleted, last_modify = @last_modify, excluded = @excluded WHERE external_id = @external_id AND integration_type = @integration_type AND company = @company AND company_name = @company_name";
        //    cmd.Parameters.AddWithValue("?", contractor.deleted);
        //    cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
        //    cmd.Parameters.AddWithValue("?", contractor.excluded);
        //    cmd.Parameters.AddWithValue("?", contractor.external_id);
        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(contractor.integration_type));
        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(contractor.company_id));
        //    cmd.Parameters.AddWithValue("?", contractor.company_name);
        //    if(!string.IsNullOrEmpty(contractor.tax_number))
        //    {
        //        cmd.CommandText += " AND tax_number = @tax_number";
        //        cmd.Parameters.AddWithValue("?", contractor.tax_number);
        //    }

        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //        cmd.Parameters.Clear();
        //        ret.status = 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        ret.status = 0;
        //        ret.msg = "Błąd w funkcji UpdateContractor. " + ex.Message;
        //    }
        //    return ret;
        //}

        //public Result GetDocuments(ref Dictionary<Company, List<Contractor>> excluded_contractors, ref Dictionary<Company, List<Document>> documents, ref Dictionary<Company, List<Document>> good_documents, ref Dictionary<Company, List<Document>> lack_documents, ref Dictionary<Company, List<Repayment>> repayments, ref Dictionary<Company, List<Document>> excluded_obligations, ref List<Document> obligations_to_send, ref List<Document> lack_obligations, ref List<Document> corrected_obligations, ref List<Document> bufor_obligations, ref List<Repayment> repayments_to_send, bool user, bool no_internet, KonektorMainForm mform)
        //{
        //    Result res = new Result();

        //    GetModules();
        //    ConnectToApi();

        //    Dictionary<int, Company> companies = new Dictionary<int, Company>();
        //    Dictionary<Company, List<Contractor>> contractors = new Dictionary<Company, List<Contractor>>(new Company.EqualityComparer());
        //    Dictionary<Company, List<Document>> documents_to_remove = new Dictionary<Company, List<Document>>(new Company.EqualityComparer());
        //    res = GetCompanies(ref companies, user, no_internet);
        //    if (res.status == 0)
        //    {
        //        res.status = 1;
        //        return res;
        //    }
        //    res = GetContractors(companies, ref contractors);
        //    if (res.status == 0)
        //    {
        //        res.status = 2;
        //        return res;
        //    }
        //    UpdatetContractorsInLocalDatabase(companies, contractors);
        //    GetContractorsFromLocalDatabase(companies, ref contractors);

        //    res = GetExcludedContractors(ref contractors, ref excluded_contractors);
        //    if (res.status == 0)
        //    {
        //        res.status = 3;
        //        return res;
        //    }
        //    var latest = GetLatestDocument(companies);

        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    Dictionary<Company, List<Document>> ddocuments = new Dictionary<Company, List<Document>>(new Company.EqualityComparer());

        //    foreach (var company in companies)
        //    {
        //        if (latest[company.Value] >= 0)
        //        {
        //            if (!ddocuments.ContainsKey(company.Value))
        //            {
        //                ddocuments.Add(company.Value, new List<Document>());
        //            }
        //            DateTime dt = Convert.ToDateTime(XMLConfig.GetLicenseAcceptation());
        //            dt = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
        //            cmd.CommandText = "SELECT d.external_id, d.integration_type, d.company, d.company_name, d.status, d.lacks, d.gross_amount, d.paid_amount, d.send_date, " +
        //                              "d.package_id, d.user_id, d.case_closed, " +
        //                              "d.corrections_count, d.full_number, d.creation_date, d.bank_account, d.net_amount, d.doc_currency, d.due_date, " +
        //                              "c.external_id as c_external_id , c.integration_type as c_integration_type, c.company as c_company, c.company_name as c_company_name, " +
        //                              "c.name, c.acronym, c.tax_number, c.address, c.city, c.post_code, c.phone1, c.nr, c.flat, " +
        //                              "c.post, c.country, c.email, c.firstName, c.lastName, c.contact_firstname, c.contact_lastname, c.contact_role " + 
        //                              "FROM Documents d LEFT JOIN Contractors c ON c.company = d.company AND c.integration_type = d.integration_type AND c.external_id = d.contractor_id " +
        //                              "WHERE d.company = " + company.Value.id.ToString() + " AND d.integration_type = " + Convert.ToInt32(company.Value.integ_type).ToString() +
        //                              " AND d.case_closed = No AND veta_error = No AND issue_date >= @issue_date";

        //            cmd.Parameters.Add("?", dt);
        //            var reader = cmd.ExecuteReader();
        //            cmd.Parameters.Clear();
        //            while (reader.Read())
        //            {
        //                Document doc = new Document();
        //                doc.external_id = Convert.ToInt32(reader["external_id"]);
        //                doc.integration_type = (IntegType)Convert.ToInt32(reader["integration_type"]);
        //                doc.company_id = Convert.ToInt32(reader["company"]);
        //                doc.company_name = reader["company_name"].ToString();
        //                doc.status = (DocStatus)Convert.ToInt32(reader["status"]);
        //                doc.lacks = reader["lacks"].ToString();
        //                doc.gross_amount.amount = Convert.ToDouble(reader["gross_amount"]);
        //                if (reader["paid_amount"] != DBNull.Value)
        //                {
        //                    doc.paid_amount.amount = Convert.ToDouble(reader["paid_amount"]);
        //                }
        //                doc.identifiers.userId = reader["user_id"].ToString();
        //                doc.identifiers.response = new Guid(reader["package_id"].ToString());
        //                doc.case_closed = Convert.ToBoolean(reader["case_closed"]);
        //                if (reader["send_date"] != DBNull.Value)
        //                {
        //                    doc.send_date = Convert.ToDateTime(reader["send_date"]);
        //                }

        //                if (reader["corrections_count"] != DBNull.Value)
        //                {
        //                    doc.corrections_count = Convert.ToInt32(reader["corrections_count"]);
        //                }
        //                else
        //                {
        //                    doc.corrections_count = 0;
        //                }
        //                doc.full_number = reader["full_number"].ToString();
        //                doc.creation_date = Convert.ToDateTime(reader["creation_date"]);
        //                //doc.bank_account = reader["bank_account"].ToString();
        //                if (reader["net_amount"] != DBNull.Value)
        //                {
        //                    doc.net_amount.amount = Convert.ToDouble(reader["net_amount"]);
        //                }
        //                doc.gross_amount.currency = reader["doc_currency"].ToString();
        //                doc.net_amount.currency = reader["doc_currency"].ToString();
        //                doc.due_date = Convert.ToDateTime(reader["due_date"]);

        //                /*doc.contractor.external_id = Convert.ToInt32(reader["c_external_id"]);
        //                doc.contractor.integration_type = (IntegType)Convert.ToInt32(reader["c_integration_type"]);
        //                doc.contractor.company_id = Convert.ToInt32(reader["company"]);
        //                doc.contractor.company_name = reader["c_company_name"].ToString();
        //                doc.contractor.name = reader["name"].ToString();
        //                doc.contractor.firstname = reader["firstName"].ToString();
        //                doc.contractor.lastname = reader["lastName"].ToString();
        //                doc.contractor.acronym = reader["acronym"].ToString();
        //                doc.contractor.tax_number = reader["tax_number"].ToString();
        //                doc.contractor.address.street = reader["address"].ToString();
        //                doc.contractor.address.building = reader["nr"].ToString();
        //                doc.contractor.address.flat = reader["flat"].ToString();
        //                doc.contractor.address.post = reader["post"].ToString();
        //                doc.contractor.address.post_code = reader["post_code"].ToString();
        //                doc.contractor.address.country = reader["country"].ToString();
        //                doc.contractor.contact.name = reader["contact_firstname"].ToString();
        //                doc.contractor.contact.surname = reader["contact_lastname"].ToString();
        //                doc.contractor.contact.email = reader["email"].ToString();
        //                doc.contractor.contact.phone = reader["phone1"].ToString();
        //                doc.contractor.contact.role = reader["contact_role"].ToString();*/

        //                ddocuments[company.Value].Add(doc);
        //            }
        //            reader.Close();
        //        }
        //        var module = modules.Find(o => o.id == company.Key.ToString());

        //        if (module.identstr == "symfonia")
        //        {
        //            var buf = ddocuments[company.Value].FindAll(o => o.status.HasFlag(DocStatus.BUFOR));
        //            if (buf != null)
        //            {
        //                DeleteBuforDocument(module.identstr, buf, cmd);
        //                bufor_obligations.Clear();
        //            }

        //            ddocuments[company.Value].RemoveAll(o => o.status.HasFlag(DocStatus.BUFOR));
        //        }

        //        Integrations[company.Key].LoginUser(module.login, module.pass, module.server, module.database, module.export_dir);

        //        res = Integrations[company.Key].GetDocuments(company.Value, excluded_contractors, latest, ref ddocuments, mform);
        //        if (res.status == 1)
        //        {
        //            foreach (var document in ddocuments[company.Value])
        //            {
        //                document.contractor_tax_number = document.contractor.tax_number;

        //                if (document.status.HasFlag(DocStatus.NEW))
        //                {
        //                    document.identifiers.userId = GenerateUserId(document.full_number);
        //                    document.status &= ~DocStatus.NEW;
        //                }
        //            }



        //            if (!documents.ContainsKey(company.Value))
        //            {
        //                documents.Add(company.Value, new List<Document>());
        //            }
        //            documents[company.Value].AddRange(ddocuments[company.Value]);


        //            if (!excluded_obligations.ContainsKey(company.Value))
        //            {
        //                excluded_obligations.Add(company.Value, new List<Document>());
        //            }

        //            if (!excluded_contractors.ContainsKey(company.Value))
        //            {
        //                excluded_contractors.Add(company.Value, new List<Contractor>());
        //            }

        //            excluded_obligations[company.Value].AddRange(documents[company.Value].FindAll(d => d.status.HasFlag(DocStatus.EXCLUDED)));
        //            if (excluded_contractors.ContainsKey(company.Value))
        //            {
        //                foreach (var document in documents[company.Value])
        //                {
        //                    foreach (var excluded_contractor in excluded_contractors[company.Value])
        //                    {
        //                        if (document.contractor_id == excluded_contractor.external_id && !document.status.HasFlag(DocStatus.SENT))
        //                        {
        //                            if (!excluded_obligations[company.Value].Contains(document))
        //                            {
        //                                document.status |= DocStatus.EXCLUDED;
        //                                excluded_obligations[company.Value].Add(document);
        //                            }
        //                        }
        //                    }
        //                }
        //            }


        //            if (!lack_documents.ContainsKey(company.Value))
        //            {
        //                lack_documents.Add(company.Value, new List<Document>());
        //            }
        //            lack_documents[company.Value].AddRange(documents[company.Value].FindAll(o => (o.status.HasFlag(DocStatus.NO_CONTR_DATA) || o.status.HasFlag(DocStatus.NO_DOCUMENT_DATA)) && o.days > 3 && !o.status.HasFlag(DocStatus.SENT) && !o.status.HasFlag(DocStatus.EXCLUDED)));

        //            if (!documents_to_remove.ContainsKey(company.Value))
        //            {
        //                documents_to_remove.Add(company.Value, new List<Document>());
        //            }
        //            documents_to_remove[company.Value].AddRange(documents[company.Value].FindAll(o => (o.status.HasFlag(DocStatus.NO_CONTR_DATA) || o.status.HasFlag(DocStatus.NO_DOCUMENT_DATA)) && o.days < 4 && !o.status.HasFlag(DocStatus.EXCLUDED)));
        //            documents_to_remove[company.Value].AddRange(documents[company.Value].FindAll(o => o.found == false && !o.status.HasFlag(DocStatus.EXCLUDED)));

        //            if (!good_documents.ContainsKey(company.Value))
        //            {
        //                good_documents.Add(company.Value, new List<Document>());
        //            }
        //            good_documents[company.Value].AddRange(documents[company.Value].FindAll(o => !(o.status.HasFlag(DocStatus.NO_CONTR_DATA) || o.status.HasFlag(DocStatus.NO_DOCUMENT_DATA) || o.status.HasFlag(DocStatus.BUFOR))));

        //            corrected_obligations.AddRange(documents[company.Value].FindAll(o => !(o.status.HasFlag(DocStatus.NO_CONTR_DATA) || o.status.HasFlag(DocStatus.NO_DOCUMENT_DATA)) && o.corrected && !o.status.HasFlag(DocStatus.EXCLUDED)));


        //        }
        //        else if (res.status == 2)
        //        {
        //            return res;
        //        }
        //    }



        //    foreach (var document_to_remove in documents_to_remove)
        //    {
        //        foreach (var document in document_to_remove.Value)
        //        {
        //            DeleteDocument(document);
        //        }

        //    }
        //    cmd.Transaction.Commit();
        //    connection.Close();
        //    res = GetRepayments(companies, ref good_documents, ref repayments);

        //    foreach (var document in good_documents)
        //    {
        //        obligations_to_send.AddRange(document.Value.FindAll(o => !o.status.HasFlag(DocStatus.SENT) && !o.status.HasFlag(DocStatus.EXCLUDED) && !o.status.HasFlag(DocStatus.INCLUDED) && !o.status.HasFlag(DocStatus.BUFOR)));
        //        //bufor_obligations.AddRange(document.Value.FindAll(o => !o.status.HasFlag(DocStatus.SENT) && !o.status.HasFlag(DocStatus.EXCLUDED) && !o.status.HasFlag(DocStatus.INCLUDED) && o.status.HasFlag(DocStatus.BUFOR)));
        //        //obligations_to_send.AddRange(good_documents[document.Key]);
        //        if (repayments.ContainsKey(document.Key))
        //        {
        //            repayments_to_send.AddRange(repayments[document.Key]);
        //        }
        //    }

        //    foreach (var document in lack_documents)
        //    {
        //        lack_obligations.AddRange(document.Value.FindAll(o => (o.status.HasFlag(DocStatus.NO_CONTR_DATA) || o.status.HasFlag(DocStatus.NO_DOCUMENT_DATA) /*|| o.status.HasFlag(DocStatus.VETA_VALIDATION_ERROR)*/) && (!o.status.HasFlag(DocStatus.EXCLUDED)) && (!o.status.HasFlag(DocStatus.BUFOR))));
        //    }

        //    DisconnectFromApi();

        //    return res;
        //}

        //public Result SaveDocument(Document document)
        //{
        //    Result res = new Result();
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    cmd.CommandText = "SELECT COUNT(external_id) FROM Documents WHERE external_id = " + document.external_id.ToString() + " AND company = " + document.company_id + " AND integration_type = " + Convert.ToInt32(document.integration_type).ToString();
        //    var count = cmd.ExecuteScalar();
        //    if (Convert.ToInt32(count) == 0)
        //    {
        //        cmd.CommandText = "INSERT INTO Documents(external_id, integration_type, company, company_name, status, lacks, gross_amount, creation_date, " +
        //                                                "last_modify, package_id, user_id, corrections_count, full_number, contractor_name, issue_date, due_date, contractor_id, bank_account, contractor_tax_number, net_amount, doc_currency, veta_error) " +
        //        "VALUES(@external_id, @integration_type, @company, @company_name, @status, @lacks, @gross_amount, @creation_date, " +
        //                                                "@last_modify, @package_id, @user_id, @corrections_count, @full_number, @contractor_name, @issue_date, @due_date, @contractor_id, @bank_account, @contractor_tax_number, @net_amount, @doc_currency, @veta_error)";

        //        cmd.Parameters.AddWithValue("?", document.external_id);
        //        cmd.Parameters.AddWithValue("?", Convert.ToInt32(document.integration_type));
        //        cmd.Parameters.AddWithValue("?", Convert.ToInt32(document.company_id));
        //        cmd.Parameters.AddWithValue("?", document.company_name);
        //        cmd.Parameters.AddWithValue("?", document.status);
        //        cmd.Parameters.AddWithValue("?", document.lacks);
        //        cmd.Parameters.AddWithValue("?", document.gross_amount.amount);
        //        cmd.Parameters.AddWithValue("?", document.creation_date);
        //        cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
        //        cmd.Parameters.AddWithValue("?", document.identifiers.response.ToString());
        //        cmd.Parameters.AddWithValue("?", document.identifiers.userId);
        //        cmd.Parameters.AddWithValue("?", document.corrections_count);
        //        cmd.Parameters.AddWithValue("?", document.full_number);
        //        cmd.Parameters.AddWithValue("?", document.contractor.name);
        //        cmd.Parameters.AddWithValue("?", document.issue_date);
        //        cmd.Parameters.AddWithValue("?", document.due_date);
        //        cmd.Parameters.AddWithValue("?", document.contractor_id);
        //        cmd.Parameters.AddWithValue("?", document.bank_account);

        //        cmd.Parameters.AddWithValue("?", document.contractor_tax_number);
        //        cmd.Parameters.AddWithValue("?", document.net_amount.amount);
        //        cmd.Parameters.AddWithValue("?", document.gross_amount.currency);
        //        cmd.Parameters.AddWithValue("?", document.veta_error);
        //        try
        //        {
        //            cmd.ExecuteNonQuery();
        //            cmd.Parameters.Clear();
        //            res.status = 1;
        //        }
        //        catch (Exception ex)
        //        {
        //            res.status = 0;
        //            res.msg = "Błąd dodawania dokumenty.";
        //            logger.LogMessage("Błąd w funkcji SaveDocument. " + ex.Message);
        //        }
        //    }
        //    else
        //    {
        //        res = UpdateDocument(cmd, document);
        //    }
        //    cmd.Transaction.Commit();
        //    connection.Close();

        //    return res;
        //}

        //public Result UpdateDocument(OleDbCommand cmd, Document document)
        //{
        //    Result ret = new Result();
        //    cmd.CommandText = "UPDATE Documents SET status = @status, lacks = @lacks, last_modify = @last_modify, " +
        //                      "gross_amount = @gross_amount, full_number = @full_number, contractor_name = @contractor_name, " +
        //                      "issue_date = @issue_date, due_date = @due_date, bank_account = @bank_account, contractor_tax_number = @contractor_tax_number, " +
        //                      "net_amount = @net_amount, doc_currency = @doc_currency, veta_error = @veta_error " +
        //                      "WHERE external_id = @external_id AND integration_type = @integration_type AND company = @company AND company_name = @company_name AND updated = 0";
        //    cmd.Parameters.AddWithValue("?", document.status);
        //    cmd.Parameters.AddWithValue("?", document.lacks);
        //    cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
        //    cmd.Parameters.AddWithValue("?", document.gross_amount.amount);
        //    cmd.Parameters.AddWithValue("?", document.full_number);
        //    cmd.Parameters.AddWithValue("?", document.contractor.name);

        //    cmd.Parameters.AddWithValue("?", document.issue_date);
        //    cmd.Parameters.AddWithValue("?", document.due_date);
        //    cmd.Parameters.AddWithValue("?", document.bank_account);

        //    cmd.Parameters.AddWithValue("?", document.contractor_tax_number);
        //    cmd.Parameters.AddWithValue("?", document.net_amount.amount);
        //    cmd.Parameters.AddWithValue("?", document.gross_amount.currency);
        //    cmd.Parameters.AddWithValue("?", document.veta_error);

        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(document.external_id));
        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(document.integration_type));
        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(document.company_id));
        //    cmd.Parameters.AddWithValue("?", document.company_name);

        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //        cmd.Parameters.Clear();
        //        ret.status = 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        ret.status = 0;
        //        ret.msg = "Błąd aktualizacji danych dokumentu.";
        //        logger.LogMessage("Błąd w funkcji UpdateDocument. " + ex.Message);

        //    }
        //    return ret;
        //}

        //public Result DeleteDocument(Document document)
        //{
        //    Result res = new Result();
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    cmd.CommandText = "DELETE FROM Documents WHERE external_id = " + document.external_id.ToString() + " AND company = " + document.company_id + " AND integration_type = " + Convert.ToInt32(document.integration_type).ToString() + " AND company_name = '" + document.company_name + "'";
        //    cmd.ExecuteNonQuery();

        //    cmd.Transaction.Commit();
        //    connection.Close();

        //    return res;
        //}

        //public Dictionary<Company, int> GetLatestDocument(Dictionary<int, Company> companies)
        //{
        //    Dictionary<Company, int> ret = new Dictionary<Company, int>(new Company.EqualityComparer());
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    ConnectToApi();

        //    foreach (var company in companies)
        //    {
        //        cmd.CommandText = "SELECT MAX(external_id) FROM Documents WHERE company = " + company.Value.id.ToString() + " AND integration_type = " + Convert.ToInt32(company.Value.integ_type).ToString() ;
        //        object max_external_id = cmd.ExecuteScalar();
        //        int max_id = 0;
        //        try
        //        {
        //            max_id = Convert.ToInt32(max_external_id);
        //            ret.Add(company.Value, max_id);
        //        }
        //        catch
        //        {
        //            DateTime date = Convert.ToDateTime(XMLConfig.GetLicenseAcceptation());
        //            Integrations[company.Key].GetLatestDocument(company.Value, ref max_id, date);
        //            ret.Add(company.Value, max_id);
        //        }
        //    }
        //    cmd.Transaction.Commit();
        //    connection.Close();
        //    DisconnectFromApi();
        //    return ret;
        //}

        //public Result GetRepayments(Dictionary<int, Company> companies, ref Dictionary<Company, List<Document>> documents, ref  Dictionary<Company, List<Repayment>> lrepayments)
        //{
        //    Result ret = new Result();
        //    foreach (var company in companies)
        //    {
        //        List<Document> ldocuments = documents[company.Value];
        //        var module = modules.Find(o => o.id == company.Key.ToString());
        //        Integrations[company.Key].LoginUser(module.login, module.pass, module.server, module.database, module.export_dir);
        //        ret = Integrations[company.Key].GetRepayments(company.Value, ref ldocuments, ref lrepayments);
        //        Integrations[company.Key].Logout();
        //    }
        //    return ret;
        //}

        //public Result UpdateDocumentStatus(Document document)
        //{
        //    Result ret = new Result();
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();

        //    cmd.CommandText = "UPDATE Documents SET status = @status, last_modify = @last_modify WHERE external_id = @external_id AND integration_type = @integration_type AND company = @company";
        //    cmd.Parameters.AddWithValue("?", document.status);
        //    cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
        //    cmd.Parameters.AddWithValue("?", document.external_id);
        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(document.integration_type));
        //    cmd.Parameters.AddWithValue("?", document.company_id);

        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //        cmd.Parameters.Clear();
        //        ret.status = 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        ret.status = 0;
        //        ret.msg = "Błąd aktualizacji danych dokumentu.";
        //        logger.LogMessage("Błąd w funkcji UpdateDocumentStatus. " + ex.Message);
        //    }

        //    cmd.Transaction.Commit();
        //    connection.Close();
        //    return ret;
        //}

        //public Result UpdateDocumentLacks(Document document)
        //{
        //    Result ret = new Result();
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();

        //    cmd.CommandText = "UPDATE Documents SET lacks = @lacks, last_modify = @last_modify WHERE external_id = @external_id AND integration_type = @integration_type AND company = @company";
        //    cmd.Parameters.AddWithValue("?", document.lacks);
        //    cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
        //    cmd.Parameters.AddWithValue("?", document.external_id);
        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(document.integration_type));
        //    cmd.Parameters.AddWithValue("?", document.company_id);

        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //        cmd.Parameters.Clear();
        //        ret.status = 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        ret.status = 0;
        //        ret.msg = "Błąd aktualizacji danych dokumentu.";
        //        logger.LogMessage("Błąd w funkcji UpdateDocumentLacks. " + ex.Message);
        //    }

        //    cmd.Transaction.Commit();
        //    connection.Close();
        //    return ret;
        //}

        //public Result UpdateDocumentExcludedStatus(Contractor contractor)
        //{
        //    Result ret = new Result();
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    string operation = "";
        //    string comparison = "";

        //    cmd.CommandText = "SELECT external_id, status FROM Documents WHERE contractor_id = @contractor_id AND integration_type = @integration_type AND company = @company AND company_name = @company_name";
        //    cmd.Parameters.AddWithValue("?", contractor.external_id);
        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(contractor.integration_type));
        //    cmd.Parameters.AddWithValue("?", contractor.company_id);
        //    cmd.Parameters.AddWithValue("?", contractor.company_name);

        //    try
        //    {


        //        var reader = cmd.ExecuteReader();
        //        List<string> queries = new List<string>();
        //        while (reader.Read())
        //        {
        //            DocStatus doc_status = (DocStatus)reader["status"];
        //            if (contractor.excluded)
        //            {
        //                doc_status &= ~DocStatus.INCLUDED;
        //                doc_status |= DocStatus.EXCLUDED;
        //            }
        //            else
        //            {
        //                doc_status &= ~DocStatus.EXCLUDED;
        //                doc_status |= DocStatus.INCLUDED;
        //            }

        //            queries.Add("UPDATE Documents SET status = " + Convert.ToInt32(doc_status).ToString() + " WHERE contractor_id = " + contractor.external_id.ToString() + " AND external_id = " + reader["external_id"].ToString() +
        //                        " AND integration_type = " + Convert.ToInt32(contractor.integration_type).ToString() + " AND company = " + contractor.company_id.ToString() + " AND company_name = '" + contractor.company_name.ToString() + "'");
        //        }
        //        reader.Close();
        //        foreach (var query in queries)
        //        {
        //            cmd.CommandText = query;
        //            cmd.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string msg = ex.Message;
        //    }

        //    /*reader.Read();
        //    if(contractor.excluded)
        //    {
        //        operation = "+";
        //        comparison = " < 1";
        //    }
        //    else
        //    {
        //        operation = "-";
        //        comparison = " > 0";
        //    }
        //    cmd.CommandText = "UPDATE Documents SET status = status " + operation + " 8192 , last_modify = @last_modify WHERE contractor_id = @contractor_id AND integration_type = @integration_type AND company = @company AND company_name = @company_name AND ((status\\(8192)) mod 2 " + comparison + ")";
        //    cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
        //    cmd.Parameters.AddWithValue("?", contractor.external_id);
        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(contractor.integration_type));
        //    cmd.Parameters.AddWithValue("?", contractor.company_id);
        //    cmd.Parameters.AddWithValue("?", contractor.company_name);

        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //        cmd.Parameters.Clear();
        //        ret.status = 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        ret.status = 0;
        //        ret.msg = "Błąd aktualizacji danych dokumentu.";
        //        logger.LogMessage("Błąd w funkcji UpdateDocumentStatus. " + ex.Message);
        //    }*/

        //    cmd.Transaction.Commit();
        //    connection.Close();
        //    return ret;
        //}

        //public Result UpdateCorrectionsCount(Document document)
        //{
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();

        //    Result ret = new Result();
        //    cmd.CommandText = "UPDATE Documents SET corrections_count = @corrections_count, last_modify = @last_modify WHERE external_id = @external_id AND integration_type = @integration_type AND company = @company";
        //    cmd.Parameters.AddWithValue("?", document.corrections_count);
        //    cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
        //    cmd.Parameters.AddWithValue("?", document.external_id);
        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(document.integration_type));
        //    cmd.Parameters.AddWithValue("?", document.company_id);

        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //        cmd.Parameters.Clear();
        //        ret.status = 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        ret.status = 0;
        //        ret.msg = "Błąd aktualizacji send_date.";
        //        logger.LogMessage("Błąd w funkcji UpdateSentDate. " + ex.Message);

        //    }
        //    cmd.Transaction.Commit();
        //    connection.Close();
        //    return ret;
        //}

        //public Result UpdatePaidAmount(Document document)
        //{
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();

        //    Result ret = new Result();
        //    cmd.CommandText = "UPDATE Documents SET paid_amount = @paid_amount WHERE external_id = @external_id AND integration_type = @integration_type AND company = @company";
        //    cmd.Parameters.AddWithValue("?", document.paid_amount.amount);
        //    cmd.Parameters.AddWithValue("?", document.external_id);
        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(document.integration_type));
        //    cmd.Parameters.AddWithValue("?", document.company_id);

        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //        cmd.Parameters.Clear();
        //        ret.status = 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        ret.status = 0;
        //        ret.msg = "Błąd aktualizacji case_closed.";
        //        logger.LogMessage("Błąd w funkcji CloseCase. " + ex.Message);

        //    }

        //    cmd.Transaction.Commit();
        //    connection.Close();
        //    return ret;
        //}

        //public Result CloseCase(Document document)
        //{
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();

        //    Result ret = new Result();
        //    cmd.CommandText = "UPDATE Documents SET case_closed = @case_closed WHERE external_id = @external_id AND integration_type = @integration_type AND company = @company";
        //    cmd.Parameters.AddWithValue("?", document.case_closed);
        //    cmd.Parameters.AddWithValue("?", document.external_id);
        //    cmd.Parameters.AddWithValue("?", Convert.ToInt32(document.integration_type));
        //    cmd.Parameters.AddWithValue("?", document.company_id);

        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //        cmd.Parameters.Clear();
        //        ret.status = 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        ret.status = 0;
        //        ret.msg = "Błąd aktualizacji case_closed.";
        //        logger.LogMessage("Błąd w funkcji CloseCase. " + ex.Message);

        //    }

        //    cmd.Transaction.Commit();
        //    connection.Close();
        //    return ret;
        //}

        //public int GetCountOfQualifiedDocuments(DateTime from, DateTime to)
        //{
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();

        //    Result ret = new Result();
        //    cmd.CommandText = "SELECT COUNT(external_id) FROM Documents WHERE ((status \\ (2^0)) mod 2  > 0 or (status \\ (2^1)) mod 2 > 0 or (status \\ (2^2)) mod 2 > 0 or (status \\ (2^9)) mod 2 > 0) and send_date >= @from and send_date < @to";
        //    cmd.Parameters.AddWithValue("?", from);
        //    cmd.Parameters.AddWithValue("?", to);
        //    int count = Convert.ToInt32(cmd.ExecuteScalar());
        //    cmd.Parameters.Clear();
        //    cmd.Transaction.Commit();
        //    connection.Close();

        //    return count;
        //}

        public int GetCountOfSentDocuments(DateTime from, DateTime to)
        {
            OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
            connection.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = connection;
            cmd.Transaction = connection.BeginTransaction();

            Result ret = new Result();
            cmd.CommandText = "SELECT COUNT(id) FROM SentDocuments WHERE send_date >= @from and send_date < @to";
            cmd.Parameters.AddWithValue("?", from);
            cmd.Parameters.AddWithValue("?", to);
            int count = Convert.ToInt32(cmd.ExecuteScalar());
            cmd.Parameters.Clear();
            cmd.Transaction.Commit();
            connection.Close();

            return count;
        }

        //public int GetCountOfSentDocumentsForIntegration(DateTime from, DateTime to)
        //{
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    GetModules();

        //    Result ret = new Result();
        //    int integ_type = (int)Enum.Parse(typeof(IntegType), modules[0].identstr);
        //    cmd.CommandText = "SELECT COUNT(external_id) FROM Documents WHERE integration_type = " + integ_type.ToString() + " and (status \\ (2^0)) mod 2  > 0  and send_date >= @from and send_date < @to";
        //    cmd.Parameters.AddWithValue("?", from);
        //    cmd.Parameters.AddWithValue("?", to);
        //    int count = Convert.ToInt32(cmd.ExecuteScalar());
        //    cmd.Parameters.Clear();
        //    cmd.Transaction.Commit();
        //    connection.Close();

        //    return count;
        //}

        //public int GetCountOfLackDocuments(DateTime from, DateTime to)
        //{
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();

        //    Result ret = new Result();
        //    cmd.CommandText = "SELECT COUNT(external_id) FROM Documents WHERE ((status \\ (2^1)) mod 2 > 0 or (status \\ (2^2)) mod 2 > 0 or (status \\ (2^9)) mod 2 > 0) and creation_date >= @from and creation_date < @to";
        //    cmd.Parameters.AddWithValue("?", from);
        //    cmd.Parameters.AddWithValue("?", to);
        //    int count = Convert.ToInt32(cmd.ExecuteScalar());
        //    cmd.Parameters.Clear();
        //    cmd.Transaction.Commit();
        //    connection.Close();

        //    return count;
        //}

        //public int GetCountOfLackDocumentsForIntegration(DateTime from, DateTime to)
        //{
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();

        //    Result ret = new Result();
        //    int integ_type = (int)Enum.Parse(typeof(IntegType), modules[0].identstr);
        //    cmd.CommandText = "SELECT COUNT(external_id) FROM Documents WHERE integration_type = " + integ_type.ToString() + " AND ((status \\ (2^1)) mod 2 > 0 or (status \\ (2^2)) mod 2 > 0 or (status \\ (2^9)) mod 2 > 0) and creation_date >= @from and creation_date < @to";
        //    cmd.Parameters.AddWithValue("?", from);
        //    cmd.Parameters.AddWithValue("?", to);
        //    int count = Convert.ToInt32(cmd.ExecuteScalar());
        //    cmd.Parameters.Clear();
        //    cmd.Transaction.Commit();
        //    connection.Close();

        //    return count;
        //}

        //public int GetCountOfBuforDocuments(DateTime from, DateTime to)
        //{
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();

        //    Result ret = new Result();
        //    cmd.CommandText = "SELECT COUNT(external_id) FROM Documents WHERE (status \\ (2^14)) mod 2 > 0 and creation_date >= @from and creation_date < @to";
        //    cmd.Parameters.AddWithValue("?", from);
        //    cmd.Parameters.AddWithValue("?", to);
        //    int count = Convert.ToInt32(cmd.ExecuteScalar());
        //    cmd.Parameters.Clear();
        //    cmd.Transaction.Commit();
        //    connection.Close();

        //    return count;
        //}

        //public List<Document> GetLackDocuments()
        //{
        //    List<Document> ret = new List<Document>();
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    int integ_type = (int)Enum.Parse(typeof(IntegType), modules[0].identstr);
        //    cmd.CommandText = "SELECT * FROM Documents WHERE integration_type = " + integ_type.ToString() + " and ((status \\ (2^1)) mod 2 > 0 or (status \\ (2^2)) mod 2 > 0 or (status \\ (2^9)) mod 2 > 0) and ((status\\(2^13)) mod 2 < 1)";

        //    try
        //    {
        //        var reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            Document doc = new Document();
        //            doc.external_id = Convert.ToInt32(reader["external_id"]);
        //            doc.integration_type = (IntegType)Convert.ToInt32(reader["integration_type"]);
        //            doc.company_id = Convert.ToInt32(reader["company"]);
        //            doc.company_name = reader["company_name"].ToString();
        //            doc.status = (DocStatus)Convert.ToInt32(reader["status"]);
        //            doc.lacks = reader["lacks"].ToString();
        //            doc.gross_amount.amount = Convert.ToDouble(reader["gross_amount"]);
        //            if (reader["paid_amount"] != DBNull.Value)
        //            {
        //                doc.paid_amount.amount = Convert.ToDouble(reader["paid_amount"]);
        //            }
        //            doc.identifiers.userId = reader["user_id"].ToString();
        //            doc.identifiers.response = new Guid(reader["package_id"].ToString());
        //            doc.case_closed = Convert.ToBoolean(reader["case_closed"]);
        //            ret.Add(doc);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.LogMessage(ex.Message);
        //    }
        //    cmd.Transaction.Commit();
        //    connection.Close();

        //    return ret;
        //}

        //public bool LoginUser()
        //{
        //    bool ret = true;
        //    GetModules();
        //    //ConnectToApi();
        //    foreach (var module in modules)
        //    {
        //        if (!Integrations[Convert.ToInt32(module.id)].LoginUser(module.login, module.pass, module.server, module.database, module.export_dir))
        //        {
        //            ret = false;
        //            break;
        //        }
        //    }
        //    DisconnectFromApi();
        //    return ret;
        //}

        //public void GetMissingValues()
        //{
        //    GetModules();
        //    ConnectToApi();
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();

        //    OleDbCommand cmd2 = new OleDbCommand();
        //    cmd2.Connection = connection;
        //    cmd2.Transaction = cmd.Transaction;

        //    foreach (var mod in modules)
        //    {
        //        //IntegType integ_type = (IntegType)Enum.Parse(typeof(IntegType), mod.identstr);
        //        cmd.CommandText = "SELECT COUNT(*), external_id FROM Documents WHERE integration_type = " + ((int)integ_type).ToString() + " " +
        //                          "AND company = " + mod.company_id + " AND company_name = '" + mod.company_name + "' AND (" +
        //                          "IsNull(full_number) OR IsNull(contractor_name) OR IsNull(issue_date) OR IsNull(contractor_id)) GROUP BY external_id";
        //        int document_id = 0;
        //        var reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            document_id = Convert.ToInt32(reader["external_id"]);
        //            var missings = Integrations[Convert.ToInt32(mod.id)].GetMissingValues(mod, document_id);
        //            if (!object.ReferenceEquals(missings, null))
        //            {
        //                cmd2.CommandText = "UPDATE Documents SET full_number = @full_number, contractor_name = @contractor_name, " +
        //                                   "issue_date = @issue_date, contractor_id = @contractor_id, bank_account = @bank_account, contractor_tax_number = @contractor_tax_number WHERE external_id = " + document_id.ToString() +
        //                                   " AND integration_type = " + ((int)integ_type).ToString() + " " +
        //                                   "AND company = " + mod.company_id + " AND company_name = '" + mod.company_name + "'";

        //                cmd2.Parameters.AddWithValue("?", missings.full_number);
        //                cmd2.Parameters.AddWithValue("?", missings.contractor_name);
        //                cmd2.Parameters.AddWithValue("?", missings.issue_date);
        //                cmd2.Parameters.AddWithValue("?", missings.contractor_id);
        //                cmd2.Parameters.AddWithValue("?", missings.bank_account);
        //                cmd2.Parameters.AddWithValue("?", missings.contractor_tax_number);

        //                cmd2.ExecuteNonQuery();
        //            }

        //        }
        //        reader.Close();
        //    }
        //    cmd.Transaction.Commit();
        //    connection.Close();
        //    DisconnectFromApi();
        //}

        //void DeleteBuforDocument(string identstr, List<Document> documents, OleDbCommand cmd)
        //{
        //    int integtype = (int)(Enum.Parse(typeof(IntegType), identstr));
        //    foreach (var document in documents)
        //    {
        //        cmd.CommandText = "DELETE FROM Documents WHERE external_id = " + document.external_id + " AND integration_type = " + integtype.ToString() + " AND company = " + document.company_id.ToString();
        //        try
        //        {
        //            cmd.ExecuteNonQuery();
        //        }
        //        catch (Exception ex)
        //        {

        //        }
        //    }
        //}
        //public Result GetLastSendTime(ref DateTime datetime)
        //{
        //    Result ret = new Result();
        //    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + databasepath + "; Jet OLEDB:Engine Type=5");
        //    connection.Open();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = connection;
        //    cmd.Transaction = connection.BeginTransaction();
        //    cmd.CommandText = "SELECT max(creation_date) FROM Documents";

        //    try
        //    {
        //        datetime = Convert.ToDateTime(cmd.ExecuteScalar());
        //        ret.status = 1;
        //        ret.msg = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.LogMessage("Błąd pobierania ostaniego czasu synchronizacji. " + ex.Message);
        //        ret.status = 0;
        //        ret.msg = "Błąd pobierania ostaniego czasu synchronizacji";
        //    }
        //    cmd.Transaction.Commit();
        //    connection.Close();

        //    return ret;
        //}
        //public bool CheckSynchroTime()
        //{
        //    bool ret = false;
        //    //GetModules();
        //    foreach (var module in modules)
        //    {
        //        string[] time_tab = module.synchro_time.Split(':');
        //        if (Convert.ToInt32(time_tab[0]) == DateTime.Now.Hour && Convert.ToInt32(time_tab[1]) == DateTime.Now.Minute && DateTime.Now.Second == 0)
        //        {
        //            ret = true;
        //            break;
        //        }
        //        time_tab = null;
        //    }
        //    return ret;
        //}
    }
}
