﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Reflection;
using System.Net;
using System.IO;
using System.Data.OleDb;
using System.Text.RegularExpressions;

namespace STP_Lite
{
    static class Utility
    {
        public static Result CreateUserMSSQL(string server, string admin_login, string admin_pass, string system)
        {
            Result res = new Result();
            SqlConnectionStringBuilder cs = new SqlConnectionStringBuilder();
            cs.DataSource = server;
            cs.UserID = admin_login;
            cs.Password = admin_pass;
            SqlConnection connection = new SqlConnection(cs.ToString());


            string loginexist = @"SELECT COUNT(loginname) from master.dbo.syslogins WHERE loginname = @Ploginname";

            string s = "EXEC sp_addlogin  N'FairPay', N'tM3xcc(#c'";

            string s1 = "EXEC sp_adduser 'FairPay', 'FairPay'";

            string s2 = "EXEC sp_addsrvrolemember @loginame = N'FairPay', @rolename = N'sysadmin'";

            string s3 = "EXEC sp_addrolemember N'db_owner', N'FairPay'";

            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                connection.Close();
                res.status = 4;
                res.msg = "Błąd połączenia z serwerem bazy danych. Connection string = \"" + cs.ToString() + "\".\n" + ex.Message;
                return res;
            }

            SqlCommand command = new SqlCommand(loginexist, connection);
            command.Parameters.Add("@Ploginname", System.Data.SqlDbType.VarChar).Value = "FairPay";

            int login_count = 0;
            try
            {
                login_count = Convert.ToInt32(command.ExecuteScalar());
                command.Parameters.Clear();
            }
            catch (Exception ex)
            {
                res.status = 2;
                res.msg = "Błąd wyszukiwania użytkownika FairPay. " + ex.Message;
                return res;
            }

            if (login_count == 0)
            {
                command.CommandText = s;
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    res.status = 0;
                    res.msg = "Błąd tworzenia loginu " + s + "." + ex.Message;
                    connection.Close();
                    return res;
                }
                command.Parameters.Clear();
                command.CommandText = s1;
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    res.status = 0;
                    res.msg = "Błąd tworzenia loginu " + s1 + "." + ex.Message;
                    connection.Close();
                    return res;
                }

                command.Parameters.Clear();
                command.CommandText = s2;
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    res.status = 0;
                    res.msg = "Błąd tworzenia loginu " + s2 + "." + ex.Message;
                    connection.Close();
                    return res;
                }

                command.Parameters.Clear();
                command.CommandText = s3;
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    res.status = 0;
                    res.msg = "Błąd tworzenia loginu " + s3 + "." + ex.Message;
                    connection.Close();
                    return res;
                }

                res.status = 1;
                res.msg = "Zintegrowano z systemem " + system + ".";
            }
            else
            {
                res.status = 3;
            }


            connection.Close();
            connection.Dispose();

            return res;
        }

        public static Version GetActualVersion()
        {
            try
            {
                return System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion;
            }
            catch
            {
                return Assembly.GetExecutingAssembly().GetName().Version;
            }
        }

        public static Result GetPublicIP()
        {

            Result res = new Result();
            try
            {
                WebRequest request = WebRequest.Create("http://ipv4.icanhazip.com/");
                request.Timeout = 2000;
                using (WebResponse response = request.GetResponse())
                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    res.msg = stream.ReadToEnd();
                }

                res.msg = res.msg.Replace("\n", "");
                res.status = 1;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return res;
        }

        public static void UpdateLoginNb()
        {
            string database = @"C:\Users\" + Environment.UserName + @"\Documents\FPEASY\database.mdb";
            OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + database + "; Jet OLEDB:Engine Type=5");
            connection.Open();

            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = connection;
            cmd.Transaction = connection.BeginTransaction();
            cmd.CommandText = "SELECT dictionary_value FROM WeeklyStatistics WHERE dictionary_key = 'nb_of_loggins' AND week_id = (SELECT MAX(week_id) FROM WeeklyStatistics) AND sent = 0";
            string value = cmd.ExecuteScalar().ToString();
            if (!string.IsNullOrEmpty(value))
            {
                int nb_value = Convert.ToInt32(value);
                nb_value++;
                cmd.CommandText = "UPDATE WeeklyStatistics SET dictionary_value = '" + nb_value.ToString() + "' WHERE dictionary_key = 'nb_of_loggins' AND week_id = (SELECT MAX(week_id) FROM WeeklyStatistics) AND sent = 0";
                cmd.ExecuteNonQuery();
                cmd.Transaction.Commit();
            }
        }

        public static void GetContractorNameAndSurname(string fullname, ref string name, ref string surname)
        {
            fullname = fullname.Trim();
            if (fullname.IndexOf("spółka", StringComparison.OrdinalIgnoreCase) < 0 && fullname.IndexOf("s.", StringComparison.OrdinalIgnoreCase) < 0 && fullname.IndexOf("sp.", StringComparison.OrdinalIgnoreCase) < 0)
            {
                string[] name_surname_tab = fullname.Split(' ');
                if (name_surname_tab.Length >= 2)
                {
                    name = name_surname_tab[name_surname_tab.Length - 2];
                    surname = name_surname_tab.Last();
                }
            }
        }

        public static void GetContractorAddress(string fulladdress, ref string street, ref string building, ref string flat)
        {
            string pattern_clean = @"((ulica)|(aleja)|(plac)|([uap]l\.))";
            string address_clean = Regex.Replace(fulladdress, pattern_clean, "", RegexOptions.IgnoreCase).ToString();
            string pattern_suffix = @"[0-9]{1,3}[A-Z]? ?(((\/ ?)|(m\. ?))[0-9]{1,3})?";
            string address_suffix = Regex.Match(address_clean, pattern_suffix, RegexOptions.IgnoreCase).ToString();
            string address_rest = Regex.Replace(address_clean, pattern_suffix, "", RegexOptions.IgnoreCase).ToString();
            street = address_rest.Trim();

            string pattern_suffix_1st = @"^[0-9]{1,3}[A-Z]?";
            string address_suffix_1st = Regex.Match(address_suffix, pattern_suffix_1st, RegexOptions.IgnoreCase).ToString();
            building = address_suffix_1st.Trim();

            string pattern_suffix_2nd = @"\d+";
            string address_suffix_rest = Regex.Replace(address_suffix, pattern_suffix_1st, "", RegexOptions.IgnoreCase).ToString();
            string address_suffix_2nd = Regex.Match(address_suffix_rest, pattern_suffix_2nd, RegexOptions.IgnoreCase).ToString();
            flat = address_suffix_2nd.Trim();
        }

        public static int DataConversionClarion(DateTime date, ref int result)
        {
            //TO int
            DateTime data = new DateTime(1800, 12, 28, 0, 0, 0);
            result = (date - data).Days;
            return 0;
        }

        public static DateTime Clarion2Date(int dateClarion)
        {
            DateTime date = new DateTime(1800, 12, 28, 0, 0, 0);
            date.AddDays(dateClarion);
            return date.AddDays(dateClarion);
        }

        public static string ClearNip(string nip)
        {
            string cleanNIP = "";
            if (!object.ReferenceEquals(nip, null))
            {
                for (int i = 0; i < nip.Length; i++)
                {
                    if (char.IsDigit(nip[i]))
                    {
                        cleanNIP = nip.Substring(i);
                        break;
                    }
                }
            }

            cleanNIP = cleanNIP.Replace("-", "");
            cleanNIP = cleanNIP.Replace(".", "");
            cleanNIP = cleanNIP.Replace(" ", "");
            cleanNIP = cleanNIP.Trim();
            return cleanNIP;
        }

        public static string RemoveIncorrectCharactersFromNIP(string nip)
        {
            string output = "";

            if (!string.IsNullOrEmpty(nip))
            {
                if (nip.Contains("PL") || nip.Contains("P") || nip.Contains("L"))
                {
                    output += "PL";
                }

                for (int i = 0; i < nip.Length; i++)
                {
                    if (char.IsDigit(nip[i]) || nip[i] == ' ' || nip[i] == '-')
                    {
                        output += nip[i];
                    }
                }
            }

            return output;
        }

        static public bool NIPValidate(string NIPValidate)
        {

            const byte lenght = 10;
            ulong nip = ulong.MinValue;
            byte[] digits;
            byte[] weights = new byte[] { 6, 5, 7, 2, 3, 4, 5, 6, 7 };
            if (NIPValidate.Length.Equals(lenght).Equals(false)) return false;
            if (ulong.TryParse(NIPValidate, out nip).Equals(false)) return false;
            else
            {
                string sNIP = NIPValidate.ToString();
                digits = new byte[lenght];
                for (int i = 0; i < lenght; i++)
                {
                    if (byte.TryParse(sNIP[i].ToString(), out digits[i]).Equals(false)) return false;
                }

                int checksum = 0;
                for (int i = 0; i < lenght - 1; i++)
                {
                    checksum += digits[i] * weights[i];
                }

                return (checksum % 11 % 10).Equals(digits[digits.Length - 1]);
            }
        }

        static public bool FileInUse(string path)
        {
            try
            {
                bool cw = false;
                using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    cw = fs.CanWrite;
                }
                return false;
            }
            catch
            {
                return true;
            }
            
        }
        static public string RemoveNoCorrectChars(string input, out bool badChar)
        {
            badChar = false;
            string output = "";
            for (int i = 0; i < input.Length; i++)
            {
                if (char.IsDigit(input[i]) || input[i] == ',' || input[i] == '.')
                {
                    output += input[i];
                    
                }
                else
                {
                    badChar = true;
                }
            }
                return output;        
        }

        static public string RemoveNoCorrectCharsBankAccount(string input)
        {
            string output = "";
            if (input.Contains("PL"))
            {
                output = "PL";
            }
            for (int i = 0; i < input.Length; i++)
            {
                if (char.IsDigit(input[i]) || input[i] == ' ')
                {
                    output += input[i];
                }
            }
            return output;
        }

        static public int GetCountDigitsInBankAccount(string input)
        {
            int output = 0;
           
            for (int i = 0; i < input.Length; i++)
            {
                if (char.IsDigit(input[i]))
                {
                    output++;
                }
            }
            return output;
        }

        static public string ClearPhoneNumber(string input)
        {
            string output = "";
            for (int i = 0; i < input.Length; i++)
            {
                if (char.IsDigit(input[i]) || input[i] == ' ' || input[i] == '-' || input[i] == '+' || input[i] == '(' || input[i] == ')')
                {
                    output += input[i];
                }
            }
            return output;
        }
    }
}
