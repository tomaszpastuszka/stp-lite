﻿using STP_Lite.XML;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace STP_Lite
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public const int HWND_BROADCAST = 0xFFFF;
        [DllImport("user32")]
        public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);

        [DllImport("user32")]
        public static extern int RegisterWindowMessage(string message);

        public static readonly int WM_ACTIVATE_FPEASY = RegisterWindowMessage("WM_ACTIVATE_FPEASY");
        public static readonly int WM_UPDATE_FPEASY = RegisterWindowMessage("WM_UPDATE_FPEASY");

        [STAThread]
        public static void Main()
        {
            bool createdNew = true;
            using (Mutex mutex = new Mutex(true, "{6407BF86-C7AE-4C8C-9AE9-FD7FC6CD8608}", out createdNew))
            {
                if (createdNew)
                {
                    var application = new App();
                    application.InitializeComponent();
                    application.Run();
                }
                else
                {
                    Process currentProcess = Process.GetCurrentProcess();
                    string app_name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                    foreach (Process process in Process.GetProcessesByName(app_name))
                    {
                        if (process.Id != currentProcess.Id)
                        {
                            IntPtr handle = process.MainWindowHandle;
                            bool result = false;

                            string actual_version = Utility.GetActualVersion().ToString();

                            string stored_version = XMLConfig.GetAppVersion();
                            if (string.IsNullOrEmpty(stored_version))
                            {
                                stored_version = "0.0.0.0";
                                XMLConfig.InsertMissingElement(XMLConfig.fileName, "app_version", stored_version);
                                XMLConfig.SetAppVersion(stored_version);
                            }

                            string[] tab_stored_version = stored_version.Split('.');
                            string[] tab_actual_version = actual_version.Split('.');
                            if (Convert.ToInt32(tab_stored_version[0]) < Convert.ToInt32(tab_actual_version[0]) ||
                                Convert.ToInt32(tab_stored_version[1]) < Convert.ToInt32(tab_actual_version[1]) ||
                                Convert.ToInt32(tab_stored_version[2]) < Convert.ToInt32(tab_actual_version[2]) ||
                                Convert.ToInt32(tab_stored_version[3]) < Convert.ToInt32(tab_actual_version[3]))
                            {
                                result = PostMessage((IntPtr)HWND_BROADCAST, WM_UPDATE_FPEASY, IntPtr.Zero, IntPtr.Zero);
                            }
                            else
                            {
                                result = PostMessage((IntPtr)HWND_BROADCAST, WM_ACTIVATE_FPEASY, IntPtr.Zero, IntPtr.Zero);
                            }

                        }
                    }
                }
            }
        }
    }
}
