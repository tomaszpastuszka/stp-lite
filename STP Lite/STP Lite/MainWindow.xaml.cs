﻿using STP_Lite.KRD;
using STP_Lite.pl.krd.demo.lucy;
using STP_Lite.XML;
using System;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Navigation;
using System.Reflection;
using Microsoft.Win32;
using System.Drawing;
using System.Collections.Generic;
using STP_Lite.Structures;
using System.ComponentModel;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Net.NetworkInformation;
using System.Threading;
using System.Windows.Media;
using System.Windows.Input;
using Api;
using STP_Lite.Pages;
using System.Windows.Interop;

namespace STP_Lite
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string apiLogerPathDir = @"C:\Users\" + Environment.UserName + @"\Documents\FPEASY\Api";
        public Comm comm = new Comm();
        static public Dictionary<string, string> CountryCodes = new Dictionary<string, string>();
        public List<Contractor> contractors = new List<Contractor>();
        public Contractor foundContrator = new Contractor();
        public List<Document> documents = new List<Document>();
        public Logger logger = new Logger();
        public Comm communication = new Comm();
        KRDApi krdapi = null;
        FPApi fpapi = null;
        public bool loggedIn = false;
        public int License_Ver = 3;
        public bool AcceptRegulation = false;
        public GetRegulationsResponse Regulation;
        public System.Windows.Forms.NotifyIcon notifyIcon = null;
        double widthScreen = 0.0;
        double heightScreen = 0.0;
        DoubleAnimation windowAnimationLeft = null;
        DoubleAnimation windowAnimationRight = null;
        BackgroundWorker bwgSendObligation = null;
        BackgroundWorker bwgPing = null;
        
        public bool InternetConnection = true;
        public string NIPFromMainPage = "";
        public bool correctNIPMainPage = false;
        public double _oldFrameWidth;
        public bool big_window = false;
        //Forms.WaitForm wF = null;
        private bool PingResult = false;
        public Pages.DocumentPage documentPageHandler = null;
        public Pages.MainPage mainPageHandler = null;
        public bool sending = false;
        System.Windows.Threading.DispatcherTimer dispatcherTimer = null;
        System.Windows.Threading.DispatcherTimer tUninstall = null;
        
        
        

        public MainWindow()
        {     
            InitializeComponent();
            logger.LogInfo("<------------------------------ START APLIKACJI ------------------------------>");

            widthScreen = System.Windows.SystemParameters.PrimaryScreenWidth;
            heightScreen = System.Windows.SystemParameters.PrimaryScreenHeight;

            SetWindowDefaultPosition();

            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 5);
            dispatcherTimer.Start();

            tUninstall = new System.Windows.Threading.DispatcherTimer();
            tUninstall.Tick += tUninstall_Tick;
            tUninstall.Interval = new TimeSpan(0, 0, 0, 0, 100);
            tUninstall.Start();



            XMLConfig.CreateDataBase();
            Version version = Utility.GetActualVersion();
            XMLConfig.CreateDoc(License_Ver, version);

            krdapi = new KRDApi(Convert.ToInt32(XMLConfig.GetEnvironment()));

            if (Convert.ToInt32(XMLConfig.GetEnvironment()) == 1)
            {
                fpapi = new FPApi("https://demoapi.kaczmarskigroup.pl/", apiLogerPathDir, XMLConfig.GetAppVersion(), Properties.Settings.Default.client_id_demo, Properties.Settings.Default.client_secret_demo, StaticFunctions.Functions.Decrypt(XMLConfig.GetLogin()), StaticFunctions.Functions.Decrypt(XMLConfig.GetPassword()));
            }
            else
            {
                fpapi = new FPApi("https://api.kaczmarskigroup.pl/", apiLogerPathDir, XMLConfig.GetAppVersion(), Properties.Settings.Default.client_id, Properties.Settings.Default.client_secret, StaticFunctions.Functions.Decrypt(XMLConfig.GetLogin()), StaticFunctions.Functions.Decrypt(XMLConfig.GetPassword()));
                //to samo tylko że dla prod
            }



            AddToAutostart();
            FillCountryCodes();
            logger.LoggerStart();

            bwgSendObligation = new BackgroundWorker();
            bwgSendObligation.DoWork += bwgSendObligation_DoWork;
            bwgSendObligation.RunWorkerCompleted += bwgSendObligation_Completed;

            bwgPing = new BackgroundWorker();
            bwgPing.DoWork += bwgPing_DoWork;
            bwgPing.RunWorkerCompleted += bwgPing_Completed;

            int week_id = 0;
            var json = comm.PrepareWeeklyReport(ref week_id);
            if (json.Count > 0)
            {
                krdapi.SendWeeklyOperation(XMLConfig.GetAppID(), "TransferReport", json);
                XMLConfig.SetLoginTime("0,0");
                communication.GenerateWeeklyStatistics();
                communication.UpdateWeeklyStatisticsSentDate(week_id);
            }

            NavigationCommands.BrowseBack.InputGestures.Clear();
            NavigationCommands.BrowseForward.InputGestures.Clear();               
        }

        private void mwindow_Loaded(object sender, RoutedEventArgs e)
        {
            string login = "";
            string password = "";
            if (XMLConfig.GetFirstRun() != "1")
            {

                krdapi.LastLogin = DateTime.Now;
                login = StaticFunctions.Functions.Decrypt(XMLConfig.GetLogin());
                password = StaticFunctions.Functions.Decrypt(XMLConfig.GetPassword());
                try
                {
                    InternetConnection = Ping();
                    logger.LogInfo("Sprawdzanie połączenia internetowego.");
                    while (!InternetConnection)
                    {
                        InternetConnection = Ping();
                        Thread.Sleep(1500);
                    }
                    logger.LogInfo("Próba logowania uzytkownika: " + login);
                    //var regulation = krdapi.LoginToKrdService(login, password);

                    //if ((regulation.RegulationAcceptedSpecified && regulation.RegulationAccepted) && (regulation.CanManageObligationsSpecified && regulation.CanManageObligations) && (regulation.HasContractOriginalSpecified && regulation.HasContractOriginal))
                    if(fpapi.Authorize(login, password))
                    {
                        var regulations = fpapi.GetRegulations();
                        if (regulations != null && regulations.regulationsId != XMLConfig.GetRegulationsId())
                        //if (!(regulation.RegulationAcceptedSpecified && regulation.RegulationAccepted) && (regulation.CanManageObligationsSpecified && regulation.CanManageObligations) && (regulation.HasContractOriginalSpecified && regulation.HasContractOriginalSpecified))
                        {
                            XMLConfig.SetOWUAcceptation("0");
                            AcceptRegulation = false;
                            mwindow.logger.LogInfo("Wysyłanie komunikatu: UserLogin");
                            krdapi.SendOperation(XMLConfig.GetAppID(), "UserLogin");
                            krdapi.LastLogin = DateTime.Now;
                            mwindow.SetLoggedIn(true);
                            
                            mwindow.logger.LogInfo("Ładowanie okna akceptacji OWU");
                            Pages.OWUPage owuPage = new OWUPage(krdapi, fpapi, mwindow);
                            mwindow.mainFrame.NavigationService.Navigate(owuPage);
                        }
                        else
                        {
                            SetLoggedIn(true);
                            logger.LogInfo("Poprawnie zalogowano użytkownika, login: " + login + " hasło: **********");
                        }

                    }
                    else
                    {

                        SetLoggedIn(false);
                        logger.LogInfo("Błąd logowania");
                        logger.LogMessage("Błąd logowania użytkownika, login: " + login + " hasło: **********");
                        if(XMLConfig.GetOWUAcceptation())
                        {
                            Pages.LoginPage loginPage = new LoginPage(krdapi, fpapi, this);
                            mainFrame.NavigationService.Navigate(loginPage);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.LogInfo("MainWindow: Błąd w trakcie logowania, login: " + login + " hasło: **********" + "\n\n" + ex.Message);
                }

            }
            else
            {
                logger.LogInfo("Wysyłanie informacji o pobraniu i zainstalowaniu aplikacji");
                GenerateAppId();
                krdapi.SendOperation(XMLConfig.GetAppID(), "Download");
                krdapi.SendOperation(XMLConfig.GetAppID(), "InstallationFinished");

            }


            string license_acceptation = XMLConfig.GetLicenseAcceptation();
            string license_version = XMLConfig.GetLicenseVersion();

            logger.LogInfo("Sprawdzanie daty akceptacji licencji");
            if (!string.IsNullOrEmpty(license_acceptation))
            {
                if (loggedIn && XMLConfig.GetOWUAcceptation())
                {
                    logger.LogInfo("Ładowanie głownego okna programu.");
                    Pages.MainPage mainPage = new Pages.MainPage(krdapi, fpapi, this);
                    mainPageHandler = mainPage;
                    mainFrame.NavigationService.Navigate(mainPage);

                }
                else if(!loggedIn && !XMLConfig.GetOWUAcceptation())
                {
                    logger.LogInfo("Ładowanie okna logowania.");
                    Pages.LoginPage loginPage = new Pages.LoginPage(krdapi, fpapi, this);
                    mainFrame.NavigationService.Navigate(loginPage);
                }
                else if (loggedIn && !XMLConfig.GetOWUAcceptation())
                {
                    logger.LogInfo("Ładowanie okna owu.");
                    Pages.OWUPage owuPage = new Pages.OWUPage(krdapi, fpapi, this);
                    mainFrame.NavigationService.Navigate(owuPage);
                }
            }
            else
            {
                logger.LogInfo("Ładowanie okna licencji.");
                Pages.LicensePage LicensePage = new Pages.LicensePage(krdapi, fpapi, this);
                mainFrame.NavigationService.Navigate(LicensePage);


            }

            notifyIcon = new System.Windows.Forms.NotifyIcon();
            notifyIcon.Click += new EventHandler(notifyIcon_Click);
            notifyIcon.DoubleClick += new EventHandler(notifyIcon_Click);
            notifyIcon.Icon = STP_Lite.Properties.Resources._72x72;
            notifyIcon.Visible = true;

            dockFrame.Navigated += dockFrame_Navigated;
            Title();
        }  

        void dockFrame_Navigated(object sender, NavigationEventArgs e)
        {            
           AnimateWindowWidthLeft();           
        }

        public bool GetUninstallString()
        {
            
            string displayname = "";
            string publisher = "";

            RegistryKey uninstallKey = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall");
                string[] appKeyNames = uninstallKey.GetSubKeyNames();
            string app_name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            bool found = false;
            foreach (string appKeyName in appKeyNames)
            {
                RegistryKey appKey = uninstallKey.OpenSubKey(appKeyName);
                displayname = (string)appKey.GetValue("DisplayName");
                publisher = (string)appKey.GetValue("Publisher");
                appKey.Close();
                if (displayname != null && publisher != null)
                {
                    if (displayname.Contains(app_name) && publisher == "Kaczmarski Group sp. z o.o.")
                    {
                        found = true;
                        break;
                    }
                }
            }

            uninstallKey.Close();
            return found;
        }
        private void tUninstall_Tick(object sender, EventArgs e)
        {

            if (!GetUninstallString())
            {
                Hide();
                logger.LogInfo("Uninstal - Rozpoczęcie odinstalowania");
                tUninstall.Stop();
                RemoveFromAutostart();
                System.Windows.MessageBox.Show("FP Easy został odinstalowany");
                logger.LogInfo("Uninstal - Odinstalowano");
                this.Close();

            }

        }

        public void FillCountryCodes()
        {
            CountryCodes.Add("Afganistan", "AF");
            CountryCodes.Add("Albania", "AL");
            CountryCodes.Add("Algieria", "DZ");
            CountryCodes.Add("Andora", "AD");
            CountryCodes.Add("Angola", "AO");
            CountryCodes.Add("Anguilla", "AI");
            CountryCodes.Add("Antarktyka", "AQ");
            CountryCodes.Add("Antigua i Barbuda", "AG");
            CountryCodes.Add("Arabia Saudyjska", "SA");
            CountryCodes.Add("Argentyna", "AR");
            CountryCodes.Add("Armenia", "AM");
            CountryCodes.Add("Aruba", "AW");
            CountryCodes.Add("Australia", "AU");
            CountryCodes.Add("Austria", "AT");
            CountryCodes.Add("Azerbejdżan", "AZ");
            CountryCodes.Add("Bahamy", "BS");
            CountryCodes.Add("Bahrajn", "BH");
            CountryCodes.Add("Bangladesz", "BD");
            CountryCodes.Add("Barbados", "BB");
            CountryCodes.Add("Belgia", "BE");
            CountryCodes.Add("Belize", "BZ");
            CountryCodes.Add("Benin", "BJ");
            CountryCodes.Add("Bermudy", "BM");
            CountryCodes.Add("Bhutan", "BT");
            CountryCodes.Add("Białoruś", "BY");
            CountryCodes.Add("Boliwia", "BO");
            CountryCodes.Add("Bonaire, Sint Eustatius i Saba", "BQ");
            CountryCodes.Add("Bośnia i Hercegowina", "BA");
            CountryCodes.Add("Botswana", "BW");
            CountryCodes.Add("Brazylia", "BR");
            CountryCodes.Add("Brunei", "BN");
            CountryCodes.Add("Brytyjskie Terytorium Oceanu Indyjskiego", "IO");
            CountryCodes.Add("Brytyjskie Wyspy Dziewicze", "VG");
            CountryCodes.Add("Bułgaria", "BG");
            CountryCodes.Add("Burkina Faso", "BF");
            CountryCodes.Add("Burundi", "BI");
            CountryCodes.Add("Chile", "CL");
            CountryCodes.Add("Chiny", "CN");
            CountryCodes.Add("Chorwacja", "HR");
            CountryCodes.Add("Curaçao", "CW");
            CountryCodes.Add("Cypr", "CY");
            CountryCodes.Add("Czad", "TD");
            CountryCodes.Add("Czarnogóra", "ME");
            CountryCodes.Add("Czechy", "CZ");
            CountryCodes.Add("Dalekie Wyspy Mniejsze Stanów Zjednoczonych", "UM");
            CountryCodes.Add("Dania", "DK");
            CountryCodes.Add("Demokratyczna Republika Konga", "CD");
            CountryCodes.Add("Dominika", "DM");
            CountryCodes.Add("Dominikana", "DO");
            CountryCodes.Add("Dżibuti", "DJ");
            CountryCodes.Add("Egipt", "EG");
            CountryCodes.Add("Ekwador", "EC");
            CountryCodes.Add("Erytrea", "ER");
            CountryCodes.Add("Estonia", "EE");
            CountryCodes.Add("Etiopia", "ET");
            CountryCodes.Add("Falklandy", "FK");
            CountryCodes.Add("Fidżi", "FJ");
            CountryCodes.Add("Filipiny", "PH");
            CountryCodes.Add("Finlandia", "FI");
            CountryCodes.Add("Francja", "FR");
            CountryCodes.Add("Francuskie Terytoria Południowe i Antarktyczne", "TF");
            CountryCodes.Add("Gabon", "GA");
            CountryCodes.Add("Gambia", "GM");
            CountryCodes.Add("Georgia Południowa i Sandwich Południowy", "GS");
            CountryCodes.Add("Ghana", "GH");
            CountryCodes.Add("Gibraltar", "GI");
            CountryCodes.Add("Grecja", "GR");
            CountryCodes.Add("Grenada", "GD");
            CountryCodes.Add("Grenlandia", "GL");
            CountryCodes.Add("Gruzja", "GE");
            CountryCodes.Add("Guam", "GU");
            CountryCodes.Add("Guernsey", "GG");
            CountryCodes.Add("Gujana Francuska", "GF");
            CountryCodes.Add("Gujana", "GY");
            CountryCodes.Add("Gwadelupa", "GP");
            CountryCodes.Add("Gwatemala", "GT");
            CountryCodes.Add("Gwinea Bissau", "GW");
            CountryCodes.Add("Gwinea Równikowa", "GQ");
            CountryCodes.Add("Gwinea", "GN");
            CountryCodes.Add("Haiti", "HT");
            CountryCodes.Add("Hiszpania", "ES");
            CountryCodes.Add("Holandia", "NL");
            CountryCodes.Add("Honduras", "HN");
            CountryCodes.Add("Hongkong", "HK");
            CountryCodes.Add("Indie", "IN");
            CountryCodes.Add("Indonezja", "ID");
            CountryCodes.Add("Irak", "IQ");
            CountryCodes.Add("Iran", "IR");
            CountryCodes.Add("Irlandia", "IE");
            CountryCodes.Add("Islandia", "IS");
            CountryCodes.Add("Izrael", "IL");
            CountryCodes.Add("Jamajka", "JM");
            CountryCodes.Add("Japonia", "JP");
            CountryCodes.Add("Jemen", "YE");
            CountryCodes.Add("Jersey", "JE");
            CountryCodes.Add("Jordania", "JO");
            CountryCodes.Add("Kajmany", "KY");
            CountryCodes.Add("Kambodża", "KH");
            CountryCodes.Add("Kamerun", "CM");
            CountryCodes.Add("Kanada", "CA");
            CountryCodes.Add("Katar", "QA");
            CountryCodes.Add("Kazachstan", "KZ");
            CountryCodes.Add("Kenia", "KE");
            CountryCodes.Add("Kirgistan", "KG");
            CountryCodes.Add("Kiribati", "KI");
            CountryCodes.Add("Kolumbia", "CO");
            CountryCodes.Add("Komory", "KM");
            CountryCodes.Add("Kongo", "CG");
            CountryCodes.Add("Korea Południowa", "KR");
            CountryCodes.Add("Korea Północna", "KP");
            CountryCodes.Add("Kostaryka", "CR");
            CountryCodes.Add("Kuba", "CU");
            CountryCodes.Add("Kuwejt", "KW");
            CountryCodes.Add("Laos", "LA");
            CountryCodes.Add("Lesotho", "LS");
            CountryCodes.Add("Liban", "LB");
            CountryCodes.Add("Liberia", "LR");
            CountryCodes.Add("Libia", "LY");
            CountryCodes.Add("Liechtenstein", "LI");
            CountryCodes.Add("Litwa", "LT");
            CountryCodes.Add("Luksemburg", "LU");
            CountryCodes.Add("Łotwa", "LV");
            CountryCodes.Add("Macedonia", "MK");
            CountryCodes.Add("Madagaskar", "MG");
            CountryCodes.Add("Majotta", "YT");
            CountryCodes.Add("Makau", "MO");
            CountryCodes.Add("Malawi", "MW");
            CountryCodes.Add("Malediwy", "MV");
            CountryCodes.Add("Malezja", "MY");
            CountryCodes.Add("Mali", "ML");
            CountryCodes.Add("Malta", "MT");
            CountryCodes.Add("Mariany Północne", "MP");
            CountryCodes.Add("Maroko", "MA");
            CountryCodes.Add("Martynika", "MQ");
            CountryCodes.Add("Mauretania", "MR");
            CountryCodes.Add("Mauritius", "MU");
            CountryCodes.Add("Meksyk", "MX");
            CountryCodes.Add("Mikronezja", "FM");
            CountryCodes.Add("Mjanma", "MM");
            CountryCodes.Add("Mołdawia", "MD");
            CountryCodes.Add("Monako", "MC");
            CountryCodes.Add("Mongolia", "MN");
            CountryCodes.Add("Montserrat", "MS");
            CountryCodes.Add("Mozambik", "MZ");
            CountryCodes.Add("Namibia", "NA");
            CountryCodes.Add("Nauru", "NR");
            CountryCodes.Add("Nepal", "NP");
            CountryCodes.Add("Niemcy", "DE");
            CountryCodes.Add("Niger", "NE");
            CountryCodes.Add("Nigeria", "NG");
            CountryCodes.Add("Nikaragua", "NI");
            CountryCodes.Add("Niue", "NU");
            CountryCodes.Add("Norfolk", "NF");
            CountryCodes.Add("Norwegia", "NO");
            CountryCodes.Add("Nowa Kaledonia", "NC");
            CountryCodes.Add("Nowa Zelandia", "NZ");
            CountryCodes.Add("Oman", "OM");
            CountryCodes.Add("Pakistan", "PK");
            CountryCodes.Add("Palau", "PW");
            CountryCodes.Add("Palestyna", "PS");
            CountryCodes.Add("Panama", "PA");
            CountryCodes.Add("Papua-Nowa Gwinea", "PG");
            CountryCodes.Add("Paragwaj", "PY");
            CountryCodes.Add("Peru", "PE");
            CountryCodes.Add("Pitcairn", "PN");
            CountryCodes.Add("Polinezja Francuska", "PF");
            CountryCodes.Add("Polska", "PL");
            CountryCodes.Add("Portoryko", "PR");
            CountryCodes.Add("Portugalia", "PT");
            CountryCodes.Add("Republika Południowej Afryki", "ZA");
            CountryCodes.Add("Republika Środkowoafrykańska", "CF");
            CountryCodes.Add("Republika Zielonego Przylądka", "CV");
            CountryCodes.Add("Reunion", "RE");
            CountryCodes.Add("Rosja", "RU");
            CountryCodes.Add("Rumunia", "RO");
            CountryCodes.Add("Rwanda", "RW");
            CountryCodes.Add("Sahara Zachodnia", "EH");
            CountryCodes.Add("Saint Kitts i Nevis", "KN");
            CountryCodes.Add("Saint Lucia", "LC");
            CountryCodes.Add("Saint Vincent i Grenadyny", "VC");
            CountryCodes.Add("Saint-Barthélemy", "BL");
            CountryCodes.Add("Saint-Martin", "MF");
            CountryCodes.Add("Saint-Pierre i Miquelon", "PM");
            CountryCodes.Add("Salwador", "SV");
            CountryCodes.Add("Samoa Amerykańskie", "AS");
            CountryCodes.Add("Samoa", "WS");
            CountryCodes.Add("San Marino", "SM");
            CountryCodes.Add("Senegal", "SN");
            CountryCodes.Add("Serbia", "RS");
            CountryCodes.Add("Seszele", "SC");
            CountryCodes.Add("Sierra Leone", "SL");
            CountryCodes.Add("Singapur", "SG");
            CountryCodes.Add("Sint Maarten", "SX");
            CountryCodes.Add("Słowacja", "SK");
            CountryCodes.Add("Słowenia", "SI");
            CountryCodes.Add("Somalia", "SO");
            CountryCodes.Add("Sri Lanka", "LK");
            CountryCodes.Add("Stany Zjednoczone", "US");
            CountryCodes.Add("Suazi", "SZ");
            CountryCodes.Add("Sudan", "SD");
            CountryCodes.Add("Sudan Południowy", "SS");
            CountryCodes.Add("Surinam", "SR");
            CountryCodes.Add("Svalbard i Jan Mayen", "SJ");
            CountryCodes.Add("Syria", "SY");
            CountryCodes.Add("Szwajcaria", "CH");
            CountryCodes.Add("Szwecja", "SE");
            CountryCodes.Add("Tadżykistan", "TJ");
            CountryCodes.Add("Tajlandia", "TH");
            CountryCodes.Add("Tajwan", "TW");
            CountryCodes.Add("Tanzania", "TZ");
            CountryCodes.Add("Timor Wschodni", "TL");
            CountryCodes.Add("Togo", "TG");
            CountryCodes.Add("Tokelau", "TK");
            CountryCodes.Add("Tonga", "TO");
            CountryCodes.Add("Trynidad i Tobago", "TT");
            CountryCodes.Add("Tunezja", "TN");
            CountryCodes.Add("Turcja", "TR");
            CountryCodes.Add("Turkmenistan", "TM");
            CountryCodes.Add("Turks i Caicos", "TC");
            CountryCodes.Add("Tuvalu", "TV");
            CountryCodes.Add("Uganda", "UG");
            CountryCodes.Add("Ukraina", "UA");
            CountryCodes.Add("Urugwaj", "UY");
            CountryCodes.Add("Uzbekistan", "UZ");
            CountryCodes.Add("Vanuatu", "VU");
            CountryCodes.Add("Wallis i Futuna", "WF");
            CountryCodes.Add("Watykan", "VA");
            CountryCodes.Add("Wenezuela", "VE");
            CountryCodes.Add("Węgry", "HU");
            CountryCodes.Add("Wielka Brytania", "GB");
            CountryCodes.Add("Wietnam", "VN");
            CountryCodes.Add("Włochy", "IT");
            CountryCodes.Add("Wybrzeże Kości Słoniowej", "CI");
            CountryCodes.Add("Wyspa Bouveta", "BV");
            CountryCodes.Add("Wyspa Bożego Narodzenia", "CX");
            CountryCodes.Add("Wyspa Man", "IM");
            CountryCodes.Add("Wyspa Świętej Heleny, Wyspa Wniebowstąpienia i Tristan da Cunha", "SH");
            CountryCodes.Add("Wyspy Alandzkie", "AX");
            CountryCodes.Add("Wyspy Cooka", "CK");
            CountryCodes.Add("Wyspy Dziewicze Stanów Zjednoczonych", "VI");
            CountryCodes.Add("Wyspy Heard i McDonalda", "HM");
            CountryCodes.Add("Wyspy Kokosowe", "CC");
            CountryCodes.Add("Wyspy Marshalla", "MH");
            CountryCodes.Add("Wyspy Owcze", "FO");
            CountryCodes.Add("Wyspy Salomona", "SB");
            CountryCodes.Add("Wyspy Świętego Tomasza i Książęca", "ST");
            CountryCodes.Add("Zambia", "ZM");
            CountryCodes.Add("Zimbabwe", "ZW");
            CountryCodes.Add("Zjednoczone Emiraty Arabskie", "AE");
        }

        public void SendObligation(Document doc)
        {
            logger.LogInfo("Podjęcie próby wysłania dokumentu: " + doc.full_number);            
            this.mainPageHandler.bToTray.IsEnabled = true;
            if (!object.ReferenceEquals(mainPageHandler, null))
            {
                mainPageHandler.ShowWaitAnimation();
            }
            if (!bwgSendObligation.IsBusy)
            {
                sending = true;
                bwgSendObligation.RunWorkerAsync(doc);
            }
        }

        private void bwgSendObligation_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            //wF.Close();
            if (!object.ReferenceEquals(mainPageHandler, null))
            {
                mainPageHandler.HideWaitAnimation();
            }
            //this.IsEnabled = false; ;
            Forms.ComunicatForm comForm = null;

            //string status = "";
            Document result = (Document)e.Result;
            string[] errorSplit = result.veta_errors.Split(';');            
            //status = errorSplit[0].ToString();
            string veta_errors = "";
            for (int i = 0; i < errorSplit.Length; i++)
            {
                if (!string.IsNullOrEmpty(errorSplit[i]))
                {
                    veta_errors += " " + errorSplit[i] + "\n";
                }
            }
            switch (result.sent)
            {
                case true:
                    {
                        logger.LogInfo("Powiodło się wysyłanie dokumentu: " + result.full_number);
                        comForm = new Forms.ComunicatForm("Succes", "", mwindow);
                        comForm.ShowDialog();
                        foundContrator = null;
                        mainPageHandler.EnableMainPageControls();
                        AnimateWindowWidthRight(false);                        
                        if (!object.ReferenceEquals(mainPageHandler, null))
                        {
                            mainPageHandler.SetDEfaultState();
                        }
                        ShowFromTray();

                    } break;
                case false:
                    {
                        logger.LogInfo("Nie powiodło się wysyłanie dokumentu: " + result.full_number);
                        comForm = new Forms.ComunicatForm("Error", veta_errors, mwindow);

                        comForm.ShowDialog();
                                                                     
                        mainPageHandler.EnableMainPageControls();

                        Pages.DocumentPage dockPage = new Pages.DocumentPage(krdapi, fpapi, mwindow);
                        mwindow.documentPageHandler = dockPage;
                        dockPage.FillDocumentFields(result);
                        logger.LogInfo("Uzupełniono pola dokumentu");
                        mwindow.dockFrame.NavigationService.Navigate(dockPage);
                        
                        ShowFromTray();
                        if (!mwindow.big_window)
                        {
                            mwindow.AnimateWindowWidthLeft();
                        }
                    } break;
            }
            
            sending = false;
            

        }

        private void bwgSendObligation_DoWork(object sender, DoWorkEventArgs e)
        {
            Document obligation = e.Argument as Document;
            int successcount = 0;
            int failcount = 0;
            int errorcount = 0;
            string branch = "";


            if (DateTime.Now.Subtract(krdapi.LastLogin).TotalMinutes > 15)
            {
                try
                {
                    krdapi.LoginToKrdService(XML.XMLConfig.GetLogin(), XML.XMLConfig.GetPassword());
                    krdapi.LastLogin = DateTime.Now;
                }
                catch (Exception ex)
                {
                    logger.LogMessage("Błąd logowania w bwgSendObligation_DoWork \n" + ex.Message);
                }
            }
            AcceptRegulation = XMLConfig.GetOWUAcceptation();
            try
            {
                var res = fpapi.AddDocument(obligation.PassToFPApi());
                if (res == null)
                {
                    obligation.sent = true;
                }
                else
                {
                    obligation.sent = false;
                    obligation.veta_errors = "";                  
                    obligation.veta_errors += "Sprawdź poprawność wprowadzonych danych i spróbuj ponownie\n\n;";
                    
                    foreach (var error in res)
                    {
                        if (error.Key == "10601" || error.Key == "10301" || error.Key == "10307")
                        {
                            obligation.veta_errors += error.Value.ToString() + ";";
                        }                      
                    }
                }
                //krdapi.SendDataLite(ref AcceptRegulation, Regulation, obligation, ref successcount, ref failcount, ref errorcount, branch);
            } catch (Exception ex)
            {
                logger.LogMessage("Błąd wysyłania dokumentu AddDocument \n" + ex.Message);
            }
            if (!AcceptRegulation)
            {
                XMLConfig.SetOWUAcceptation("0");
            }
            if (obligation.sent)
            {                
                Result res = comm.SaveSentDocument(obligation);
                if (res.status == 1)    
                {
                    logger.LogInfo("Zapisano wysłany dokument w bazie\n");
                }
                else
                {
                    logger.LogInfo("Nieudany zapis wysłanego dokumentu do bazy\n" + res.msg);
                }
            }
            
            e.Result = obligation;
        }

        public void SetLoggedIn(bool value)
        {
            loggedIn = value;
        }

        private void AddToAutostart()
        {

            Assembly assembly = Assembly.GetExecutingAssembly();
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
            string app_name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            var value = rkApp.GetValue(app_name);

            //if (object.ReferenceEquals(value, null))
            // {
            string startPath = Environment.GetFolderPath(Environment.SpecialFolder.Programs) + @"\Kaczmarski Group sp. z o.o\Kaczmarski Group sp. z o.o\" + app_name + ".appref-ms";
            rkApp.SetValue(app_name, startPath);
            //}
        }
        private void RemoveFromAutostart()
        {
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
            string app_name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            var value = rkApp.GetValue(app_name);
            {
                if (!object.ReferenceEquals(value, null))
                {

                    string startPath = Environment.GetFolderPath(Environment.SpecialFolder.Programs) + @"\Kaczmarski Group sp. z o.o\Kaczmarski Group sp. z o.o\" + app_name + ".appref-ms";
                    rkApp.DeleteValue(app_name);
                }
            }
        }

        void notifyIcon_Click(object sender, EventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Minimized)
            {
                if (!object.ReferenceEquals(mainPageHandler, null) && !sending)
                {
                    mainPageHandler.SetDEfaultState();
                }
                this.WindowState = System.Windows.WindowState.Normal;
                this.Show();
                this.ShowInTaskbar = true;
            }
            else if (this.WindowState == System.Windows.WindowState.Normal)
            {
                if (!sending)
                {
                    this.AnimateWindowWidthRight(false);
                }
                this.WindowState = System.Windows.WindowState.Minimized;
                //this.Hide();
                this.ShowInTaskbar = true;
                
                
            }

        }

        public void ShowFromTray()
        {
            if (this.WindowState == System.Windows.WindowState.Minimized)
            {
                this.WindowState = System.Windows.WindowState.Normal;
                this.Show();
                this.ShowInTaskbar = true;
            }
        }

        public void SetWindowDefaultPosition()
        {
            logger.LogInfo("Ustawianie pozycji okna.");
            widthScreen = System.Windows.SystemParameters.PrimaryScreenWidth;
            heightScreen = System.Windows.SystemParameters.PrimaryScreenHeight;
            this.Left = widthScreen - 990;
            this.Top = heightScreen - 520;
            //this.Width = 270;
        }
        public void SetWindowMaximizedPosition()
        {
            widthScreen = System.Windows.SystemParameters.PrimaryScreenWidth;
            heightScreen = System.Windows.SystemParameters.PrimaryScreenHeight;
            this.Left = widthScreen - 980;
            this.Top = heightScreen - 500;

        }
        public string GetCountryCode(string country)
        {
            if (CountryCodes.ContainsKey(country))
            {
                return CountryCodes[country];
            }
            else
            {
                return "";
            }
        }
        public bool ParseNIP(ref string nip)
        {

            long contractorNIP;
            if (nip.Length == 10 && Int64.TryParse(nip, out contractorNIP))
            {
                nip = contractorNIP.ToString();
                return true;
            }
            else
            {
                nip = "";
                return false;
            }
        }

        public void GenerateAppId()
        {
            logger.LogInfo("Generowanie app_id");
            string app_id = "";
            app_id = XMLConfig.GetAppID();
            if (string.IsNullOrEmpty(app_id))
            {
                app_id = Guid.NewGuid().ToString();
                XMLConfig.SetAppID(app_id);
                krdapi.SendOperation(app_id, "IdGenerated");
            }
        }



        private void Window_ContentRendered(object sender, EventArgs e)
        {
            this.Topmost = false;
            this.Activate();
        }
        public void SetFoundContractorData(Contractor contr)
        {
            Pages.DocumentPage pageDoc = new Pages.DocumentPage(krdapi,fpapi, this);
            documentPageHandler = pageDoc;
            pageDoc.FillContractorFields(contr);
            dockFrame.NavigationService.Navigate(pageDoc);

        }

        #region animacje
        //LEFT
        public void AnimateWindowWidthLeft(System.Windows.Controls.Page page = null)
        {
            windowAnimationLeft = new DoubleAnimation();
            windowAnimationLeft.Duration = new Duration(TimeSpan.Parse("00:00:01.400"));
            windowAnimationLeft.From = MainDockPanel.ActualWidth;
            windowAnimationLeft.To = 970;
            windowAnimationLeft.Completed += windowAnimationLeft_Completed;
            MainDockPanel.BeginAnimation(WidthProperty, windowAnimationLeft);
        }

        void windowAnimationLeft_Completed(object sender, EventArgs e)
        {
            big_window = true;
        }

        
        //LEFT

        //RIGHT
        System.Windows.Controls.Page pageAfterAnimation = null;
        public void AnimateWindowWidthRight(bool doubleSlide, System.Windows.Controls.Page page = null)
        {
            this.pageAfterAnimation = page;
            windowAnimationRight = new DoubleAnimation();
            windowAnimationRight.Duration = new Duration(TimeSpan.Parse("00:00:00.700"));  
            windowAnimationRight.From = MainDockPanel.ActualWidth;
            windowAnimationRight.To = 270;
            //windowAnimationRight.Completed -= (s, e) => windowAnimationRight_Completed(windowAnimationRight, doubleSlide, page);
            windowAnimationRight.Completed += (s, e) => windowAnimationRight_Completed(windowAnimationRight, doubleSlide);
            MainDockPanel.BeginAnimation(WidthProperty, windowAnimationRight);
            
        }
        void windowAnimationRight_Completed(object sender, bool e)
        {           
            
            big_window = false;
            if (e)
            {
            if (!object.ReferenceEquals(pageAfterAnimation, null))
                {
                    
                    mwindow.dockFrame.Content = null;
                    //Thread.Sleep(200);
                    mwindow.dockFrame.NavigationService.Navigate(pageAfterAnimation);
                }
                //AnimateWindowWidthLeft();
            }
        }

        //RIGHT

        public void AnimationDoubleSlide(System.Windows.Controls.Page page)
        {
            if (mwindow.big_window)
            {
                AnimateWindowWidthRight(true, page);
            }
            else
            {
                if (!object.ReferenceEquals(page, null))
                {
                    mwindow.dockFrame.NavigationService.Navigate(page);
                }
                
                AnimateWindowWidthLeft();
            }
        }

        #endregion

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (!bwgPing.IsBusy)
            {
                bwgPing.RunWorkerAsync();
            }
        }

        public bool Ping()
        {
            try
            {
                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                String host = "google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = ping.Send(host, timeout, buffer, pingOptions);
                ping = null;
                buffer = null;
                pingOptions = null;
                return (reply.Status == IPStatus.Success);
            }
            catch
            {
                return false;
            }
        }

        private void bwgPing_DoWork(object sender, DoWorkEventArgs e)
        {
            PingResult = Ping();
        }

       
        private void bwgPing_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!PingResult)
            {
                if (InternetConnection)
                {
                    if (!object.ReferenceEquals(mainPageHandler, null))
                    {
                        mainPageHandler.SetInternetConnection(false);
                        InternetConnection = false;                  
                    }                      
                }
            }
            else if (PingResult)
            {
                if (!InternetConnection)
                {
                    if (!object.ReferenceEquals(mainPageHandler, null))
                    {
                        mainPageHandler.SetInternetConnection(true);
                        InternetConnection = true;
                        krdapi.GetIP();
                    }
                }

            }
            
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            HwndSource source = PresentationSource.FromVisual(this) as HwndSource;
            source.AddHook(WndProc);
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == App.WM_ACTIVATE_FPEASY)
            {
                if (this.WindowState == WindowState.Minimized)
                {
                    this.Show();
                    this.WindowState = WindowState.Normal;
                }
            }
            else if (msg == App.WM_UPDATE_FPEASY)
            {
                System.Windows.Application.Current.Shutdown();
                System.Windows.Forms.Application.Restart();
            }

            return IntPtr.Zero;
        }

        private void Title()
        {
            Version ver = Utility.GetActualVersion();
            string environment = XMLConfig.GetEnvironment();
#if TEST
            environment = "(test)";
           
#else
            switch (environment)
            {
                case "0":
                    {
                        environment = "(prod)";
                    }
                    break;
                case "1":
                    {
                        environment = "(demo)";
                    }
                    break;
            }
#endif

            string str_version = string.Format("{0}.{1}.{2}.{3}", ver.Major, ver.Minor, ver.Build, ver.Revision);
            string stored_version = "";
            notifyIcon.Text = "Wersja: " + str_version + " " + environment;

            stored_version = XMLConfig.GetAppVersion();
            if (string.IsNullOrEmpty(stored_version))
            {
                stored_version = "0.0.0.0";
                XMLConfig.InsertMissingElement(XMLConfig.fileName, "app_version", stored_version);
                XMLConfig.SetAppVersion(stored_version);
            }


            if (stored_version != str_version)
            {
                string app_name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                XMLConfig.SetAppVersion(str_version);
            }

            string app_id = "";
            app_id = XMLConfig.GetAppID();
            if (string.IsNullOrEmpty(app_id))
            {
                app_id = Guid.NewGuid().ToString();
                XMLConfig.InsertMissingElement(XMLConfig.fileName, "app_id", app_id);
                XMLConfig.SetAppID(app_id);
                krdapi.SendOperation(app_id, "IdGenerated");
            }
        }
    }

    
}
