﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.IO.Compression;
using STP_Lite.XML;
using System.ComponentModel;
//using Ionic.Zip;


namespace STP_Lite
{
    public class Logger
    {
        private static string baseDir;
        private static string pathToZipKG;
        BackgroundWorker bwgSendMail = new BackgroundWorker();


        public void LoggerStart()
        {
            baseDir = @"C:\Users\" + Environment.UserName + @"\Documents\FPEASY\";
            bwgSendMail.DoWork += bwgSendMail_DoWork;
            bwgSendMail.RunWorkerCompleted += bwgSendMail_Completed;
        }

        private void bwgSendMail_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            LogInfo("Poprawnie wysłano email do leadfpplite@krd.pl");
        }

        private void bwgSendMail_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string login = StaticFunctions.Functions.Decrypt(XML.XMLConfig.GetLogin());
                var fromAddress = new MailAddress("fairpayeasy@gmail.com", "FairPay Easy");
                var toAddress = new MailAddress("leadfpplite@krd.pl");
                const string fromPassword = "f@irp@ysupport";
                string subject = login;
                string body = login + " " + DateTime.Now.ToString();

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    //message.Attachments.Add(new Attachment(pathToAttach));
                    //smtp.Timeout = 5000;
                    LogInfo("Próba wywołania metody smtp.Send");
                    smtp.Send(message);
                }
            }
            catch (Exception ex)
            {
                LogMessage("Błąd wysyłania wiadomości e-mail \n" + ex.Message);
            }
        }

        public bool FileInUse(string path)
        {
            try
            {
                bool cw = false;
                using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    cw = fs.CanWrite;
                }
                return false;
            }
            catch
            {
                return true;
            }
        }

        public void LogException(Exception e, int log = 0)
        {
            string path = baseDir;
            switch (log)
            {
                case 0:
                    {
                        path += "\\debug_fpeasy.txt";
                    } break;
                case 1:
                    {
                        path += "\\debug_get_companies_fpeasy.txt";
                    } break;
                case 2:
                    {
                        path += "\\debug_get_synchro_fpeasy.txt";
                    } break;
            }
            if (!FileInUse(path))
            {
                try
                {

                    if (!File.Exists(System.IO.Path.GetFullPath(path)))
                    {
                        File.Create(System.IO.Path.GetFullPath(path)).Close();
                    }
                    using (StreamWriter w = File.AppendText(System.IO.Path.GetFullPath(path)))
                    {
                        w.WriteLine("\r\nLog Entry : ");
                        w.WriteLine("Wersja aplikacji: " + XMLConfig.GetAppVersion());
                        w.WriteLine("{0}", DateTime.Now.ToString(CultureInfo.InvariantCulture));
                        string err = "Error Message:" + e.Message;
                        w.WriteLine(err);
                        string errStack = "StackTrace:" + e.StackTrace;
                        w.WriteLine(errStack);
                        w.WriteLine("____________________________________________________________________");
                        w.Flush();
                        w.Close();
                    }
                }
                catch (Exception ex)
                {
                    LogMessage(ex.StackTrace);
                }
            }
        }


        public void LogMessage(string errorMessage, int log = 0)
        {
            string path = baseDir;
            switch (log)
            {
                case 0:
                    {
                        path += "\\debug_fpeasy.txt";
                    } break;
                case 1:
                    {
                        path += "\\debug_get_companies_fpeasy.txt";
                    } break;
                case 2:
                    {
                        path += "\\debug_get_synchro_fpeasy.txt";
                    } break;
            }
            if (!FileInUse(path))
            {
                try
                {
                    if (!FileInUse(path))
                    {
                        if (!File.Exists(System.IO.Path.GetFullPath(path)))
                        {
                            File.Create(System.IO.Path.GetFullPath(path)).Close();
                        }
                        using (StreamWriter w = File.AppendText(System.IO.Path.GetFullPath(path)))
                        {
                            w.WriteLine("\r\nLog Entry : ");
                            w.WriteLine("Wersja aplikacji: " + XMLConfig.GetAppVersion());
                            w.WriteLine("{0}", DateTime.Now.ToString(CultureInfo.InvariantCulture));
                            string err = "Error Message:" + errorMessage;
                            w.WriteLine(err);
                            w.WriteLine("____________________________________________________________________");
                            w.Flush();
                            w.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogMessage(ex.StackTrace);
                }
            }
        }

        public void LogInfo(string info, int log = 0)
        {
            string path = baseDir;
            switch (log)
            {
                case 0:
                    {
                        path += "\\debug_info.txt";
                    } break;
                case 1:
                    {
                        path += "\\debug_info_get_companies.txt";
                    } break;
                case 2:
                    {
                        path += "\\debug_info_synchro_companies.txt";
                    } break;
            }

            if (!FileInUse(path))
            {
                try
                {
                    if (!File.Exists(System.IO.Path.GetFullPath(path)))
                    {
                        File.Create(System.IO.Path.GetFullPath(path)).Close();
                    }
                    using (StreamWriter w = File.AppendText(System.IO.Path.GetFullPath(path)))
                    {
                        w.WriteLine("\r\n");
                        w.WriteLine("Wersja aplikacji: " + XMLConfig.GetAppVersion());
                        w.WriteLine("{0}" + "Info Log: " + info, DateTime.Now.ToString(CultureInfo.InvariantCulture));

                        // w.WriteLine("____________________________________________________________________");
                        w.Flush();
                        w.Close();
                    }
                }
                catch (Exception ex)
                {
                    LogException(ex);
                }
            }
        }

        public void LogXMLError(string xml)
        {
            string path = baseDir;
            path += "\\debug_xml.txt";


            if (!FileInUse(path))
            {
                try
                {
                    if (!File.Exists(System.IO.Path.GetFullPath(path)))
                    {
                        File.Create(System.IO.Path.GetFullPath(path)).Close();
                    }
                    using (StreamWriter w = File.AppendText(System.IO.Path.GetFullPath(path)))
                    {
                        w.WriteLine("\r\n");
                        w.WriteLine("Wersja aplikacji: " + XMLConfig.GetAppVersion());
                        w.WriteLine("{0}" + "xml Log: " + xml, DateTime.Now.ToString(CultureInfo.InvariantCulture));

                        // w.WriteLine("____________________________________________________________________");
                        w.Flush();
                        w.Close();
                    }
                }
                catch (Exception ex)
                {
                    LogException(ex);
                }
            }
        }


        public void sendMail()
        {
            if (!bwgSendMail.IsBusy)
            {
                LogInfo("Próba uruchomienia bwgSendMail");            
                bwgSendMail.RunWorkerAsync();
            }

        }
        //public string zipKG()
        //{
        //    string correct = "";
        //    string destinationFileName = @"KG_" + DateTime.Now.ToShortDateString() + ".zip";
        //    //pathToZipKG = @"C:\Users\" + Environment.UserName + @"\Documents\" + destinationFileName;


        //    try
        //    {
        //        ZipFile zip = new ZipFile();

        //        if (Directory.Exists(baseDir))
        //        {
        //            string[] files = System.IO.Directory.GetFiles(baseDir, "*.txt");
        //            foreach (var file in files)
        //            {
        //                //zip.AddDirectory(baseDir); // recurses subdirectories
        //                zip.AddFile(file, "");
        //            }
        //            zip.Save(destinationFileName);
        //            correct = baseDir + destinationFileName;
        //            File.Move(destinationFileName, correct);


        //        }
        //        else
        //        {
        //            correct = "";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        correct = "";
        //    }


        //    return correct;
        //}
        //public void clearLogs(int lastDays)
        //{



        //}


        public void SaveActivityTime()
        {
            string path = baseDir;
            path += "\\Time.txt";

            if (!FileInUse(path))
            {
                try
                {
                    if (!File.Exists(System.IO.Path.GetFullPath(path)))
                    {
                        File.Create(System.IO.Path.GetFullPath(path)).Close();
                    }

                    System.IO.StreamWriter file = new System.IO.StreamWriter(path);
                    file.WriteLine("0");

                    file.Close();
                }
                catch (Exception ex)
                {
                    LogException(ex);
                }
            }
        }
    }
}
