﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STP_Lite.KrdStructures
{
    public class Entrepreneur : LegalPerson
    {
        public string firstname;
        public string surname;

        public Entrepreneur()
        {
            name = "";
            tax_number = "";
            seat_address = new Address();
            contact_data = new ContactData();
            //bank_account = "";
            firstname = "";
            surname = "";
        }
    }
}
