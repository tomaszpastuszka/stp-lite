﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace STP_Lite.KrdStructures
{
    public class Address
    {
        public string street;
        public string building;
        public string flat;
        public string city;
        public string post_code;
        public string post;
        public string country;

        public Address()
        {
            street = "";
            building = "";
            flat = "";
            city = "";
            post_code = "";
            post = "";
            country = "";
        }

        public string ValidateAddress(string street, string building, string flat, string city, string post_code, string post, string country, string country_code)
        {
            string ret = "";
            country_code = country_code.Trim();
            //if(!string.IsNullOrEmpty(country_code) && !string.IsNullOrEmpty(country))
            //{
            //    if(country_code.Length == 2)
            //    {
            //        this.country = country_code;
            //    }
            //    else if(country_code.Length != 2)
            //    {
            //        if(KonektorMainForm.CountryCodes.ContainsKey(country))
            //        {
            //            this.country = KonektorMainForm.CountryCodes[country];
            //        }
            //        else
            //        {
            //            ret += "Brak państwa kontrahenta;";
            //        }
            //    }
            //}
            //else if (!string.IsNullOrEmpty(country_code) && string.IsNullOrEmpty(country))
            //{
            //    this.country = country_code;
            //}
            //else if (string.IsNullOrEmpty(country_code) && !string.IsNullOrEmpty(country))
            //{
            //    if (KonektorMainForm.CountryCodes.ContainsKey(country))
            //    {
            //        this.country = KonektorMainForm.CountryCodes[country];
            //    }
            //    else
            //    {
            //        ret += "Brak państwa kontrahenta;";
            //    }
            //}
            //else if (string.IsNullOrEmpty(country_code) && string.IsNullOrEmpty(country))
            //{
            //    ret += "Brak państwa kontrahenta;";
            //}

            if(!string.IsNullOrEmpty(building))
            {
                this.building = building;
            }

            if (!string.IsNullOrEmpty(flat))
            {
                this.flat = flat;
            }

            if (!string.IsNullOrEmpty(post_code) && Regex.IsMatch(post_code, @"[0-9]{2}-[0-9]{3}", RegexOptions.IgnoreCase))
            {
                this.post_code = post_code;
            }
            else if(!Regex.IsMatch(post_code, @"[0-9]{2}-[0-9]{3}", RegexOptions.IgnoreCase))
            {
                ret += "Niepoprawny kod pocztowy;";
            }
            else
            {
                ret += "Brak kodu pocztowego;";
            }

            if (!string.IsNullOrEmpty(post))
            {
                this.post = post;
            }
            else if (!string.IsNullOrEmpty(city))
            {
                this.post = city;
            }
            else
            {
                ret += "Brak miejscowości;";
            }

            if (string.IsNullOrEmpty(this.country))
            {
                ret += "Brak kraju;";
            }

            if(string.IsNullOrEmpty(street))
            {
                ret += "Brak ulicy;";
            }
            if(!string.IsNullOrEmpty(street))
            {
                this.street = street;
            }
            if(!string.IsNullOrEmpty(city))
            {
                this.city = city;
            }
            else
            {
                ret += "Brak miasta;";
            }

            return ret;
        }
    }
}
