﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STP_Lite.KrdStructures
{
    public class LegalPerson
    {
        public string name;
        public string tax_number;
        public Address seat_address;
        public ContactData contact_data;

        public LegalPerson()
        {
            name = "";
            tax_number = "";
            seat_address = new Address();
            contact_data = new ContactData();
            //bank_account = "";
        }
    }
}
