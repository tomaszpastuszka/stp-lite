﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STP_Lite.KrdStructures
{
    public class Obligation
    {
        //public DocType type;
        public string number;
        public string document_creation;
        public string amount;
        public string duedate;
        public Money totalgross_amount;
        public Money totalnet_amount;
        public string bank_account;
        public Entrepreneur entrepreneur;
        public LegalPerson legal_person;


        public Obligation()
        {
            //type = DocType.INVOICE;
            number = "";
            document_creation = "";
            amount = "";
            duedate = "";
            totalgross_amount = new Money();
            totalnet_amount = new Money();
            bank_account = "";
        }
    }
}
