﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STP_Lite.KrdStructures
{
    public class ContactData
    {
        public string name;
        public string surname;
        public string role;
        public string phone;
        public string email;
        public bool isdefault;

        public ContactData()
        {
            name = "";
            surname = "";
            role = "";
            phone = "";
            email = "";
            isdefault = false;
        }
    }
}
