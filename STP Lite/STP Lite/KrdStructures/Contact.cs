﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STP_Lite.Structures
{
    public class Contact
    {
        public string firstname;
        public string surname;
        public string phone;
        public string email;
    }
}
