﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STP_Lite.KrdStructures
{
    public class ResponseGuid
    {
        public string userId;
        public Guid response;

        public ResponseGuid()
        {
            userId = "";
            response = Guid.Empty;
        }
    }
}
