﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STP_Lite.KrdStructures
{
    public class Money
    {
        public double amount;
        public string currency;
        public Money()
        {
            amount = 0.0;
            currency = "PLN";
        }
    }
}
