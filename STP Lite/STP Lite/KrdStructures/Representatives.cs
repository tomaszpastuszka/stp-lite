﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STP_Lite.KrdStructures
{
    public class Representatives
    {
        public string firstname;
        public string secondname;
        public string surname;
        public string role;
        public string idnumber;

        public Representatives()
        {
            firstname = "";
            secondname = "";
            surname = "";
            role = "";
            idnumber = "";
        }
    }
}
