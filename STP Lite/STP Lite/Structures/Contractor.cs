﻿using STP_Lite.KrdStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STP_Lite.Structures
{
    public class Contractor
    {
        public int external_id;
        //public IntegType integration_type;
        public int company_id;
        public string company_name;
        public string name;
        public string acronym;
        public string tax_number;
        public Address address;
        public ContactData contact;
        public bool entreprenuer;
        public bool deleted;
        public bool excluded;
        public string firstname;
        public string lastname;
        public string bank_account;

        public Contractor()
        {
            external_id = 0;
            //integration_type = IntegType.none;
            company_id = 0;
            company_name = "";
            name = "";
            acronym = "";
            tax_number = "";
            address = new Address();
            contact = new ContactData();
            entreprenuer = false;
            deleted = false;
            excluded = false;
            firstname = "";
            lastname = "";
            bank_account = "";
        }

        public string ValidateContractorData(string name, string firstname, string surname, string tax_number, string street, string building, string flat,
                                             string city, string post_code, string post, string country, string country_code, List<ContactData> contacts)
        {
            string ret = "";
            if (!string.IsNullOrEmpty(name))
            {
                this.name = name;
            }
            else
            {
                ret += "Brak nazwy kontrahenta;";
            }
            if (!string.IsNullOrEmpty(firstname) && !string.IsNullOrEmpty(surname))
            {
                this.entreprenuer = true;
                this.firstname = firstname;
                this.lastname = surname;
            }

            if (!string.IsNullOrEmpty(tax_number))
            {
                this.tax_number = tax_number.Trim().Replace("-", "").Replace(" ", "");
            }
            else
            {
                ret += "Brak nipu kontrahenta;";
            }

            ret += address.ValidateAddress(street, building, flat, city, post_code, post, country, country_code);
            var defcontact_data = contacts.Find(o => o.isdefault == true);

            if (!string.IsNullOrEmpty(contacts[0].email))
            {
                if (KRD.Validators.KrdEmailValidator.IsEmailValid(contacts[0].email))
                {
                    this.contact.email = contacts[0].email;
                }
                else
                {
                    ret += "Niepoprawny email kontrahenta;";
                }

                if (!string.IsNullOrEmpty(contacts[0].phone))
                {
                    if (KRD.Validators.PhoneNumberValidator.IsValid(contacts[0].phone))
                    {
                        this.contact.phone = contacts[0].phone;
                    }
                    else
                    {
                        ret += "Niepoprawny numer telefonu kontrahenta;";
                    }
                }
                if(!string.IsNullOrEmpty(contacts[0].name))
                {
                    this.contact.name = contacts[0].name;
                }

                if (!string.IsNullOrEmpty(contacts[0].surname))
                {
                    this.contact.surname = contacts[0].surname;
                }

                if (!string.IsNullOrEmpty(contacts[0].role))
                {
                    this.contact.role = contacts[0].role;
                }
            }
            else if (defcontact_data != null && !string.IsNullOrEmpty(defcontact_data.email))
            {
                if (KRD.Validators.KrdEmailValidator.IsEmailValid(defcontact_data.email))
                {
                    this.contact.email = defcontact_data.email;
                }
                else
                {
                    ret += "Niepoprawny email osoby kontaktowej;";
                }

                if (!string.IsNullOrEmpty(defcontact_data.phone))
                {
                    if (KRD.Validators.PhoneNumberValidator.IsValid(defcontact_data.phone))
                    {
                        this.contact.phone = defcontact_data.phone;
                    }
                    else
                    {
                        ret += "Niepoprawny numer telefonu kontrahenta;";
                    }
                }

                if (!string.IsNullOrEmpty(defcontact_data.name))
                {
                    this.contact.name = defcontact_data.name;
                }

                if (!string.IsNullOrEmpty(defcontact_data.surname))
                {
                    this.contact.surname = defcontact_data.surname;
                }

                if (!string.IsNullOrEmpty(defcontact_data.role))
                {
                    this.contact.role = defcontact_data.role;
                }
            }
            else
            {
                var contact = contacts.Find(o => !string.IsNullOrEmpty(o.email));
                if (contact == null)
                {
                    ret += "Brak adresu email osoby kontaktowej;";
                }
                else
                {
                    if (KRD.Validators.KrdEmailValidator.IsEmailValid(contact.email))
                    {
                        this.contact.email = contact.email;
                    }
                    else
                    {
                        ret += "Niepoprawny adres email osoby kontaktowej";
                    }

                    if (!string.IsNullOrEmpty(contact.phone))
                    {
                        if (KRD.Validators.PhoneNumberValidator.IsValid(contact.phone))
                        {
                            this.contact.phone = contact.phone;
                        }
                        else
                        {
                            ret += "Niepoprawny numer telefonu osoby kontaktowej";
                        }
                    }

                    if (!string.IsNullOrEmpty(contact.name))
                    {
                        this.contact.name = contact.name;
                    }

                    if (!string.IsNullOrEmpty(contact.surname))
                    {
                        this.contact.surname = contact.surname;
                    }

                    if (!string.IsNullOrEmpty(contact.role))
                    {
                        this.contact.role = contact.role;
                    }
                }
            }

            return ret;
        }

        public string ValidateContractorData()
        {
            string ret = "";

            if (string.IsNullOrEmpty(name))
            {
                ret += "Brak nazwy kontrahenta;";
            }

            if (!string.IsNullOrEmpty(this.firstname) && !string.IsNullOrEmpty(this.lastname))
            {
                this.entreprenuer = true;
            }

            if (string.IsNullOrEmpty(tax_number))
            {
                ret += "Brak nipu kontrahenta;";
            }

            ret += address.ValidateAddress(this.address.street, this.address.building, this.address.flat, this.address.city, this.address.post_code, this.address.post, this.address.country, this.address.country);

            if (string.IsNullOrEmpty(this.contact.email))
            {
                ret += "Brak adresu email;";
              
            }
            else
            {
                if (!KRD.Validators.KrdEmailValidator.IsEmailValid(this.contact.email))
                {
                    ret += "Niepoprawny adres email;";
                }
            }

            if (!string.IsNullOrEmpty(this.contact.phone))
            {
                if (!KRD.Validators.PhoneNumberValidator.IsValid(this.contact.phone))
                {
                    ret += "Niepoprawny numer telefonu;";
                }
            }
           
            return ret;
        }

        public void SetContractorData(int external_id, int company_id, string company_name, 
                                      string name, string acronym, string firstname, string surname, string tax_number, string street, string building, string flat,
                                      string city, string post_code, string post, string country, string country_code, List<ContactData> contacts)
        {
            this.external_id = external_id;            
            this.company_id = company_id;
            this.company_name = company_name;
            if (!string.IsNullOrEmpty(firstname) && !string.IsNullOrEmpty(surname))
            {
                this.entreprenuer = true;
                this.firstname = firstname;
                this.lastname = surname;
            }
            this.name = name;
            this.acronym = acronym;
            this.tax_number = tax_number.Trim().Replace("-", "").Replace(" ", "");
            this.address.street = street;
            this.address.building = building;
            this.address.flat = flat;
            this.address.city = city;
            this.address.post_code = post_code;
            if (string.IsNullOrEmpty(post))
            {
                this.address.post = city;
            }
            else
            {
                this.address.post = post;
            }
            this.address.country = country_code;
            if (!string.IsNullOrEmpty(country_code) && !string.IsNullOrEmpty(country))
            {
                if (country_code.Length == 2)
                {
                    this.address.country = country_code;
                }
                else if (country_code.Length != 2)
                {
                    //if (KonektorMainForm.CountryCodes.ContainsKey(country))
                    //{
                    //    this.address.country = KonektorMainForm.CountryCodes[country];
                    //}
                }
            }
            else if (!string.IsNullOrEmpty(country_code) && string.IsNullOrEmpty(country))
            {
                this.address.country = country_code;
            }
            else if (string.IsNullOrEmpty(country_code) && !string.IsNullOrEmpty(country))
            {
                //if (KonektorMainForm.CountryCodes.ContainsKey(country))
                //{
                //    this.address.country = KonektorMainForm.CountryCodes[country];
                //}               
            }
            

            var defcontact_data = contacts.Find(o => o.isdefault == true);
            if (!string.IsNullOrEmpty(contacts[0].email) && !string.IsNullOrEmpty(contacts[0].phone))
            {
                this.contact.email = contacts[0].email;
                this.contact.phone = contacts[0].phone;
            }
            else if (defcontact_data != null)
            {
                if (!string.IsNullOrEmpty(defcontact_data.email) && !string.IsNullOrEmpty(defcontact_data.phone))
                {
                    this.contact.email = defcontact_data.email;
                    this.contact.phone = defcontact_data.phone;
                    this.contact.name = defcontact_data.name;
                    this.contact.surname = defcontact_data.surname;
                    this.contact.role = defcontact_data.role;
                }
            }
            else
            {
                var contact = contacts.Find(o => !string.IsNullOrEmpty(o.phone) && !string.IsNullOrEmpty(o.email));
                if (contact != null)
                {
                    this.contact.email = contact.email;
                    this.contact.phone = contact.phone;
                    this.contact.name = contact.name;
                    this.contact.surname = contact.surname;
                    this.contact.role = contact.role;
                }
            }
            this.deleted = false;
            this.excluded = false;
        }

        public void SetContractorType()
        {
            string firstname = "";
            string surname = "";
            Utility.GetContractorNameAndSurname(this.name, ref firstname, ref surname);
            if (!string.IsNullOrEmpty(firstname) && !string.IsNullOrEmpty(surname))
            {
                this.entreprenuer = true;
                this.firstname = firstname;
                this.lastname = surname;
            }
        }
    }
}
