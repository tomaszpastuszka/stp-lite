﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STP_Lite.KrdStructures;
using System.Globalization;
using RestSharp.Extensions.MonoHttp;

namespace STP_Lite.Structures
{

    public class Document
    {
        string[] formats = {
                                        "d/M/yyyy", "dd/MM/yyyy",
                                        "yyyy/M/d", "yyyy/MM/dd",
                                        "yyyy-M-d", "yyyy-MM-dd",
                                        "d.M.yyyy", "dd.MM.yyyy",
                                        "yyyy.M.d", "yyyy.MM.dd",
                                        "d,M,yyyy", "dd,MM,yyyy",
                                        "yyyy,M,d", "yyyy,MM,dd",
                                        "d M yyyy", "dd MM yyyy",
                                        "yyyy M d", "yyyy MM dd"
                               };
        public int external_id;
        //public IntegType integration_type;
        public int company_id;
        public string company_name;
        //public DocStatus status;
        //public DocType type;
        public string lacks;
        public string full_number;
        public Money gross_amount;
        public Money paid_amount;
        public Money net_amount;
        public string bank_account;
        public DateTime creation_date;
        public DateTime send_date;
        public DateTime last_modify;
        public Contractor contractor;
        //public Obligation obligation;
        public ResponseGuid identifiers;
        public bool case_closed;
        public int days;
        public bool found;
        //public int definition;
        public bool corrected;
        public int optima_document_type;
        public int table_id;
        public int corrections_count;
        public string contractor_name;
        public DateTime issue_date;
        public int contractor_id;
        public string contractor_tax_number;
        public DateTime due_date;
        public string veta_errors;
        public bool sent;

        public Document()
        {
            external_id = 0;
            //integration_type = IntegType.none;
            company_id = 0;
            company_name = "";
            //status = DocStatus.NONE;
            //type = DocType.INVOICE;
            lacks = "";
            full_number = "";
            gross_amount = new Money();
            paid_amount = new Money();
            net_amount = new Money();
            bank_account = "";
            contractor = new Contractor();
            identifiers = new ResponseGuid();
            case_closed = false;
            days = 0;
            found = false;
            corrected = false;
            optima_document_type = 0;
            table_id = 0;
            corrections_count = 0;
            contractor_name = "";
            issue_date = DateTime.Now;
            contractor_id = 0;
            contractor_tax_number = "";
            veta_errors = "";
            sent = false;
        }

        public string ValidateDocumentData()
        {
            string ret = "";

            //this.type = DocType.INVOICE;


            if (string.IsNullOrEmpty(this.full_number))
            {
                ret += "Brak numeru dokumentu;";
            }

            if (this.gross_amount == null || this.gross_amount.amount == 0.0)
            {
                ret += "Brak kwoty brutto dokumentu;";
            }

            if (this.net_amount == null || this.net_amount.amount == 0.0)
            {
                ret += "Brak kwoty netto dokumentu;";
            }

            if (this.creation_date == null)
            {
                ret += "Brak daty wystawienia dokumentu;";
            }

            if (string.IsNullOrEmpty(this.gross_amount.currency))
            {
                this.gross_amount.currency = "PLN";
            }
            else
            {
                if (this.gross_amount.currency.Length < 3)
                {
                    ret += "Niepoprawna waluta";
                }
            }

            if (string.IsNullOrEmpty(this.net_amount.currency))
            {
                this.net_amount.currency = "PLN";
            }
            else
            {
                if (this.gross_amount.currency.Length < 3)
                {
                    ret += "Niepoprawna waluta";
                }
            }

            if (this.due_date == null)
            {
                ret += "Brak terminu zapłaty dokumentu";
            }

            if (string.IsNullOrEmpty(this.bank_account))
            {
                ret += "Brak numeru konta Twojej firmy;";
            }

            else
            {
                string bank_account = this.bank_account;
                bank_account = bank_account.Trim();
                bank_account = bank_account.Replace("-", "");
                bank_account = bank_account.Replace(" ", "");
                if (bank_account.Length != 26)
                {
                    ret += "Numer konta bankowego Twojej firmy musi mieć 26 znaków";
                }
            }
            return ret;
        }

        public void FillDocumentData(Company cmp, int external_id,
                                     string full_number, string gross_amount,
                                     string currency, string creation_date, string table_id, string days, string issue_date,
                                     string net_amount, string bufor, string contractor_id, string bank_account,
                                     string name, string firstname, string surname, string tax_number, string street, string building, string flat,
                                     string city, string post_code, string post, string country, string country_code, List<ContactData> contacts, string due_date)
        {
            this.external_id = external_id;
            //this.integration_type = integration_type;
            this.company_id = cmp.id;
            this.company_name = cmp.name;
            this.full_number = full_number;
            this.gross_amount.amount = Convert.ToDouble(gross_amount);
            this.net_amount.amount = Convert.ToDouble(net_amount);
            if (this.net_amount.amount == 0)
            {
                this.net_amount.amount = this.gross_amount.amount;
            }
            this.lacks = "";
            if (string.IsNullOrEmpty(this.bank_account))
            {
                this.bank_account = bank_account;
            }

            if (!DateTime.TryParse(creation_date, out this.creation_date))
            {
                DateTime.TryParseExact(creation_date, formats, CultureInfo.CurrentCulture, DateTimeStyles.None, out this.creation_date);
            }
            //this.creation_date = Convert.ToDateTime(creation_date);
            this.table_id = Convert.ToInt32(table_id);
            this.days = Convert.ToInt32(days);
            this.found = true;
            if (!DateTime.TryParse(issue_date, out this.issue_date))
            {
                DateTime.TryParseExact(issue_date, formats, CultureInfo.CurrentCulture, DateTimeStyles.None, out this.issue_date);
            }
            //this.issue_date = Convert.ToDateTime(issue_date);
            if (!DateTime.TryParse(due_date, out this.due_date))
            {
                DateTime.TryParseExact(due_date, formats, CultureInfo.CurrentCulture, DateTimeStyles.None, out this.due_date);
            }
            //this.due_date = Convert.ToDateTime(due_date);
            if (string.IsNullOrEmpty(currency))
            {
                this.gross_amount.currency = "PLN";
                this.gross_amount.currency = "PLN";
                this.net_amount.currency = "PLN";
                this.net_amount.currency = "PLN";
            }
            else
            {
                this.gross_amount.currency = currency;
                this.gross_amount.currency = currency;
                this.net_amount.currency = currency;
                this.net_amount.currency = currency;
            }
            this.contractor_id = Convert.ToInt32(contractor_id);
            //if (Convert.ToInt32(bufor) == 1)
            //{
            //    this.status |= DocStatus.BUFOR;
            //}
            //else
            //{
            //    this.status &= ~DocStatus.BUFOR;
            //}

            //var contres = Comm.GetContractor(this, Convert.ToInt32(contractor_id));

            string contractor_lacks = "";
            //if (this.contractor.external_id == 0)
            //{
            this.contractor.external_id = Convert.ToInt32(contractor_id);
            //this.contractor.integration_type = integration_type;
            this.contractor.company_id = company_id;
            this.contractor.company_name = company_name;

            contractor_lacks = this.contractor.ValidateContractorData(name, firstname, surname, tax_number, street, building, flat, city, post_code, post, country,
                                                                            country_code, contacts);
            if (contractor_lacks.Length > 0)
            {
                this.lacks += contractor_lacks;
                //this.status |= DocStatus.NO_CONTR_DATA;
            }
            //}

            /*else
            {
                contractor_lacks = this.contractor.ValidateContractorData();
                if (contractor_lacks.Length > 0)
                {
                    this.lacks += contractor_lacks;
                    this.status |= DocStatus.NO_CONTR_DATA;
                }
            }*/
            //if(string.IsNullOrEmpty(contractor_lacks))
            //{
            //    this.status &= ~DocStatus.NO_CONTR_DATA;
            //}

            string document_lacks = this.ValidateDocumentData();
            //if (document_lacks.Length > 0)
            //{
            //    this.lacks += document_lacks;
            //    this.status |= DocStatus.NO_DOCUMENT_DATA;
            //}
            //else
            //{
            //    this.status &= ~DocStatus.NO_DOCUMENT_DATA;
            //}



            if (this.lacks.Length > 0)
            {
                this.lacks = this.full_number + ";" + this.lacks;
            }
        }

        public Api.Models.Document PassToFPApi()
        {
            Api.Models.Document document = new Api.Models.Document();
            document.documentId = HttpUtility.UrlEncode(this.full_number.Replace(".", "_").Replace(" ", "_"));
            this.identifiers.userId = document.documentId;
            document.documentNumber = this.full_number;
            document.documentType = "Invoice";
            document.issuedDate = this.creation_date.ToString("yyyy-MM-dd");
            document.sourceApplication = "FPConnectLite";

            document.contractor = new Api.Models.DocumentContractor();
            document.contractor.contractorId = HttpUtility.UrlEncode(this.contractor.tax_number.Replace(".", "_").Replace(" ", "_") + "_" + this.full_number.Replace(".", "_").Replace(" ", "_"));
            document.contractor.companyName = this.contractor.name;
            document.contractor.addressLine = this.contractor.address.street;
            document.contractor.building = this.contractor.address.building;
            document.contractor.flat = this.contractor.address.flat;
            document.contractor.zipCode = this.contractor.address.post_code;
            document.contractor.phone = this.contractor.contact.phone;
            document.contractor.email = this.contractor.contact.email;
            document.contractor.locality = this.contractor.address.country;
            document.contractor.taxNumber = this.contractor.tax_number;

            document.receivables = new List<Api.Models.Receivable>();
            document.receivables.Add(new Api.Models.Receivable
            {
                accountNumber = this.bank_account,
                currency = this.gross_amount.currency,
                grossAmount = this.gross_amount.amount,
                netAmount = this.net_amount.amount,
                dueDate = this.due_date.ToString("yyyy-MM-dd")
            }
            );

            document.processConfiguration = new Api.Models.ProcessConfiguration();
            document.processConfiguration.selectedServiceType = "Stp";
            document.processConfiguration.stpServiceConfiguration = new Api.Models.StpServiceConfiguration();
            document.processConfiguration.stpServiceConfiguration.debtCollectionConfiguration = new Api.Models.DebtCollectionConfiguration();
            document.processConfiguration.stpServiceConfiguration.maxNumberOfDaysAfterDueDateToAddPositiveInformation = 7;
            document.processConfiguration.stpServiceConfiguration.debtCollectionConfiguration.startAutomatically = false;
            document.processConfiguration.stpServiceConfiguration.debtCollectionConfiguration.daysToStartAfterDueDate = 21;
            document.processConfiguration.stpServiceConfiguration.blockTransfer = false;
            document.processConfiguration.stpServiceConfiguration.enableNotifications = true;


            return document;
        }
    }
}
