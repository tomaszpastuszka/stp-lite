﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STP_Lite.Structures
{
    public class Regulations
    {
        public string regulationsId { get; set; }
        public string regulationsUrl { get; set; }
    }
}