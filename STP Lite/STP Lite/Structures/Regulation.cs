﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STP_Lite.Structures
{
    public class Regulation
    {
        public bool RegulationAccepted;
        public bool RegulationAcceptedSpecified;
        public bool CanManageObligations;
        public bool CanManageObligationsSpecified;
        public bool HasContractOriginal;
        public bool HasContractOriginalSpecified;

        public Regulation()
        {
            RegulationAccepted = false;
            RegulationAcceptedSpecified = false;
            CanManageObligations = false;
            CanManageObligationsSpecified = false;
            HasContractOriginal = false;
            HasContractOriginalSpecified = false;
        }
    }
}
