﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STP_Lite.KrdStructures;

namespace STP_Lite.Structures
{
    public class Repayment
    {
        public int external_id;
        //public IntegType integration_type;
        public int company_id;
        //public DocStatus doc_status;
        public Money amount;
        public DateTime paid_date;
        public bool Positive;
        public bool Negative;
        public string Userid;
        public Company cmp;

        public Repayment()
        {
            external_id = 0;
            //integration_type = IntegType.none;
            company_id = 0;
            amount = new Money();
            paid_date = DateTime.Now;
            Positive = false;
            Negative = false;
            Userid = "";
            cmp = new Company();
        }
    }
}
