﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STP_Lite.Structures
{
    public class Company
    {
        public int id;
        public int module_id;
        //public IntegType integ_type;
        public string name;
        public string database;

        public Company()
        {
            id = 0;
            module_id = 0;
            //integ_type = IntegType.none;
            name = "";
        }

        public Company(int id, string name)
        {
            this.id = id;
            //this.integ_type = integ_type;
            this.name = name;
        }

        
    }

    
}
